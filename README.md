# Energy Analyzer
[![NuGet Status](https://img.shields.io/nuget/vpre/energyanalyzer)](https://www.nuget.org/packages/EnergyAnalyzer/)
[![Build Status](https://img.shields.io/gitlab/pipeline-status/ImDreamer/analyzer-p10?branch=main)](https://gitlab.com/ImDreamer/analyzer-p10)
[![Coverage Status](https://img.shields.io/gitlab/coverage/imdreamer/analyzer-p10/main)](https://gitlab.com/ImDreamer/analyzer-p10)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/ImDreamer/analyzer-p10/-/blob/main/LICENSE)
[![LOC](https://img.shields.io/tokei/lines/gitlab.com/imdreamer/analyzer-p10)](https://gitlab.com/ImDreamer/analyzer-p10/-/tree/main/EnergyAnalyzer)

EnergyAnalyzer is a collection of Roslyn Analyzers that can be used to improve the energy efficiency of C# programs.

This project was developed during a masters thesis at Aalborg University, Denmark. The project can be found [here (PDF)](https://gitlab.com/ImDreamer/analyzer-p10/-/blob/main/Improving_C__Programs_for_Energy_Efficiency.pdf).

The suggestions and code fixes implemented in EnergyAnalyzer are based on [Benchmarking C# for Energy Consumption (PDF)](https://gitlab.com/ImDreamer/CsharpRAPL/-/blob/main/Benchmarking%20C%23%20for%20Energy.pdf)

The results gathered in both projects are found using [CsharpRAPL](https://gitlab.com/ImDreamer/CsharpRAPL) which is a framework created to benchmark different language constructs in C#.

Further benchmarks can be found [here](https://gitlab.com/ImDreamer/benchmarks-p10) or in the ['3rd Party Example Projects' folder](https://gitlab.com/ImDreamer/analyzer-p10/-/tree/main/3rd%20Party%20Example%20Projects). 

You can get the analyzer from NuGet [here](https://www.nuget.org/packages/EnergyAnalyzer/).

## Features

The analyzer supports giving suggestions regarding the following topics, with code fixes for some of them (specified):

- Collections:
  - Array (Has code fix)
  - List (Has code fix)
  - Dictionary (Has code fix)
  - Set (Has code fix)
- Datatypes:
  - Floating Point Numbers (Has code fix)
  - Integer Numbers (Has code fix)
  - String Concatenation
  - Unboxing (Has code fix)
- Exceptions:
  - Efficient Exceptions (Has code fix)
  - Replacing Exceptions
- Invocations:
  - Lambda Expressions (Has code fix)
  - Reflection (Has code fix)
- LINQ:
  - Replacing LINQ
- Loops:
  - Efficient Loops (Has code fix)
- Object:
  - Class vs. Struct (Has code fix)
- Selection:
  - If vs. Switch (Has code fix)

## Installation

To install the package, run the following command:

### Install from NuGet

#### Nuget Manager
Select the `NuGet Package Manager` from the `Tools` menu.

Search for `EnergyAnalyzer` and click the `Install` button.

#### Nuget Console
You can install the package from NuGet by one of the following ways:
```
Install-Package EnergyAnalyzer
```

#### dotnet CLI
```
dotnet add package EnergyAnalyzer
```
#### csproj reference
```
<PackageReference Include="EnergyAnalyzer" Version="0.1.2">
  <PrivateAssets>all</PrivateAssets>
  <IncludeAssets>runtime; build; native; contentfiles; analyzers</IncludeAssets>
</PackageReference>
```

## Examples
Examples of the changes the tool can make can be seen in the folder "3rd Party Example Projects", where we have used the analyzer to create various versions of the programs.
Such that projects named `CodeFixes` are the ones that have been changed by the automatic changes and `All` is where both code fixes and suggestions have been implemented.

Provided is also tasks and examples used for a usability test which resides [here](https://gitlab.com/ImDreamer/EnergyAnalyzerUsabilityTest).

## License
MIT License

## Errors
When building your solution you might encounter warnings similar to the one below:

```
 An instance of analyzer EnergyAnalyzer cannot be created from ... : Could not load file or assembly ... or one of its dependencies. The system cannot find the file specified..
```
This warning does not mean that the analyzer is not working correctly.