﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Exception; 

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class MoreEfficientExceptionAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "ReplaceInefficientException";

	private static readonly DiagnosticDescriptor ReplaceInefficientException = new(DiagnosticId,
		"Replace inefficient exception",
		"Using the base Exception class is more efficient than DivideByZeroException or ArgumentException.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(ReplaceInefficientException);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.CatchDeclaration);
	}

	private void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (IsInefficientException((CatchDeclarationSyntax)context.Node)) {
			context.ReportDiagnostic(Diagnostic.Create(ReplaceInefficientException, context.Node.GetLocation()));
		}
	}

	private bool IsInefficientException(CatchDeclarationSyntax catchDeclaration) {
		string[] relevantExceptionTypes = { "DivideByZeroException", "ArgumentException" };
		if (catchDeclaration.Type is not IdentifierNameSyntax) {
			return false;
		}
		if (relevantExceptionTypes.Contains(((IdentifierNameSyntax)catchDeclaration.Type).Identifier.Text)) {
			return true;
		}
		
		return false;
	}
}