﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Exception;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class ReplaceExceptionAnalyzer : DiagnosticAnalyzer {
	private const string DiagnosticId = "ReplaceException";

	private static readonly DiagnosticDescriptor ReplaceException = new(DiagnosticId,
		"Replace exception",
		"Removing try-catch blocks or using if statements to check for exceptions is more efficient if possible.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(ReplaceException);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.TryStatement);
	}

	private void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		context.ReportDiagnostic(Diagnostic.Create(ReplaceException,
			((TryStatementSyntax)context.Node).TryKeyword.GetLocation()));
	}
}