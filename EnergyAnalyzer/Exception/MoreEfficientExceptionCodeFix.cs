﻿using System.Collections.Immutable;
using System.Composition;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace EnergyAnalyzer.Exception;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(MoreEfficientExceptionCodeFix)), Shared]
public class MoreEfficientExceptionCodeFix : CodeFixProvider {
	private const string Title = "Make exception more efficient";

	public sealed override ImmutableArray<string> FixableDiagnosticIds =>
		ImmutableArray.Create(MoreEfficientExceptionAnalyzer.DiagnosticId);

	public override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

	public sealed override async Task RegisterCodeFixesAsync(CodeFixContext context) {
		SyntaxNode root = (await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false))!;

		Diagnostic diagnostic = context.Diagnostics.First();
		TextSpan diagnosticSpan = diagnostic.Location.SourceSpan;

		// Find the catch statement identified by the diagnostic.
		CatchDeclarationSyntax catchStatement = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<CatchDeclarationSyntax>()
			.First();

		// Register a code action that will invoke the fix.
		context.RegisterCodeFix(
			CodeAction.Create(
				Title,
				c => MakeEfficient(context.Document, catchStatement, c),
				nameof(MoreEfficientExceptionCodeFix)),
			diagnostic);
	}

	private static async Task<Document> MakeEfficient(Document document, CatchDeclarationSyntax catchStatement,
		CancellationToken cancellationToken) {
		CatchDeclarationSyntax newCatchStatement = SyntaxFactory.CatchDeclaration(catchStatement.OpenParenToken,
				SyntaxFactory.ParseTypeName("Exception"), catchStatement.Identifier, catchStatement.CloseParenToken).NormalizeWhitespace()
			.WithTriviaFrom(catchStatement);

		SyntaxNode root = (await document.GetSyntaxRootAsync(cancellationToken))!;

		SyntaxNode newRoot = root.ReplaceNode(catchStatement, newCatchStatement);

		return document.WithSyntaxRoot(newRoot);
	}
}