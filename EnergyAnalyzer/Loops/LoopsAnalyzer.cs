﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Loops;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class LoopsAnalyzer : DiagnosticAnalyzer {
	internal const string DiagnosticId = "PreferForEachOverForLoop";

	private static readonly DiagnosticDescriptor LoopUsage = new(DiagnosticId,
		"Prefer foreach over for loop",
		"When iterating through all element sequentially it has shown to be more efficient to use foreach rather than using for loops.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);


	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(LoopUsage);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.Block);
	}

	private static void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		foreach (StatementSyntax statement in ((BlockSyntax)context.Node).Statements) {
			switch (statement) {
				case ForStatementSyntax forStatement:
					AnalyzeForStatement(context, forStatement);
					break;
			}
		}
	}

	private static void AnalyzeForStatement(SyntaxNodeAnalysisContext context, ForStatementSyntax forStatement) {
		if (forStatement.Condition is BinaryExpressionSyntax condition) {
			if (condition.Left is IdentifierNameSyntax && condition.Right is LiteralExpressionSyntax) {
				return;
			}

			if (condition.Right is IdentifierNameSyntax && condition.Left is LiteralExpressionSyntax) {
				return;
			}
		}
		else {
			return;
		}

		MemberAccessExpressionSyntax memberAccess;
		if (condition.Left is MemberAccessExpressionSyntax identLeft) {
			memberAccess = identLeft;
		}
		else if (condition.Right is MemberAccessExpressionSyntax identRight) {
			memberAccess = identRight;
		}
		else {
			return;
		}

		if (memberAccess.Name.Identifier.ValueText is not ("Length" or "Count")) {
			return;
		}

		var block = (BlockSyntax?)forStatement.ChildNodes().FirstOrDefault(node => node.IsKind(SyntaxKind.Block));

		if (block == null) {
			return;
		}

		foreach (IfStatementSyntax ifStatement in block.DescendantNodes().OfType<IfStatementSyntax>()) {
			List<BinaryExpressionSyntax> conditions =
				ifStatement.Condition.DescendantNodes().OfType<BinaryExpressionSyntax>().ToList();

			if (ifStatement.Condition is BinaryExpressionSyntax cond)
				conditions.Add(cond);

			if (ReferencesIndexer(conditions, forStatement.Declaration.Variables)) {
				return;
			}
		}

		if (ReferencesIndexer(block.DescendantNodes().OfType<AssignmentExpressionSyntax>().ToList(),
			forStatement.Declaration.Variables)) {
			return;
		}

		if (ReferencesIndexer(block.DescendantNodes().OfType<ElementAccessExpressionSyntax>().ToList(),
			forStatement.Declaration.Variables)) {
			return;
		}

		if (ReferencesIndexer(block.DescendantNodes().OfType<PostfixUnaryExpressionSyntax>().ToList(),
			forStatement.Declaration.Variables)) {
			return;
		}

		if (ReferencesIndexer(block.DescendantNodes().OfType<PrefixUnaryExpressionSyntax>().ToList(),
			forStatement.Declaration.Variables)) {
			return;
		}

		switch (forStatement.Condition) {
			case BinaryExpressionSyntax binaryExpressionSyntax:

				if (forStatement.Declaration is null) {
					break;
				}

				if (binaryExpressionSyntax.Left is IdentifierNameSyntax leftName &&
				    forStatement.Declaration.Variables.Any(var =>
					    var.Identifier.Text == leftName.Identifier.Text)) {
					CheckAccess(forStatement, block, context, leftName.Identifier.Text);
				}
				else if (binaryExpressionSyntax.Right is IdentifierNameSyntax rightName &&
				         forStatement.Declaration.Variables.Any(var =>
					         var.Identifier.Text == rightName.Identifier.Text)) {
					CheckAccess(forStatement, block, context, rightName.Identifier.Text);
				}

				break;
		}
	}

	//TODO: Maybe instead of using the variables we should use the the condition
	private static bool ReferencesIndexer(List<AssignmentExpressionSyntax> assignments,
		SeparatedSyntaxList<VariableDeclaratorSyntax> variables) {
		foreach (AssignmentExpressionSyntax condition in assignments) {
			if (condition.Left is IdentifierNameSyntax leftName &&
			    variables.Any(var => var.Identifier.Text == leftName.Identifier.Text)) {
				return true;
			}
		}

		return false;
	}

	private static bool ReferencesIndexer(List<ElementAccessExpressionSyntax> elementAccess,
		SeparatedSyntaxList<VariableDeclaratorSyntax> variables) {
		foreach (ElementAccessExpressionSyntax accessExpression in elementAccess) {
			bool hasRef = ReferencesIndexer(
				accessExpression.ArgumentList.DescendantNodes().OfType<BinaryExpressionSyntax>().ToList(), variables);
			if (hasRef) {
				return true;
			}
		}

		return false;
	}

	private static bool ReferencesIndexer(List<PrefixUnaryExpressionSyntax> prefixExpressions,
		SeparatedSyntaxList<VariableDeclaratorSyntax> variables) {
		foreach (PrefixUnaryExpressionSyntax condition in prefixExpressions) {
			if (condition.Operand is IdentifierNameSyntax leftName &&
			    variables.Any(var => var.Identifier.Text == leftName.Identifier.Text)) {
				return true;
			}
		}

		return false;
	}

	private static bool ReferencesIndexer(List<PostfixUnaryExpressionSyntax> postfixExpressions,
		SeparatedSyntaxList<VariableDeclaratorSyntax> variables) {
		foreach (PostfixUnaryExpressionSyntax condition in postfixExpressions) {
			if (condition.Operand is IdentifierNameSyntax leftName &&
			    variables.Any(var => var.Identifier.Text == leftName.Identifier.Text)) {
				return true;
			}
		}

		return false;
	}

	private static bool ReferencesIndexer(List<BinaryExpressionSyntax> conditions,
		SeparatedSyntaxList<VariableDeclaratorSyntax> variables) {
		foreach (BinaryExpressionSyntax condition in conditions) {
			if (condition.Left is IdentifierNameSyntax leftName &&
			    variables.Any(var => var.Identifier.Text == leftName.Identifier.Text)) {
				return true;
			}

			if (condition.Right is IdentifierNameSyntax rightName &&
			    variables.Any(var => var.Identifier.Text == rightName.Identifier.Text)) {
				return true;
			}
		}

		return false;
	}


	private static void CheckAccess(ForStatementSyntax forStatement, BlockSyntax block,
		SyntaxNodeAnalysisContext context, string identifierText) {
		foreach (ElementAccessExpressionSyntax access in
			block.DescendantNodes().OfType<ElementAccessExpressionSyntax>()) {
			if (access.Parent is AssignmentExpressionSyntax assignment) {
				if (assignment.Left == access) {
					continue;
				}
			}

			if (access.ArgumentList.Arguments[0].Expression is not IdentifierNameSyntax identifierName) {
				continue;
			}

			if (identifierName.Identifier.Text == identifierText) {
				context.ReportDiagnostic(Diagnostic.Create(LoopUsage, forStatement.ForKeyword.GetLocation()));
			}
		}
	}
}