﻿using System.Collections.Immutable;
using System.Composition;
using System.Text.RegularExpressions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace EnergyAnalyzer.Loops;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(LoopsCodeFixProvider)), Shared]
public class LoopsCodeFixProvider : CodeFixProvider {
	public override ImmutableArray<string> FixableDiagnosticIds => ImmutableArray.Create(LoopsAnalyzer.DiagnosticId);

	public override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

	public override Task RegisterCodeFixesAsync(CodeFixContext context) {
		Diagnostic diagnostic = context.Diagnostics.First();

		CodeAction action = CodeAction.Create(
			"Make foreach loop",
			async c => await MakeForEachLoop(context, diagnostic.Location.SourceSpan, c),
			nameof(LoopsCodeFixProvider));
		context.RegisterCodeFix(action, diagnostic);

		return Task.CompletedTask;
	}

	private static async Task<Document> MakeForEachLoop(CodeFixContext context,
		TextSpan diagnosticSpan,
		CancellationToken cancellationToken) {
		SyntaxNode root = (await context.Document.GetSyntaxRootAsync(cancellationToken))!;
		ForStatementSyntax forLoop = (ForStatementSyntax)root.FindNode(diagnosticSpan);


		if (forLoop.Condition is not BinaryExpressionSyntax condition) {
			return context.Document;
		}

		MemberAccessExpressionSyntax memberAccess;
		if (condition.Left is MemberAccessExpressionSyntax identLeft) {
			memberAccess = identLeft;
		}
		else if (condition.Right is MemberAccessExpressionSyntax identRight) {
			memberAccess = identRight;
		}
		else {
			return context.Document;
		}

		if (memberAccess.Name.Identifier.ValueText is not ("Length" or "Count")) {
			return context.Document;
		}

		SemanticModel? model = await context.Document.GetSemanticModelAsync(cancellationToken);

		string itemName = CheckIfNameIsValid("item", model.LookupSymbols(forLoop.SpanStart)
			.Select(symbol => symbol.Name).Where(symbol => symbol.Contains("item"))
			.ToList());

		ForEachStatementSyntax newForEach = SyntaxFactory.ForEachStatement(
			SyntaxFactory.ParseToken("foreach"),
			forLoop.OpenParenToken,
			SyntaxFactory.ParseTypeName("var"),
			SyntaxFactory.Identifier(itemName),
			SyntaxFactory.ParseToken("in"),
			SyntaxFactory.ParseExpression(memberAccess.Expression.GetText().ToString()),
			forLoop.CloseParenToken,
			forLoop.Statement);

		var placesToUpdate = new Dictionary<ElementAccessExpressionSyntax, IdentifierNameSyntax>();
		foreach (ElementAccessExpressionSyntax var in newForEach.Statement.DescendantNodes()
			.OfType<ElementAccessExpressionSyntax>()) {
			if (var.Expression.GetText().ToString() == memberAccess.Expression.GetText().ToString()) {
				placesToUpdate.Add(var,
					SyntaxFactory.IdentifierName(SyntaxFactory.Identifier(itemName)).WithTriviaFrom(var));
			}
		}

		newForEach = newForEach.ReplaceNodes(placesToUpdate.Keys, (oldNode, _) => placesToUpdate[oldNode])
			.WithTriviaFrom(forLoop);

		SyntaxNode newRoot = root.ReplaceNode(forLoop, newForEach).WithTriviaFrom(forLoop);

		return context.Document.WithSyntaxRoot(newRoot.WithTriviaFrom(root));

	}

	private static string CheckIfNameIsValid(string name, List<string> symbols, int iteration = 1) {
		while (true) {
			if (!symbols.Contains(name)) {
				return name;
			}

			name = Regex.Split(name, @"\d+")[0] + (symbols.Count + iteration);
			iteration++;
		}
	}
}