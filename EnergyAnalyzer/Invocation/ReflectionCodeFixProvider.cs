﻿using System.Collections.Immutable;
using System.Composition;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

namespace EnergyAnalyzer.Invocation;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(ReflectionCodeFixProvider)), Shared]
public class ReflectionCodeFixProvider : CodeFixProvider {
	public override ImmutableArray<string> FixableDiagnosticIds =>
		ImmutableArray.Create(ReflectionAnalyzer.DiagnosticId);

	public override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

	public override Task RegisterCodeFixesAsync(CodeFixContext context) {
		Diagnostic diagnostic = context.Diagnostics.First();

		CodeAction action = CodeAction.Create(
			"Make delegate invocation",
			async c => await MakeDelegateInvocation(context.Document, diagnostic.Location.SourceSpan, c),
			nameof(ReflectionCodeFixProvider));
		context.RegisterCodeFix(action, diagnostic);

		return Task.CompletedTask;
	}

	private static async Task<Document> MakeDelegateInvocation(Document document, TextSpan span,
		CancellationToken cancellationToken) {
		SyntaxNode? root = await document.GetSyntaxRootAsync(cancellationToken);

		if (root == null) {
			return document;
		}

		InvocationExpressionSyntax? invocationExpression =
			root.FindNode(span).FirstAncestorOrSelf<InvocationExpressionSyntax>();

		if (invocationExpression == null) {
			return document;
		}

		//Get the name of the field which contains the MethodInfo
		string methodInfoFieldName =
			((IdentifierNameSyntax)((MemberAccessExpressionSyntax)invocationExpression.Expression).Expression)
			.Identifier.Text;

		//Get the MethodInfo field
		FieldDeclarationSyntax? methodInfoField = FindField(root, methodInfoFieldName);

		if (methodInfoField == null) {
			return document;
		}

		//From the methodInfo field, we can get the name of the method we are trying to invoke
		string methodNameToInvoke = GetMethodToCall(methodInfoField);

		//From the name we can again find the method
		MethodDeclarationSyntax? methodToInvoke = FindMethod(root, methodNameToInvoke);

		if (methodToInvoke == null) {
			return document;
		}

		//Construct the parameters for the delegate type
		var parameters = "";
		foreach (ParameterSyntax parameter in methodToInvoke.ParameterList.Parameters) {
			if (methodToInvoke.ParameterList.Parameters.IndexOf(parameter) == 0) {
				parameters = ",";
			}

			parameters += parameter.Type?.GetText();

			if (methodToInvoke.ParameterList.Parameters.IndexOf(parameter) !=
			    methodToInvoke.ParameterList.Parameters.Count - 1) {
				parameters += ",";
			}
		}

		//Depending on if the method has a return type or not we use either Action or Func and emplace the parameters
		//along with the instance object
		TypeSyntax delegateType = SyntaxFactory.ParseTypeName(methodToInvoke.ReturnType.ToString().ToLower() == "void"
			? $"Action<{GetOwningClass(methodToInvoke)!.Identifier.Text + parameters}>"
			: $"Func<{GetOwningClass(methodToInvoke)!.Identifier.Text + ", " + parameters + methodToInvoke.ReturnType.GetText()}>");

		TypeDeclarationSyntax parentClass = GetOwningClass(invocationExpression)!;

		FieldDeclarationSyntax newDelegateField =
			CreateDelegateField(delegateType, methodInfoFieldName, methodInfoField);

		TypeDeclarationSyntax newClass = parentClass.AddMembers(newDelegateField);
		InvocationExpressionSyntax newInvocation =
			CreateInvocationExpression(invocationExpression, methodInfoFieldName);

		//Here it is important that these are two separate calls
		root = root.ReplaceNode(parentClass, newClass);
		//We also need to find the node again since we update the class above
		root = root.ReplaceNode(root.FindNode(invocationExpression.Span), newInvocation);
		return document.WithSyntaxRoot(root);
	}

	private static FieldDeclarationSyntax? FindField(SyntaxNode root, string methodInfoName) {
		return root.DescendantNodes().OfType<FieldDeclarationSyntax>().FirstOrDefault(x =>
			x.Declaration.Variables.Any(y => y.Identifier.Text == methodInfoName));
	}

	private static MethodDeclarationSyntax? FindMethod(SyntaxNode root, string methodInfoName) {
		return root.DescendantNodes().OfType<MethodDeclarationSyntax>().FirstOrDefault(x =>
			x.Identifier.Text == methodInfoName);
	}

	private static string GetMethodToCall(FieldDeclarationSyntax field) {
		ArgumentSyntax test = ((InvocationExpressionSyntax)field.Declaration.Variables.First().Initializer!.Value)
			.ArgumentList
			.Arguments
			.First();

		return test.Expression switch {
			InvocationExpressionSyntax invocationExpression => invocationExpression.ArgumentList.Arguments[0]
				.ToString()
				.Split(".")[^1],
			LiteralExpressionSyntax literalExpression => literalExpression.Token.Text.Replace("\"", ""),
			_ => throw new ArgumentException($"{test.Expression.GetType().Name} is not supported.")
		};
	}

	private static TypeDeclarationSyntax? GetOwningClass(SyntaxNode? root) {
		while (root?.Parent != null) {
			if (root.Parent is ClassDeclarationSyntax ||
			    root.Parent is StructDeclarationSyntax) {
				return (TypeDeclarationSyntax?)root.Parent;
			}

			root = root.Parent;
		}

		return null;
	}

	private static InvocationExpressionSyntax CreateInvocationExpression(
		InvocationExpressionSyntax invocationExpression,
		string fieldName) {
		var parameters = new List<ArgumentSyntax>
			{ SyntaxFactory.Argument(invocationExpression.ArgumentList.Arguments[0].Expression) };
		ArgumentSyntax firstArgument = invocationExpression.ArgumentList.Arguments[1];

		//If the first "real" argument isn't empty we need unpack it and get the parameters
		if (!firstArgument.GetText().ToString().Contains("Array.Empty") &&
		    firstArgument.GetText().ToString() != "null") {
			for (var index = 1; index < invocationExpression.ArgumentList.Arguments.Count; index++) {
				ArgumentSyntax parameter = invocationExpression.ArgumentList.Arguments[index];

				if (parameter.Expression is ArrayCreationExpressionSyntax arrayCreation) {
					foreach (ExpressionSyntax element in arrayCreation.Initializer!.Expressions) {
						parameters.Add(SyntaxFactory.Argument(element));
						index++;
					}
				}
				else {
					parameters.Add(parameter);
				}
			}
		}


		return SyntaxFactory.InvocationExpression(
			SyntaxFactory.IdentifierName($"{fieldName}Delegate"),
			SyntaxFactory.ArgumentList(
				SyntaxFactory.SeparatedList(parameters))).NormalizeWhitespace().WithTriviaFrom(invocationExpression);
	}

	private static FieldDeclarationSyntax CreateDelegateField(TypeSyntax delegateType, string fieldName,
		FieldDeclarationSyntax oldField) {
		VariableDeclarationSyntax declarationSyntax = SyntaxFactory.VariableDeclaration(delegateType,
			SyntaxFactory.SeparatedList(new[] {
				SyntaxFactory.VariableDeclarator(SyntaxFactory.Identifier(fieldName + "Delegate"))
					.WithInitializer(SyntaxFactory.EqualsValueClause(
						SyntaxFactory.CastExpression(delegateType, SyntaxFactory.InvocationExpression(
							SyntaxFactory.MemberAccessExpression(SyntaxKind.SimpleMemberAccessExpression,
								SyntaxFactory.IdentifierName("Delegate"), SyntaxFactory.IdentifierName(
									"CreateDelegate")),
							SyntaxFactory.ArgumentList(
								SyntaxFactory.SeparatedList(
									new[] {
										SyntaxFactory.Argument(SyntaxFactory.TypeOfExpression(delegateType)),
										SyntaxFactory.Argument(SyntaxFactory.IdentifierName(fieldName))
									}))))))
			}));

		FieldDeclarationSyntax newField = SyntaxFactory.FieldDeclaration(declarationSyntax).WithModifiers(
			SyntaxFactory.TokenList(SyntaxFactory.Token(SyntaxKind.PrivateKeyword),
				SyntaxFactory.Token(SyntaxKind.StaticKeyword), SyntaxFactory.Token(SyntaxKind.ReadOnlyKeyword)));

		return newField.NormalizeWhitespace().WithTriviaFrom(oldField);
	}
}