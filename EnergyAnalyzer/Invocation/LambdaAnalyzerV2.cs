﻿using System.Collections.Concurrent;
using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Invocation;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class LambdaAnalyzerV2 : DiagnosticAnalyzer {
	public const string DiagnosticId = "RemoveClosure";

	private static readonly DiagnosticDescriptor RemoveClosure = new(DiagnosticId,
		"Remove closure",
		"Using parameters in lambda expressions is usually more efficient than accessing outside variables directly.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(RemoveClosure);

	public static readonly ConcurrentDictionary<string, List<IdentifierNameSyntax>>
		MapFromLambdaToIdentifiersToFix = new();

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.ParenthesizedLambdaExpression);
	}

	private void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (IsClosure((ParenthesizedLambdaExpressionSyntax)context.Node, context.SemanticModel)) {
			context.ReportDiagnostic(Diagnostic.Create(RemoveClosure, context.Node.GetLocation()));
		}
	}

	private bool IsClosure(ParenthesizedLambdaExpressionSyntax node, SemanticModel semanticModel) {
		MapFromLambdaToIdentifiersToFix[node.ToString()] = new List<IdentifierNameSyntax>();
		
		var identifiers = node.FindNode(node.Span).DescendantNodes().OfType<IdentifierNameSyntax>().ToArray();

		if (identifiers.Length == 0) {
			return false;
		}

		// Perform data flow analysis on the lambda expression found.
		DataFlowAnalysis dataFlowAnalysis = semanticModel.AnalyzeDataFlow(node)!;

		// See if the variables declared within the lambda expression contains the variable we are looking for
		// In which case everything is fine
		foreach (IdentifierNameSyntax identifier in identifiers) {
			if (!dataFlowAnalysis.VariablesDeclared.Contains(semanticModel.GetSymbolInfo(identifier).Symbol!)) {
				MapFromLambdaToIdentifiersToFix[node.ToString()].Add(identifier);
			}
		}

		if (MapFromLambdaToIdentifiersToFix[node.ToString()].Count > 0) {
			return true;
		}

		return false;
	}
}