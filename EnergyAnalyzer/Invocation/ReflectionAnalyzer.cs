﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Invocation;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class ReflectionAnalyzer : DiagnosticAnalyzer {
	internal const string DiagnosticId = "ReplaceOrWrapReflection";

	private static readonly DiagnosticDescriptor ReplaceOrWrapReflection = new(DiagnosticId,
		"Replace or Wrap Reflection",
		"Wrapping reflection invocation in a delegate is more efficient than just using reflection. " +
		"If invoking a method directly is possible instead, that is the most efficient.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(ReplaceOrWrapReflection);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.SimpleMemberAccessExpression);
	}

	private void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (IsReflectionInvocation((MemberAccessExpressionSyntax)context.Node, context.SemanticModel)) {
			context.ReportDiagnostic(Diagnostic.Create(ReplaceOrWrapReflection, context.Node.GetLocation()));
		}
	}

	private static bool IsReflectionInvocation(MemberAccessExpressionSyntax invocation, SemanticModel semanticModel) {
		if (invocation.Name.Identifier.Text == "Invoke" && invocation.Expression is IdentifierNameSyntax ident &&
		    semanticModel.GetSymbolInfo(ident).Symbol is IFieldSymbol symbol && symbol.Type.Name == "MethodInfo") {
			return true;
		}

		return false;
	}
}