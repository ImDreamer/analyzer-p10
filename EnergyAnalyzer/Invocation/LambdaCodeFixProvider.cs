﻿using System.Collections.Immutable;
using System.Composition;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace EnergyAnalyzer.Invocation;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(LambdaCodeFixProvider)), Shared]
public class LambdaCodeFixProvider : CodeFixProvider {
	public override ImmutableArray<string> FixableDiagnosticIds =>
		ImmutableArray.Create(LambdaAnalyzerV2.DiagnosticId);

	public override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

	public override Task RegisterCodeFixesAsync(CodeFixContext context) {
		Diagnostic diagnostic = context.Diagnostics.First();
		CodeAction action = CodeAction.Create(
			"Extract variables to parameters",
			c => MakeLambdaWithoutClosure(context, c),
			nameof(LambdaCodeFixProvider));
		context.RegisterCodeFix(action, diagnostic);
		return Task.CompletedTask;
	}

	private static async Task<Document> MakeLambdaWithoutClosure(CodeFixContext context, CancellationToken c) {
		Document document = context.Document;
		SyntaxNode? root = await document.GetSyntaxRootAsync(c);
		Diagnostic diagnostic = context.Diagnostics.First();
		Microsoft.CodeAnalysis.Text.TextSpan diagnosticSpan = diagnostic.Location.SourceSpan;

		ParenthesizedLambdaExpressionSyntax lambdaExpression =
			root!.FindToken(diagnosticSpan.Start).Parent!
				.AncestorsAndSelf().OfType<ParenthesizedLambdaExpressionSyntax>().FirstOrDefault()!;

		VariableDeclarationSyntax? ancestor;
		if (lambdaExpression.Parent?.Parent?.Parent is VariableDeclarationSyntax v) {
			ancestor = v;
		}
		else {
			// TODO: Find the original declaration of the lambda expression and change the type there
			return document;
		}

		GenericNameSyntax? genericType = ancestor.Type as GenericNameSyntax;
		GenericNameSyntax newGenericType = genericType!;
		newGenericType = newGenericType.WithTypeArgumentList(
			newGenericType.TypeArgumentList.WithArguments(
				newGenericType.TypeArgumentList.Arguments.RemoveAt(newGenericType.TypeArgumentList.Arguments.Count -
				                                                   1)));
		ParenthesizedLambdaExpressionSyntax newLambdaExpression = lambdaExpression;

		foreach (IdentifierNameSyntax identifierNameSyntax in LambdaAnalyzerV2.MapFromLambdaToIdentifiersToFix[
			lambdaExpression.ToString()]) {
			string variableName = identifierNameSyntax.Identifier.Text;
			ILocalSymbol? symbol = document.GetSemanticModelAsync().Result!
				.LookupSymbols(diagnostic.Location.SourceSpan.Start).First(a =>
					a.Name == variableName) as ILocalSymbol;

			ParameterSyntax parameter = SyntaxFactory.Parameter(
				new SyntaxList<AttributeListSyntax>(), new SyntaxTokenList(), null,
				SyntaxFactory.Identifier(variableName), null);

			string typeName = symbol!.Type.ToString()!;

			newGenericType = newGenericType.AddTypeArgumentListArguments(SyntaxFactory.ParseTypeName(typeName));

			newLambdaExpression = SyntaxFactory.ParenthesizedLambdaExpression()
				.WithBlock(newLambdaExpression.Block)
				.WithAsyncKeyword(newLambdaExpression.AsyncKeyword)
				.WithParameterList(newLambdaExpression.ParameterList.AddParameters(parameter))
				.WithArrowToken(newLambdaExpression.ArrowToken)
				.WithBody(newLambdaExpression.Body)
				.WithModifiers(newLambdaExpression.Modifiers)
				.WithAttributeLists(newLambdaExpression.AttributeLists)
				.WithExpressionBody(newLambdaExpression.ExpressionBody)
				.WithReturnType(newLambdaExpression.ReturnType)
				.WithTriviaFrom(newLambdaExpression).NormalizeWhitespace();
		}

		newGenericType = newGenericType.AddTypeArgumentListArguments(
			SyntaxFactory.ParseTypeName(genericType!.TypeArgumentList.Arguments[^1].ToString()));

		Dictionary<SyntaxNode, SyntaxNode> replacements = new Dictionary<SyntaxNode, SyntaxNode>();

		replacements.Add(genericType, newGenericType);
		replacements.Add(lambdaExpression, newLambdaExpression);

		root = root.ReplaceNodes(new List<SyntaxNode> { genericType, lambdaExpression },
			(oldNode, _) => replacements[oldNode].WithTriviaFrom(oldNode));

		return document.WithSyntaxRoot(root);
	}
}