﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Linq;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class LinqAnalyzer : DiagnosticAnalyzer {
	private const string DiagnosticId = "LINQUsage";

	private static readonly DiagnosticDescriptor LinqUsage = new(DiagnosticId,
		"Don't use LINQ",
		"Replace LINQ as LINQ has shown to be significantly less efficient than any alternative.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	private static readonly HashSet<string> LinqMethods = new() {
		"Select", "SelectMany", "Where", "GroupJoin", "Join", "All", "Any", "DefaultIfEmpty",
		"Distinct", "Except", "Intersect", "Union", "OrderBy", "OrderByDescending", "ThenBy", "ThenByDescending",
		"Reverse", "GroupBy", "Aggregate", "Average", "Count", "LongCount", "Max", "Min", "MaxBy", "MinBy", "Sum",
		"Cast", "OfType", "ElementAt", "ElementAtOrDefault", "First", "FirstOrDefault", "Last", "LastOrDefault",
		"Single", "SingleOrDefault", "Skip", "SkipWhile", "Take", "TakeWhile", "TakeLast", "ToList", "SequenceEqual",
		"ToArray", "ToDictionary", "Chunk", "Prepend", "Zip"
	};

	private static readonly HashSet<string> BaseClassesToIgnore = new() { "string", "String" };

	//TODO: The following are not yet supported due to the names being too generic:
	//"Contains", "Concat", "Append",

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(LinqUsage);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.Block);
	}

	private static void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		foreach (StatementSyntax statement in ((BlockSyntax)context.Node).Statements) {
			switch (statement) {
				case LocalDeclarationStatementSyntax localDeclaration: {
					foreach (VariableDeclaratorSyntax variable in localDeclaration.Declaration.Variables) {
						if (variable.Initializer?.Value is not InvocationExpressionSyntax invocationExpression) {
							continue;
						}

						if (UsesLinq(invocationExpression)) {
							context.ReportDiagnostic(Diagnostic.Create(LinqUsage, invocationExpression.GetLocation()));
						}
					}

					break;
				}
				case ExpressionStatementSyntax expressionStatement: {
					if (expressionStatement.Expression is not InvocationExpressionSyntax
						invocationExpression) {
						return;
					}

					if (UsesLinq(invocationExpression)) {
						context.ReportDiagnostic(Diagnostic.Create(LinqUsage, invocationExpression.GetLocation()));
					}

					break;
				}
			}
		}
	}

	private static bool UsesLinq(InvocationExpressionSyntax expressionSyntax) {
		if (expressionSyntax.Expression is MemberAccessExpressionSyntax memberAccess) {
			if (BaseClassesToIgnore.Contains(memberAccess.Expression.ToString())) {
				return false;
			}
		}

		foreach (SyntaxNode child in expressionSyntax.Expression.DescendantNodes().Skip(1)) {
			if (!child.IsKind(SyntaxKind.IdentifierName)) {
				continue;
			}

			var identifier = (IdentifierNameSyntax)child;
			if (LinqMethods.Contains(identifier.Identifier.Text)) {
				return true;
			}
		}

		return false;
	}
}