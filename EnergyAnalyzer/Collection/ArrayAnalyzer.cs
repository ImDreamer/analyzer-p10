﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Collection;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class ArrayAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "MakeArray";

	private static readonly string[] RelevantTypes = {
		"List", "ImmutableList", "ArrayList", "LinkedList", "ImmutableArray", "IEnumerable"
	};

	private static readonly DiagnosticDescriptor MakeArray = new(DiagnosticId,
		"Use array",
		"Using an array is usually more efficient than other types of lists.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeArray);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.LocalDeclarationStatement);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.FieldDeclaration);
	}

	private static void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (CanBeArray(context.Node)) {
			context.ReportDiagnostic(Diagnostic.Create(MakeArray, context.Node.GetLocation()));
		}
	}

	internal static bool CanBeArray(SyntaxNode node) {
		TypeSyntax type;

		switch (node) {
			case FieldDeclarationSyntax fieldDeclaration: {
				type = fieldDeclaration.Declaration.Type;
				if (type is ArrayTypeSyntax ||
				    !RelevantTypes.Contains(type.GetText().ToString().Trim().Split("<")[0])) {
					return false;
				}

				if (fieldDeclaration.Declaration.Variables.Any(variableDeclaratorSyntax =>
					CheckVariable(variableDeclaratorSyntax.Initializer))) {
					return true;
				}

				break;
			}
			case LocalDeclarationStatementSyntax localDeclaration: {
				type = localDeclaration.Declaration.Type;
				if (type is ArrayTypeSyntax ||
				    !RelevantTypes.Contains(type.GetText().ToString().Trim().Split("<")[0])) {
					return false;
				}

				if (localDeclaration.Declaration.Variables.Any(variableDeclaratorSyntax =>
					CheckVariable(variableDeclaratorSyntax.Initializer))) {
					return true;
				}

				break;
			}

			case PropertyDeclarationSyntax propertyDeclaration:
				type = propertyDeclaration.Type;
				if (type is ArrayTypeSyntax ||
				    !RelevantTypes.Contains(type.GetText().ToString().Trim().Split("<")[0])) {
					return false;
				}

				if (CheckVariable(propertyDeclaration.Initializer)) {
					return true;
				}

				break;
			default:
				return false;
		}


		return false;
	}

	private static bool CheckVariable(EqualsValueClauseSyntax? equalsValueClauseSyntax) {
		if (equalsValueClauseSyntax is null) {
			return false;
		}

		if (equalsValueClauseSyntax.Value is ObjectCreationExpressionSyntax o &&
		    o.ArgumentList != null &&
		    o.ArgumentList.Arguments.Count == 1 &&
		    int.TryParse(o.ArgumentList.Arguments[0].Expression.ToString(), out _)) {
			return true;
		}

		// if it is equal to Enumerable.Range()
		if (equalsValueClauseSyntax.Value is InvocationExpressionSyntax invo &&
		    invo.ArgumentList.Arguments.Count == 2 && invo.ToString().Contains("Enumerable.Range")) {
			int.TryParse(invo.ArgumentList.Arguments[1].ToString(), out _);
			return true;
		}

		// if it is equal to Enumerable.Range().???, like when using immutable lists or arrays where we go
		// Enumerable.Range().ToImmutableArray()
		if (equalsValueClauseSyntax.Value is InvocationExpressionSyntax i &&
		    i.Expression is MemberAccessExpressionSyntax m && m.Expression is InvocationExpressionSyntax i2 &&
		    i2.ArgumentList.Arguments.Count == 2 && i2.ToString().Contains("Enumerable.Range")) {
			int.TryParse(i2.ArgumentList.Arguments[1].ToString(), out _);
			return true;
		}

		return false;
	}
}