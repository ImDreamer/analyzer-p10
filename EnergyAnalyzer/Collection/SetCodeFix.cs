﻿using System.Collections.Immutable;
using System.Composition;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace EnergyAnalyzer.Collection;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(SetCodeFix)), Shared]
public class SetCodeFix : CodeFixProvider {
	public override ImmutableArray<string> FixableDiagnosticIds =>
		ImmutableArray.Create(SetAnalyzer.DiagnosticId);

	public override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

	public override async Task RegisterCodeFixesAsync(CodeFixContext context) {
		SyntaxNode? root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

		Diagnostic diagnostic = context.Diagnostics.First();
		Microsoft.CodeAnalysis.Text.TextSpan diagnosticSpan = diagnostic.Location.SourceSpan;

		LocalDeclarationStatementSyntax? declaration = root!.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<LocalDeclarationStatementSyntax>().FirstOrDefault();
		PropertyDeclarationSyntax? property = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<PropertyDeclarationSyntax>().FirstOrDefault();
		FieldDeclarationSyntax? fieldDeclaration = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<FieldDeclarationSyntax>().FirstOrDefault();

		CodeAction action;

		if (declaration != null) {
			action = CodeAction.Create(
				"Make HashSet",
				_ => MakeHashSetAsync(root, context.Document, declaration),
				equivalenceKey: nameof(SetCodeFix));
			context.RegisterCodeFix(action, diagnostic);
		}

		if (property != null) {
			action = CodeAction.Create(
				"Make HashSet",
				_ => MakeHashSetAsync(root, context.Document, property),
				equivalenceKey: nameof(SetCodeFix));
			context.RegisterCodeFix(action, diagnostic);
		}

		if (fieldDeclaration != null) {
			action = CodeAction.Create(
				"Make HashSet",
				_ => MakeHashSetAsync(root, context.Document, fieldDeclaration),
				equivalenceKey: nameof(SetCodeFix));
			context.RegisterCodeFix(action, diagnostic);
		}
	}

	private static Task<Document> MakeHashSetAsync(SyntaxNode root, Document document, SyntaxNode declaration) {
		TypeSyntax typeName = FixVariableType(declaration);

		SeparatedSyntaxList<VariableDeclaratorSyntax> newVariableInitializers;
		SyntaxNode newRoot;

		if (declaration is not PropertyDeclarationSyntax) {
			newVariableInitializers =
				FixVariableInitializers(declaration);

			newRoot = FixRoot(typeName, newVariableInitializers, root, declaration);
		}
		else {
			newRoot = FixRootProperty(typeName, root, declaration);
		}

		return Task.FromResult(document.WithSyntaxRoot(newRoot));
	}

	private static TypeSyntax FixVariableType(SyntaxNode declaration) {
		TypeSyntax? type;
		switch (declaration) {
			case LocalDeclarationStatementSyntax l:
				type = l.Declaration.Type;
				break;
			case FieldDeclarationSyntax f:
				type = f.Declaration.Type;
				break;
			case PropertyDeclarationSyntax p:
				type = p.Type;
				break;
			default:
				// Should never happen
				type = null;
				break;
		}

		if (!type!.ToString().Contains("<")) {
			return SyntaxFactory.ParseTypeName("HashSet<object>")
				.WithLeadingTrivia(type.GetLeadingTrivia())
				.WithTrailingTrivia(type.GetTrailingTrivia());
		}

		StringBuilder relevantRegex = new StringBuilder();
		foreach (string relevantType in SetAnalyzer.RelevantTypes) {
			if (relevantType.Equals("HashSet")) {
				continue;
			}

			relevantRegex.Append("(");
			relevantRegex.Append(relevantType);
			relevantRegex.Append(")");
			relevantRegex.Append("|");
		}

		relevantRegex.Remove(relevantRegex.Length - 1, 1);

		return SyntaxFactory.ParseTypeName(Regex.Replace(type.ToString(), relevantRegex.ToString(), "HashSet"))
			.WithLeadingTrivia(type.GetLeadingTrivia()).WithTrailingTrivia(type.GetTrailingTrivia());
	}

	private static SeparatedSyntaxList<VariableDeclaratorSyntax> FixVariableInitializers(SyntaxNode declaration) {
		SeparatedSyntaxList<VariableDeclaratorSyntax> newVariableInitializers =
			new SeparatedSyntaxList<VariableDeclaratorSyntax>();

		switch (declaration) {
			case LocalDeclarationStatementSyntax l:
				foreach (VariableDeclaratorSyntax variableDeclarationVariable in l.Declaration.Variables) {
					newVariableInitializers =
						newVariableInitializers.Add(variableDeclarationVariable.WithInitializer(
							FixVariableInitializer(variableDeclarationVariable.Initializer)));
				}

				break;
			case FieldDeclarationSyntax f:
				foreach (VariableDeclaratorSyntax variableDeclarationVariable in f.Declaration.Variables) {
					newVariableInitializers =
						newVariableInitializers.Add(variableDeclarationVariable.WithInitializer(
							FixVariableInitializer(variableDeclarationVariable.Initializer)));
				}

				break;
		}

		return newVariableInitializers;
	}

	private static EqualsValueClauseSyntax? FixVariableInitializer(EqualsValueClauseSyntax? initializer) {
		if (initializer == null) {
			return initializer;
		}

		if (!initializer.ToString().Contains("<")) {
			return initializer.WithValue(SyntaxFactory.ParseExpression("new HashSet<object>()"))
				.WithLeadingTrivia(initializer.GetLeadingTrivia())
				.WithTrailingTrivia(initializer.GetTrailingTrivia());
		}

		StringBuilder relevantRegex = new StringBuilder();
		foreach (string relevantType in SetAnalyzer.RelevantTypes) {
			if (relevantType.Equals("HashSet")) {
				continue;
			}

			relevantRegex.Append("(");
			relevantRegex.Append(relevantType);
			relevantRegex.Append(")");
			relevantRegex.Append("|");
		}

		relevantRegex.Remove(relevantRegex.Length - 1, 1);

		return initializer.WithValue(
			SyntaxFactory
				.ParseExpression(Regex.Replace(initializer.Value.ToString(), relevantRegex.ToString(), "HashSet"))
				.WithLeadingTrivia(initializer.GetLeadingTrivia()).WithTrailingTrivia(initializer.GetTrailingTrivia()));
	}

	private static SyntaxNode FixRoot(TypeSyntax type, SeparatedSyntaxList<VariableDeclaratorSyntax> initializers,
		SyntaxNode root, SyntaxNode declaration) {
		switch (declaration) {
			case LocalDeclarationStatementSyntax l:
				return root.ReplaceNode(declaration,
					l.WithDeclaration(l.Declaration.WithType(type).WithVariables(initializers)));
			case FieldDeclarationSyntax f:
				return root.ReplaceNode(declaration,
					f.WithDeclaration(f.Declaration.WithType(type).WithVariables(initializers)));
			default:
				return root;
		}
	}

	private static SyntaxNode FixRootProperty(TypeSyntax type, SyntaxNode root, SyntaxNode declaration) {
		// If we are here, it is always a property and therefore this cast is ok
		PropertyDeclarationSyntax p = (PropertyDeclarationSyntax)declaration;
		return root.ReplaceNode(declaration,
			p.WithType(type).WithInitializer(FixVariableInitializer(p.Initializer)));
	}
}