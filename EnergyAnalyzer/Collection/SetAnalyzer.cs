﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Collection;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class SetAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "MakeHashSet";

	private static readonly DiagnosticDescriptor MakeHashSet = new(DiagnosticId,
		"Use HashSet",
		"Using a HashSet is usually more efficient than other types of sets.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);
	
	public static readonly string[] RelevantTypes =
		{ "HashSet", "ImmutableHashSet", "ImmutableSortedSet", "SortedSet" };

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeHashSet);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.LocalDeclarationStatement);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.FieldDeclaration);
	}

	private static void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (CanBeHashSet(context.Node)) {
			context.ReportDiagnostic(Diagnostic.Create(MakeHashSet, context.Node.GetLocation()));
		}
	}


	private static bool CanBeHashSet(SyntaxNode node) {
		string type;
		switch (node) {
			case LocalDeclarationStatementSyntax localDeclaration:
				type = localDeclaration.Declaration.Type.GetText().ToString().Trim();
				break;
			case FieldDeclarationSyntax fieldDeclaration:
				type = fieldDeclaration.Declaration.Type.GetText().ToString().Trim();
				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				type = propertyDeclaration.Type.GetText().ToString().Trim();
				break;
			default:
				return false;
		}

		return RelevantTypes.Contains(type.Split("<")[0]) && !type.StartsWith("HashSet");
	}
}