﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Collection;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class ListAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "MakeList";

	private static readonly DiagnosticDescriptor MakeList = new(DiagnosticId,
		"Use List",
		"Using a List is usually more efficient than other types of lists, when array is not easily available.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public static readonly string[] RelevantTypes =
		{ "List", "ImmutableList", "ArrayList", "LinkedList", "ImmutableArray", "IEnumerable" };

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeList);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.LocalDeclarationStatement);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.FieldDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
	}

	private void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (CanBeList(context.Node)) {
			context.ReportDiagnostic(Diagnostic.Create(MakeList, context.Node.GetLocation()));
		}
	}

	private static bool CanBeList(SyntaxNode node) {
		string type;
		switch (node) {
			case LocalDeclarationStatementSyntax localDeclaration:
				type = localDeclaration.Declaration.Type.GetText().ToString().Trim();
				break;
			case FieldDeclarationSyntax fieldDeclaration:
				type = fieldDeclaration.Declaration.Type.GetText().ToString().Trim();
				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				type = propertyDeclaration.Type.GetText().ToString().Trim();
				break;
			default:
				return false;
		}


		if (RelevantTypes.Contains(type.Split("<")[0]) &&
		    !type.StartsWith("List")) {
			return !ArrayAnalyzer.CanBeArray(node);
		}

		return false;
	}
}