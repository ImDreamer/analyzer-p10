﻿using System.Collections.Immutable;
using System.Composition;
using System.Text;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace EnergyAnalyzer.Collection;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(ArrayCodeFix)), Shared]
public class ArrayCodeFix : CodeFixProvider {
	public override ImmutableArray<string> FixableDiagnosticIds =>
		ImmutableArray.Create(ArrayAnalyzer.DiagnosticId);

	public override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

	public override async Task RegisterCodeFixesAsync(CodeFixContext context) {
		SyntaxNode? root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

		Diagnostic diagnostic = context.Diagnostics.First();
		Microsoft.CodeAnalysis.Text.TextSpan diagnosticSpan = diagnostic.Location.SourceSpan;

		LocalDeclarationStatementSyntax? declaration = root!.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<LocalDeclarationStatementSyntax>().FirstOrDefault();
		PropertyDeclarationSyntax? property = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<PropertyDeclarationSyntax>().FirstOrDefault();
		FieldDeclarationSyntax? fieldDeclaration = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<FieldDeclarationSyntax>().FirstOrDefault();

		CodeAction action;

		if (declaration != null) {
			// TODO: We do not handle multiple variable declarations at once, as that would require special case where
			// TODO: we create more declarations with different sizes
			if (declaration.Declaration.Variables.Count > 1) {
				return;
			}

			action = CodeAction.Create(
				"Make Array",
				_ => MakeArrayAsync(root, context.Document, declaration),
				equivalenceKey: nameof(ArrayCodeFix));
			context.RegisterCodeFix(action, diagnostic);
		}

		if (property != null) {
			action = CodeAction.Create(
				"Make Array",
				_ => MakeArrayAsync(root, context.Document, property),
				equivalenceKey: nameof(ArrayCodeFix));
			context.RegisterCodeFix(action, diagnostic);
		}

		if (fieldDeclaration != null) {
			// TODO: We do not handle multiple variable declarations at once, as that would require special case where
			// TODO: we create more declarations with different sizes
			if (fieldDeclaration.Declaration.Variables.Count > 1) {
				return;
			}

			action = CodeAction.Create(
				"Make Array",
				_ => MakeArrayAsync(root, context.Document, fieldDeclaration),
				equivalenceKey: nameof(ArrayCodeFix));
			context.RegisterCodeFix(action, diagnostic);
		}
	}

	private static Task<Document> MakeArrayAsync(SyntaxNode root, Document document, SyntaxNode declaration) {
		int arraySize = FindArraySize(declaration);
		if (arraySize == -1) {
			return Task.FromResult(document.WithSyntaxRoot(root));
		}

		TypeSyntax typeName = FixVariableType(declaration);

		SeparatedSyntaxList<VariableDeclaratorSyntax> newVariableInitializers;
		SyntaxNode newRoot;

		if (declaration is not PropertyDeclarationSyntax) {
			newVariableInitializers =
				FixVariableInitializers(declaration, arraySize);

			newRoot = FixRoot(typeName, newVariableInitializers, root, declaration);
		}
		else {
			newRoot = FixRootProperty(typeName, root, declaration, arraySize);
		}

		return Task.FromResult(document.WithSyntaxRoot(newRoot));
	}

	private static int FindArraySize(SyntaxNode declaration) {
		ExpressionSyntax? rightSide = null;

		switch (declaration) {
			case LocalDeclarationStatementSyntax l:
				// TODO: We only check first, because we do not handle extra rightsides
				rightSide = l.Declaration.Variables.First().Initializer?.Value;
				break;
			case PropertyDeclarationSyntax p:
				rightSide = p.Initializer?.Value;
				break;
			case FieldDeclarationSyntax f:
				// TODO: We only check first, because we do not handle extra rightsides
				rightSide = f.Declaration.Variables.First().Initializer?.Value;
				break;
		}

		if (rightSide == null) {
			return -1;
		}

		int arraySize = 0;

		switch (rightSide) {
			case ObjectCreationExpressionSyntax o:
				// TODO: If there is an analysis thing here we *know* that the number is in argument 0
				int.TryParse(o.ArgumentList!.Arguments[0].Expression.ToString(), out arraySize);
				break;
			case InvocationExpressionSyntax i:
				if (int.TryParse(i.ArgumentList.Arguments[1].ToString(), out arraySize)) {
					break;
				}

				// With how the analyzer is currently set up, this *should be* correct every time
				int.TryParse(
					((InvocationExpressionSyntax)((MemberAccessExpressionSyntax)i.Expression).Expression)
					.ArgumentList.Arguments[1].ToString(), out arraySize);

				break;
		}

		return arraySize;
	}

	private static TypeSyntax FixVariableType(SyntaxNode declaration) {
		TypeSyntax? type;
		switch (declaration) {
			case LocalDeclarationStatementSyntax l:
				type = l.Declaration.Type;
				break;
			case FieldDeclarationSyntax f:
				type = f.Declaration.Type;
				break;
			case PropertyDeclarationSyntax p:
				type = p.Type;
				break;
			default:
				// Should never happen
				type = null;
				break;
		}


		// TODO: ew
		string genericType;
		if (type!.ToString().Contains("<")) {
			genericType = type.ToString().Split("<")[1].Split(">")[0];
		}
		else {
			genericType = "object";
		}

		return SyntaxFactory.ParseTypeName($"{genericType}[]").WithLeadingTrivia(type.GetLeadingTrivia()).WithTrailingTrivia(type.GetTrailingTrivia());
	}

	private static SeparatedSyntaxList<VariableDeclaratorSyntax> FixVariableInitializers(SyntaxNode declaration,
		int arraySize) {
		SeparatedSyntaxList<VariableDeclaratorSyntax> newVariableInitializers =
			new SeparatedSyntaxList<VariableDeclaratorSyntax>();

		switch (declaration) {
			case LocalDeclarationStatementSyntax l:
				foreach (VariableDeclaratorSyntax variableDeclarationVariable in l.Declaration.Variables) {
					newVariableInitializers =
						newVariableInitializers.Add(variableDeclarationVariable.WithInitializer(
							FixVariableInitializer(variableDeclarationVariable.Initializer, arraySize)));
				}

				break;
			case FieldDeclarationSyntax f:
				foreach (VariableDeclaratorSyntax variableDeclarationVariable in f.Declaration.Variables) {
					newVariableInitializers =
						newVariableInitializers.Add(variableDeclarationVariable.WithInitializer(
							FixVariableInitializer(variableDeclarationVariable.Initializer, arraySize)));
				}

				break;
		}

		return newVariableInitializers;
	}

	private static EqualsValueClauseSyntax?
		FixVariableInitializer(EqualsValueClauseSyntax? initializer, int arraySize) {
		if (initializer == null) {
			return initializer;
		}

		if (initializer.Value.ToString().Contains("<")) {
			return initializer.WithValue(SyntaxFactory.ParseExpression(new StringBuilder("new ")
					.Append(initializer.Value.ToString().Split("<")[1].Split(">")[0]).Append("[").Append(arraySize)
					.Append("];").ToString()).WithLeadingTrivia(initializer.GetLeadingTrivia())
				.WithTrailingTrivia(initializer.GetTrailingTrivia()));
		}

		return initializer.WithValue(
				SyntaxFactory.ParseExpression(
					new StringBuilder("new object[").Append(arraySize).Append("];").ToString()))
			.WithLeadingTrivia(initializer.GetLeadingTrivia()).WithTrailingTrivia(initializer.GetTrailingTrivia());
	}

	private static SyntaxNode FixRoot(TypeSyntax type, SeparatedSyntaxList<VariableDeclaratorSyntax> initializers,
		SyntaxNode root, SyntaxNode declaration) {
		switch (declaration) {
			case LocalDeclarationStatementSyntax l:
				return root.ReplaceNode(declaration,
					l.WithDeclaration(l.Declaration.WithType(type).WithVariables(initializers)));
			case FieldDeclarationSyntax f:
				return root.ReplaceNode(declaration,
					f.WithDeclaration(f.Declaration.WithType(type).WithVariables(initializers)));
			default:
				return root;
		}
	}

	private static SyntaxNode FixRootProperty(TypeSyntax type, SyntaxNode root, SyntaxNode declaration, int arraySize) {
		// If we are here, it is always a property and therefore this cast is ok
		PropertyDeclarationSyntax p = (PropertyDeclarationSyntax)declaration;
		return root.ReplaceNode(declaration,
			p.WithType(type).WithInitializer(FixVariableInitializer(p.Initializer, arraySize)));
	}
}