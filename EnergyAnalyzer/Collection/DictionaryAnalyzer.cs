﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Collection;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class DictionaryAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "MakeDictionary";

	private static readonly DiagnosticDescriptor MakeDictionary = new(DiagnosticId,
		"Use Dictionary",
		"Using a Dictionary is usually more efficient than other types of tables.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public static readonly string[] RelevantTypes = {
		"Dictionary", "Hashtable", "ImmutableDictionary", "ImmutableSortedDictionary",
		"ReadOnlyDictionary", "SortedDictionary", "SortedList"
	};

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeDictionary);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.LocalDeclarationStatement);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.FieldDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
	}

	private void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (CanBeDictionary(context.Node)) {
			context.ReportDiagnostic(Diagnostic.Create(MakeDictionary, context.Node.GetLocation()));
		}
	}

	private static bool CanBeDictionary(SyntaxNode declaration) {
		string type = declaration switch {
			LocalDeclarationStatementSyntax l => l.Declaration.Type.GetText().ToString().Trim(),
			FieldDeclarationSyntax f => f.Declaration.Type.GetText().ToString().Trim(),
			PropertyDeclarationSyntax prop => prop.Type.GetText().ToString().Trim(),
			_ => ""
		};


		return RelevantTypes.Contains(type.Split("<")[0]) && !type.StartsWith("Dictionary");
	}
}