﻿using System.Collections.Immutable;
using System.Composition;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace EnergyAnalyzer.Collection;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(DictionaryCodeFix)), Shared]
public class DictionaryCodeFix : CodeFixProvider {
	public override ImmutableArray<string> FixableDiagnosticIds =>
		ImmutableArray.Create(DictionaryAnalyzer.DiagnosticId);

	public override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

	public override async Task RegisterCodeFixesAsync(CodeFixContext context) {
		SyntaxNode root = (await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false))!;

		Diagnostic diagnostic = context.Diagnostics.First();
		Microsoft.CodeAnalysis.Text.TextSpan diagnosticSpan = diagnostic.Location.SourceSpan;

		LocalDeclarationStatementSyntax? declaration = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<LocalDeclarationStatementSyntax>().FirstOrDefault();
		PropertyDeclarationSyntax? property = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<PropertyDeclarationSyntax>().FirstOrDefault();
		FieldDeclarationSyntax? fieldDeclaration = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<FieldDeclarationSyntax>().FirstOrDefault();

		CodeAction action;

		if (declaration != null) {
			action = CodeAction.Create(
				"Make Dictionary",
				_ => MakeDictionaryAsync(root, context.Document, declaration),
				equivalenceKey: nameof(DictionaryCodeFix));
			context.RegisterCodeFix(action, diagnostic);
		}

		if (property != null) {
			action = CodeAction.Create(
				"Make Dictionary",
				_ => MakeDictionaryAsync(root, context.Document, property),
				equivalenceKey: nameof(DictionaryCodeFix));
			context.RegisterCodeFix(action, diagnostic);
		}

		if (fieldDeclaration != null) {
			action = CodeAction.Create(
				"Make Dictionary",
				_ => MakeDictionaryAsync(root, context.Document, fieldDeclaration),
				equivalenceKey: nameof(DictionaryCodeFix));
			context.RegisterCodeFix(action, diagnostic);
		}
	}

	private static Task<Document> MakeDictionaryAsync(SyntaxNode root, Document document, SyntaxNode declaration) {
		// The variable declaration
		VariableDeclarationSyntax variableDeclaration;

		// The new type of the variable declaration
		TypeSyntax typeName;

		// The new variable initializers
		var newVariableInitializers = new SeparatedSyntaxList<VariableDeclaratorSyntax>();

		// The new root
		SyntaxNode newRoot;

		switch (declaration) {
			case LocalDeclarationStatementSyntax l:
				// Get the declaration of the variable
				variableDeclaration = l.Declaration;

				// Get the new type of the declaration
				typeName = FixVariableType(l.Declaration.Type);

				// Change the variable declaration to use the new initializer
				foreach (VariableDeclaratorSyntax variableDeclarationVariable in variableDeclaration.Variables) {
					newVariableInitializers = newVariableInitializers.Add(
						variableDeclarationVariable.WithInitializer(
							FixVariableInitializer(variableDeclarationVariable.Initializer)));
				}

				// Change the variable declaration to use the new type and new initializers
				variableDeclaration = variableDeclaration.WithType(typeName).WithVariables(newVariableInitializers);

				// Create the new declaration
				LocalDeclarationStatementSyntax
					newDeclaration = l.WithDeclaration(variableDeclaration);

				// Create a new root with the new declaration and return it
				newRoot = root.ReplaceNode(declaration, newDeclaration);
				return Task.FromResult(document.WithSyntaxRoot(newRoot));
			case FieldDeclarationSyntax f:
				// Get the declaration of the variable
				variableDeclaration = f.Declaration;

				// Get the new type of the declaration
				typeName = FixVariableType(f.Declaration.Type);

				// Change the variable declaration to use the new initializer
				foreach (VariableDeclaratorSyntax variableDeclarationVariable in variableDeclaration.Variables) {
					newVariableInitializers = newVariableInitializers.Add(
						variableDeclarationVariable.WithInitializer(
							FixVariableInitializer(variableDeclarationVariable.Initializer)));
				}

				// Change the variable declaration to use the new type and new initializers
				variableDeclaration = variableDeclaration.WithType(typeName).WithVariables(newVariableInitializers);

				// Create the new declaration
				FieldDeclarationSyntax newFieldDeclaration = f.WithDeclaration(variableDeclaration);

				// Create a new root with the new declaration and return it
				newRoot = root.ReplaceNode(declaration, newFieldDeclaration);
				return Task.FromResult(document.WithSyntaxRoot(newRoot));
			case PropertyDeclarationSyntax p:
				// Get the new type of the declaration
				typeName = FixVariableType(p.Type);

				// Change the variable declaration and initializer to use the new type
				p = p.WithType(typeName).WithInitializer(FixVariableInitializer(p.Initializer));

				// Create a new root with the new declaration and return it
				newRoot = root.ReplaceNode(declaration, p);
				return Task.FromResult(document.WithSyntaxRoot(newRoot));
		}

		return Task.FromResult(document.WithSyntaxRoot(root));
	}

	private static TypeSyntax FixVariableType(TypeSyntax type) {
		if (!type.ToString().Contains("<")) {
			return SyntaxFactory.ParseTypeName("Dictionary<object, object>")
				.WithLeadingTrivia(type.GetLeadingTrivia())
				.WithTrailingTrivia(type.GetTrailingTrivia());
		}

		StringBuilder relevantRegex = new StringBuilder();
		foreach (string relevantType in DictionaryAnalyzer.RelevantTypes) {
			if (relevantType.Equals("Dictionary")) {
				continue;
			}

			relevantRegex.Append("(");
			relevantRegex.Append(relevantType);
			relevantRegex.Append(")");
			relevantRegex.Append("|");
		}

		relevantRegex.Remove(relevantRegex.Length - 1, 1);

		return SyntaxFactory.ParseTypeName(Regex.Replace(type.ToString(), relevantRegex.ToString(), "Dictionary"))
			.WithLeadingTrivia(type.GetLeadingTrivia()).WithTrailingTrivia(type.GetTrailingTrivia());
	}

	private static EqualsValueClauseSyntax? FixVariableInitializer(EqualsValueClauseSyntax? initializer) {
		if (initializer == null) {
			return initializer;
		}

		if (!initializer.ToString().Contains("<")) {
			return initializer.WithValue(SyntaxFactory.ParseExpression("new Dictionary<object, object>()"))
				.WithLeadingTrivia(initializer.GetLeadingTrivia())
				.WithTrailingTrivia(initializer.GetTrailingTrivia());
		}

		StringBuilder relevantRegex = new StringBuilder();
		foreach (string relevantType in DictionaryAnalyzer.RelevantTypes) {
			if (relevantType.Equals("Dictionary")) {
				continue;
			}

			relevantRegex.Append("(");
			relevantRegex.Append(relevantType);
			relevantRegex.Append(")");
			relevantRegex.Append("|");
		}

		relevantRegex.Remove(relevantRegex.Length - 1, 1);

		return initializer.WithValue(
			SyntaxFactory
				.ParseExpression(Regex.Replace(initializer.Value.ToString(), relevantRegex.ToString(), "Dictionary"))
				.WithLeadingTrivia(initializer.GetLeadingTrivia()).WithTrailingTrivia(initializer.GetTrailingTrivia()));
	}
}