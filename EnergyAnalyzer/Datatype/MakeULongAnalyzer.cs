﻿using System.Collections.Concurrent;
using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Datatype;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class MakeULongAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "MakeUlong";

	private static readonly ConcurrentDictionary<BlockSyntax, List<SyntaxToken>> VariablesToDiagnose = new();

	public static readonly DiagnosticDescriptor MakeULongRule =
		new(DiagnosticId,
			"Use ulong",
			"Using ulong is usually more efficient than other datatypes when above the 32-bit limit.",
			"Usage",
			DiagnosticSeverity.Warning,
			true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeULongRule);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeBlock, SyntaxKind.Block);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.FieldDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.LocalDeclarationStatement);
		context.RegisterSyntaxNodeAction(AnalyzeExpression, SyntaxKind.ExpressionStatement);
		context.RegisterCodeBlockAction(SendDiagnostics);
	}

	internal static bool CheckDependencies(SyntaxNode node, SemanticModel semanticModel) {
		List<EqualsValueClauseSyntax?> initializers;

		if (node is LocalDeclarationStatementSyntax l) {
			initializers = l.Declaration.Variables.Select(
				variable => variable.Initializer).ToList();
			if (!initializers.Any(MakeUIntAnalyzer.IsGreaterThanUInt) &&
			    l.Declaration.Variables.Any(v => MakeUIntAnalyzer.IsWrittenOutside(v, semanticModel))) {
				return true;
			}
		}

		// Check if uint should be used instead - In which case we do not give suggestion from here
		return MakeUIntAnalyzer.CanBeMadeUInt(node, semanticModel);
	}

	private static void AnalyzeBlock(SyntaxNodeAnalysisContext context) {
		if (!VariablesToDiagnose.ContainsKey((context.Node as BlockSyntax)!)) {
			VariablesToDiagnose.TryAdd((context.Node as BlockSyntax)!, new());
		}
	}

	private static void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (CheckDependencies(context.Node, context.SemanticModel)) {
			return;
		}

		if (!CanBeMadeULong(context.Node, context.SemanticModel)) {
			return;
		}

		switch (context.Node) {
			case LocalDeclarationStatementSyntax localDeclarationStatementSyntax:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeULongRule, localDeclarationStatementSyntax.Declaration.Type.GetLocation()));
				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeULongRule, propertyDeclaration.Type.GetLocation()));
				break;
			case FieldDeclarationSyntax fieldDeclarationSyntax:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeULongRule, fieldDeclarationSyntax.Declaration.Type.GetLocation()));
				break;
		}
	}

	private static void AnalyzeExpression(SyntaxNodeAnalysisContext context) {
		if (((ExpressionStatementSyntax)context.Node).Expression is not AssignmentExpressionSyntax assignment) {
			return;
		}

		if (assignment.Left is not IdentifierNameSyntax l) {
			return;
		}

		if (assignment is not { Right: PrefixUnaryExpressionSyntax }) {
			return;
		}

		ISymbol? temp = context.SemanticModel.LookupSymbols(l.GetLocation().SourceSpan.Start)
			.FirstOrDefault(x => x.Name == l.Identifier.Text);
		Location? loc = temp?.Locations[0];
		BlockSyntax? block = assignment.AncestorsAndSelf().OfType<BlockSyntax>().FirstOrDefault();
		if (block == null) {
			return;
		}

		if (VariablesToDiagnose.ContainsKey(block)) {
			VariablesToDiagnose[block].Remove(
				VariablesToDiagnose[block].FirstOrDefault(token => token.GetLocation() == loc));
		}
	}

	private static void SendDiagnostics(CodeBlockAnalysisContext context) {
		if (context.CodeBlock is not MethodDeclarationSyntax methodDeclaration) {
			return;
		}

		if (methodDeclaration.Modifiers.Any(x => x.IsKind(SyntaxKind.AbstractKeyword))) {
			return;
		}

		//The body is null if it is an abstract method or a interface method definition
		if (methodDeclaration.Body == null) {
			return;
		}

		List<SyntaxToken> toBeRemoved = new();
		foreach (SyntaxToken syntaxToken in VariablesToDiagnose[methodDeclaration.Body!]) {
			try {
				context.ReportDiagnostic(Diagnostic.Create(MakeULongRule,
					syntaxToken.Parent!.Parent!.Parent!.GetLocation()));
				toBeRemoved.Add(syntaxToken);
			}
			catch (System.Exception) {
				// ignored
			}
		}

		foreach (SyntaxToken syntaxToken in toBeRemoved) {
			if (!VariablesToDiagnose.ContainsKey((context.CodeBlock as MethodDeclarationSyntax)!.Body!)) {
				continue;
			}

			VariablesToDiagnose[(context.CodeBlock as MethodDeclarationSyntax)!.Body!].Remove(syntaxToken);
		}
	}


	private static bool CanBeMadeULong(SyntaxNode node, SemanticModel semanticModel) {
		string type;
		List<EqualsValueClauseSyntax?> initializers;

		switch (node) {
			case LocalDeclarationStatementSyntax localDeclaration:
				type = localDeclaration.Declaration.Type.GetText().ToString().Trim();
				initializers = localDeclaration.Declaration.Variables.Select(
					variable => variable.Initializer).ToList();
				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				type = propertyDeclaration.Type.GetText().ToString().Trim();
				initializers = new List<EqualsValueClauseSyntax?>() {
					propertyDeclaration.Initializer
				};
				break;
			case FieldDeclarationSyntax fieldDeclaration:
				type = fieldDeclaration.Declaration.Type.GetText().ToString().Trim();
				initializers = fieldDeclaration.Declaration.Variables.Select(
					variable => variable.Initializer).ToList();
				break;
			default:
				return false;
		}


		if (type is "ulong" or "nuint" or "uint") {
			return false;
		}

		// If the datatype is int sized or less then the uint analyzer will take care of it
		if (type is not ("long" or "nint")) {
			return false;
		}

		if (initializers.Any(initializer => initializer != null) &&
		    initializers.Any(initializer => initializer is not { Value: LiteralExpressionSyntax exp })) {
			return false;
		}

		if (initializers.Any(IsNegative)) {
			return false;
		}

		//TODO: Not sure this works for field and props
		if (node is not LocalDeclarationStatementSyntax localDecl) {
			return true;
		}


		// If the variable is used outside of the declaration, we wait with the diagnostic reporting
		foreach (VariableDeclaratorSyntax variable in localDecl.Declaration.Variables) {
			if (IsWrittenOutside(variable, semanticModel)) {
				return false;
			}
		}


		return true;
	}

	private static bool IsNegative(EqualsValueClauseSyntax? initializer) {
		return initializer is { Value: PrefixUnaryExpressionSyntax };
	}

	private static bool IsWrittenOutside(VariableDeclaratorSyntax declaratorSyntax, SemanticModel semanticModel) {
		// Perform data flow analysis on the local declaration found.
		DataFlowAnalysis dataFlowAnalysis = semanticModel.AnalyzeDataFlow(declaratorSyntax.Parent!.Parent!);

		BlockSyntax? block = declaratorSyntax.AncestorsAndSelf().OfType<BlockSyntax>().FirstOrDefault();

		if (block is null) {
			return false;
		}

		if (semanticModel.GetDeclaredSymbol(declaratorSyntax) is null) {
			return false;
		}

		// See if the variables declared within the lambda expression contains the variable we are looking for
		// In which case everything is fine
		if (!dataFlowAnalysis.WrittenOutside.Contains(semanticModel.GetDeclaredSymbol(declaratorSyntax)!)) {
			return false;
		}

		if (!VariablesToDiagnose.ContainsKey(block)) {
			VariablesToDiagnose.TryAdd(block, new List<SyntaxToken>());
		}

		VariablesToDiagnose[block].Add(declaratorSyntax.Identifier);
		return true;
	}
}