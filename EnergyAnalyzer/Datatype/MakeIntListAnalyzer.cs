﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Datatype;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class MakeIntListAnalyzer : DiagnosticAnalyzer {
	private const string DiagnosticId = "MakeIntList";

	private static readonly DiagnosticDescriptor MakeIntList = new(DiagnosticId,
		"Use List<int>",
		"Using int in a list is usually more efficient than uint.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeIntList);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.LocalDeclarationStatement);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.FieldDeclaration);
	}

	private void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (ShouldBeIntList(context.Node)) {
			context.ReportDiagnostic(Diagnostic.Create(MakeIntList, context.Node.GetLocation()));
		}
	}

	private static bool ShouldBeIntList(SyntaxNode node) {
		string type;
		switch (node) {
			case LocalDeclarationStatementSyntax localDeclaration:
				type = localDeclaration.Declaration.Type.GetText().ToString().Trim();
				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				type = propertyDeclaration.Type.GetText().ToString().Trim();
				break;
			case FieldDeclarationSyntax fieldDeclaration:
				type = fieldDeclaration.Declaration.Type.GetText().ToString().Trim();
				break;
			default:
				return false;
		}

		return type.Equals("List<uint>") || type.Equals("List<UInt32>");
	}
}