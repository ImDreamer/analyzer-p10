using System.Collections.Immutable;
using System.Composition;
using System.Text.RegularExpressions;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Formatting;
using Microsoft.CodeAnalysis.Simplification;

namespace EnergyAnalyzer.Datatype;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(MakeUIntCodeFixProvider)), Shared]
public sealed class MakeUIntCodeFixProvider : CodeFixProvider {
	public override ImmutableArray<string> FixableDiagnosticIds =>
		ImmutableArray.Create(MakeUIntAnalyzer.DiagnosticId);

	public override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

	public override async Task RegisterCodeFixesAsync(CodeFixContext context) {
		SyntaxNode root = (await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false))!;

		Diagnostic diagnostic = context.Diagnostics.First();
		Microsoft.CodeAnalysis.Text.TextSpan diagnosticSpan = diagnostic.Location.SourceSpan;

		// Find the local declaration identified by the diagnostic.
		LocalDeclarationStatementSyntax? declaration = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<LocalDeclarationStatementSyntax>().FirstOrDefault();
		PropertyDeclarationSyntax? property = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<PropertyDeclarationSyntax>().FirstOrDefault();
		FieldDeclarationSyntax? fieldDeclaration = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<FieldDeclarationSyntax>().FirstOrDefault();

		// Register a code action that will invoke the fix.
		if (declaration != null) {
			CodeAction action = CodeAction.Create(
				"Make uint",
				c => MakeUIntLocal(context.Document, declaration, c),
				nameof(MakeUIntCodeFixProvider));
			context.RegisterCodeFix(action, diagnostic);
		}

		if (property != null) {
			CodeAction prop = CodeAction.Create(
				"Make uint",
				c => MakeUIntProperty(context.Document, property, c),
				nameof(MakeUIntCodeFixProvider));
			context.RegisterCodeFix(prop, diagnostic);
		}

		if (fieldDeclaration != null) {
			CodeAction field = CodeAction.Create(
				"Make uint",
				c => MakeUIntField(context.Document, fieldDeclaration, c),
				nameof(MakeUIntCodeFixProvider));
			context.RegisterCodeFix(field, diagnostic);
		}
	}

	private static async Task<Document> MakeUIntLocal(Document document,
		LocalDeclarationStatementSyntax localDeclaration,
		CancellationToken cancellationToken) {
		VariableDeclarationSyntax variableDeclaration = localDeclaration.Declaration;
		TypeSyntax variableTypeName = variableDeclaration.Type;

		TypeSyntax typeName = SyntaxFactory.ParseTypeName("uint")
			.WithLeadingTrivia(variableTypeName.GetLeadingTrivia())
			.WithTrailingTrivia(variableTypeName.GetTrailingTrivia());


		var newVariableDeclarators = new SeparatedSyntaxList<VariableDeclaratorSyntax>();

		foreach (VariableDeclaratorSyntax variableDeclarator in variableDeclaration.Variables) {
			VariableDeclaratorSyntax fix =
				variableDeclarator.WithInitializer(FixVariableDeclarator(variableDeclarator.Initializer));
			
			newVariableDeclarators = newVariableDeclarators.Add(fix);
		}

		TypeSyntax simplifiedTypeName = typeName.WithAdditionalAnnotations(Simplifier.Annotation);
		variableDeclaration = variableDeclaration.WithType(simplifiedTypeName).WithVariables(newVariableDeclarators);

		// Produce the new local declaration.
		LocalDeclarationStatementSyntax newLocal = localDeclaration.WithDeclaration(variableDeclaration);

		SyntaxNode root = (await document.GetSyntaxRootAsync(cancellationToken).ConfigureAwait(false))!;
		// Replace the old local declaration with the new local declaration.
		SyntaxNode newRoot = root.ReplaceNode(localDeclaration, newLocal);

		// Return document with transformed tree.
		return document.WithSyntaxRoot(newRoot);
	}

	private static async Task<Document> MakeUIntField(Document document,
		FieldDeclarationSyntax fieldDeclaration,
		CancellationToken cancellationToken) {
		VariableDeclarationSyntax variableDeclaration = fieldDeclaration.Declaration;
		TypeSyntax variableTypeName = variableDeclaration.Type;

		TypeSyntax typeName = SyntaxFactory.ParseTypeName("uint")
			.WithLeadingTrivia(variableTypeName.GetLeadingTrivia())
			.WithTrailingTrivia(variableTypeName.GetTrailingTrivia());

		var newVariableDeclarators = new SeparatedSyntaxList<VariableDeclaratorSyntax>();

		foreach (VariableDeclaratorSyntax variableDeclarator in variableDeclaration.Variables) {
			VariableDeclaratorSyntax fix =
				variableDeclarator.WithInitializer(FixVariableDeclarator(variableDeclarator.Initializer));
			
			newVariableDeclarators = newVariableDeclarators.Add(fix);
		}

		TypeSyntax simplifiedTypeName = typeName.WithAdditionalAnnotations(Simplifier.Annotation);
		variableDeclaration = variableDeclaration.WithType(simplifiedTypeName).WithVariables(newVariableDeclarators);

		// Produce the new local declaration.
		FieldDeclarationSyntax newLocal = fieldDeclaration.WithDeclaration(variableDeclaration);

		SyntaxNode root = (await document.GetSyntaxRootAsync(cancellationToken).ConfigureAwait(false))!;
		// Replace the old local declaration with the new local declaration.
		SyntaxNode newRoot = root.ReplaceNode(fieldDeclaration, newLocal);

		// Return document with transformed tree.
		return document.WithSyntaxRoot(newRoot);
	}

	private static async Task<Document> MakeUIntProperty(Document document,
		PropertyDeclarationSyntax propertyDeclarationNode,
		CancellationToken cancellationToken) {
		PropertyDeclarationSyntax variableDeclaration = propertyDeclarationNode;
		TypeSyntax variableTypeName = variableDeclaration.Type;
		TypeSyntax typeName = SyntaxFactory.ParseTypeName("uint")
			.WithLeadingTrivia(variableTypeName.GetLeadingTrivia())
			.WithTrailingTrivia(variableTypeName.GetTrailingTrivia());

		TypeSyntax simplifiedTypeName = typeName.WithAdditionalAnnotations(Simplifier.Annotation);
		variableDeclaration = variableDeclaration.WithType(simplifiedTypeName)
			.WithInitializer(FixVariableDeclarator(variableDeclaration.Initializer));

		// Add an annotation to format the new local declaration.
		PropertyDeclarationSyntax formattedLocal = variableDeclaration.WithAdditionalAnnotations(Formatter.Annotation);

		SyntaxNode root = (await document.GetSyntaxRootAsync(cancellationToken).ConfigureAwait(false))!;
		// Replace the old local declaration with the new local declaration.
		SyntaxNode newRoot = root.ReplaceNode(propertyDeclarationNode, formattedLocal);

		// Return document with transformed tree.
		return document.WithSyntaxRoot(newRoot);
	}

	private static EqualsValueClauseSyntax? FixVariableDeclarator(EqualsValueClauseSyntax? initializer) {
		if (initializer == null) {
			return initializer;
		}

		if (initializer.Value is LiteralExpressionSyntax) {
			return initializer.WithValue(
				SyntaxFactory.ParseExpression(Regex.Replace(initializer.Value.ToString(), "([L|l|u|U|ul|UL|])", "")));
		}

		return initializer;
	}
}