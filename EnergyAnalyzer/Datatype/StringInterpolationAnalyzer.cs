﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Datatype;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class StringInterpolationAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "MakeStringInterpolation";

	private static readonly DiagnosticDescriptor MakeStringInterpolation = new(DiagnosticId,
		"MakeStringInterpolation",
		"Using String Interpolation is usually more efficient than other string concatenation methods when concatenating 2 strings.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeStringInterpolation);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeBlock, SyntaxKind.Block);
	}

	private void AnalyzeBlock(SyntaxNodeAnalysisContext context) {
		Dictionary<string, int> count = new();
		// Go through all of the statements in the block
		foreach (StatementSyntax statement in ((BlockSyntax)context.Node).Statements) {
			switch (statement) {
				// Count up for the actual declaration in case there is concatenation there and then again later
				case LocalDeclarationStatementSyntax localDeclaration: {
					string type = localDeclaration.Declaration.Type.GetText().ToString().Trim();
					if (type is not "string" and not "StringBuilder" and not "String") {
						continue;
					}

					foreach (VariableDeclaratorSyntax variableDeclaratorSyntax in
						localDeclaration.Declaration.Variables) {
						count.Add(variableDeclaratorSyntax.Identifier.Text, 0);

						if (variableDeclaratorSyntax.Initializer != null) {
							count[variableDeclaratorSyntax.Identifier.Text] +=
								CountRight(variableDeclaratorSyntax.Initializer.Value);
						}
						else {
							// We set the start value to 0, as the default value of a string is null
							// This ensures that if a string is uninitialized, we won't be the ones making an error
							count[variableDeclaratorSyntax.Identifier.Text] = 0;
						}
					}

					break;
				}
				// If the statement is an expression, the expression is an assignment expression syntax and the left
				// side of the assignment is an identifier, we count the identifier up so we can figure out if there
				// are more than 2 and we want to say it should be a StringBuilder
				case ExpressionStatementSyntax {
					Expression: AssignmentExpressionSyntax {
						Left: IdentifierNameSyntax identifier
					} assignmentExpression
				}: {
					ILocalSymbol? symbol = context.SemanticModel
						.LookupSymbols(identifier.Parent!.GetLocation().SourceSpan.Start).FirstOrDefault(a =>
							a.Name == identifier.Identifier.Text) as ILocalSymbol;
					if (symbol is null || symbol.Type.ToString() != "string" &&
						symbol.Type.ToString() != "System.Text.StringBuilder" &&
						symbol.Type.ToString() != "System.String") {
						return;
					}

					if (count.ContainsKey(identifier.Identifier.Text)) {
						count[identifier.Identifier.Text]++;
					}
					else {
						// We start at 2 because there are always at least 2 strings being concatenated together if we are here
						// i.e. if we get here it is because a += has occurred which means there is an assignment previously
						// and at least two elements are therefore concatenated together
						count.Add(identifier.Identifier.Text, 2);
					}

					// If we haven't counted to 3 yet and we assign in a way that is not +=, then we know it's a new
					// assignment and we need to count from 0 again.
					// If it has += and it is equal to one, we count add one because we know a previous
					// string has been created that we are concatenating to, so we have at least 2
					switch (count[identifier.Identifier.Text]) {
						case < 3 when !assignmentExpression.ToString().Contains("+="):
							count[identifier.Identifier.Text] = 0;
							break;
						case 1 when assignmentExpression.ToString().Contains("+="):
							count[identifier.Identifier.Text]++;
							break;
					}

					count[identifier.Identifier.Text] += CountRight(assignmentExpression.Right);
					break;
				}
			}

			if (statement is not ExpressionStatementSyntax {
					Expression: InvocationExpressionSyntax {
						Expression: MemberAccessExpressionSyntax m
					} invocationExpression
				}) {
				continue;
			}

			if (m.Name.ToString().Contains("Append")) {
				string identifier = m.Expression.ToString().Split(".")[0];
				ILocalSymbol? symbol = context.SemanticModel
					.LookupSymbols(m.Parent!.GetLocation().SourceSpan.Start).FirstOrDefault(a =>
						a.Name == identifier) as ILocalSymbol;
				if (symbol is null || symbol.Type.ToString() != "string" &&
					symbol.Type.ToString() != "System.Text.StringBuilder" &&
					symbol.Type.ToString() != "System.String") {
					return;
				}

				if (!count.ContainsKey(identifier)) {
					count.Add(identifier, 0);
				}

				count[identifier] += CountRight(invocationExpression);
			}
		}

		// Go through the statements in the block so we can find the variable declarations
		foreach (StatementSyntax statementSyntax in ((BlockSyntax)context.Node).Statements) {
			// If it is not a variable declaration
			if (statementSyntax is not LocalDeclarationStatementSyntax localDeclarationStatementSyntax) {
				continue;
			}

			// Go through all the variables in the declaration
			foreach (VariableDeclaratorSyntax variableDeclaratorSyntax in localDeclarationStatementSyntax.Declaration
				.Variables) {
				// If the identifiers name is in the dictionary, and there are more than 2 concatenation
				// we want it to be a StringBuilder, because otherwise another analyzer will want to make
				// it into string interpolation
				if (count.ContainsKey(variableDeclaratorSyntax.Identifier.Text) &&
				    count[variableDeclaratorSyntax.Identifier.Text] == 2) {
					context.ReportDiagnostic(Diagnostic.Create(MakeStringInterpolation,
						localDeclarationStatementSyntax.GetLocation()));
				}
			}
		}
	}

	private int CountRight(ExpressionSyntax expression) {
		switch (expression) {
			// Check which type of expression e is.
			// If it is a binary expression we use the CountRecursive method to check all parts of it
			// If it is an interpolated string, we count the number of things in it
			// If it is an invocation, we count the arguments in the invocation
			case BinaryExpressionSyntax binaryExpressionSyntax:
				return CountRecursive(binaryExpressionSyntax);
			case InvocationExpressionSyntax invocationExpression:
				if (invocationExpression.Expression is not MemberAccessExpressionSyntax memberAccessExpression) {
					return 0;
				}

				if (memberAccessExpression.ToString().Contains("string.Format")) {
					return invocationExpression.ArgumentList.Arguments.Count - 1;
				}

				if (memberAccessExpression.ToString().Contains("string.Join") ||
				    memberAccessExpression.ToString().Contains("string.Concat")) {
					return invocationExpression.ArgumentList.Arguments.Count;
				}

				if (!memberAccessExpression.Name.ToString().Contains("Append")) {
					return 0;
				}

				var countToReturn = 1;
				ExpressionSyntax innerMa = memberAccessExpression.Expression;
				while (innerMa is MemberAccessExpressionSyntax or InvocationExpressionSyntax) {
					switch (innerMa) {
						case MemberAccessExpressionSyntax ima:
							if (ima.Name.ToString().Contains("Append")) {
								countToReturn += 1;
								innerMa = ima.Expression;
							}

							break;
						case InvocationExpressionSyntax ies:
							innerMa = ies.Expression;
							break;
					}
				}

				if (innerMa is ObjectCreationExpressionSyntax innerObjectCreationExpression &&
				    innerObjectCreationExpression.ArgumentList!.Arguments.Count > 0) {
					countToReturn += 1;
				}

				return countToReturn;

			case ObjectCreationExpressionSyntax objectCreationExpression:
				if (objectCreationExpression.ToString().Contains("new StringBuilder") &&
				    objectCreationExpression.ArgumentList!.Arguments.Count > 0) {
					return 1;
				}

				return 0;
			default:
				return 0;
		}
	}

	private int CountRecursive(BinaryExpressionSyntax b) {
		var count = 1;

		switch (b.Left) {
			case InvocationExpressionSyntax invocationExpression:
				if (invocationExpression.ToString().Contains("string.Format")) {
					count += invocationExpression.ArgumentList.Arguments.Count - 1;
				}
				else if (invocationExpression.ToString().Contains("string.Join") ||
				         invocationExpression.ToString().Contains("string.Concat")) {
					count += invocationExpression.ArgumentList.Arguments.Count;
				}
				else if (invocationExpression.ToString().Contains("Append")) {
					count += 1;
				}

				break;
			case BinaryExpressionSyntax bl:
				count += CountRecursive(bl);
				break;
			default:
				count++;
				break;
		}

		if (b.Right is BinaryExpressionSyntax br) {
			return count + CountRecursive(br);
		}

		switch (b.Right) {
			case InterpolatedStringExpressionSyntax i:
				return count + i.Contents.Count;
			case InvocationExpressionSyntax ie:
				if (ie.ToString().Contains("string.Format")) {
					count += ie.ArgumentList.Arguments.Count - 1;
				}
				else if (ie.ToString().Contains("string.Join") ||
				         ie.ToString().Contains("string.Concat")) {
					count += ie.ArgumentList.Arguments.Count;
				}
				else if (ie.ToString().Contains("Append")) {
					count += 1;
				}

				return count;
			default:
				return count;
		}
	}
}