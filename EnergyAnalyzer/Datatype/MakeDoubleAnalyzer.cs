﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Datatype;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class MakeDoubleAnalyzer : DiagnosticAnalyzer {
	public static readonly string DiagnosticId = "MakeDouble";

	private static readonly DiagnosticDescriptor MakeDouble = new(DiagnosticId,
		"Use double",
		"Using double is usually more efficient than other decimal datatypes.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeDouble);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.LocalDeclarationStatement);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.FieldDeclaration);
	}

	private void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (!CanBeDouble(context.Node, context)) {
			return;
		}

		switch (context.Node) {
			case LocalDeclarationStatementSyntax localDeclarationStatementSyntax:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeDouble, localDeclarationStatementSyntax.Declaration.Type.GetLocation()));
				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeDouble, propertyDeclaration.Type.GetLocation()));
				break;
			case FieldDeclarationSyntax fieldDeclarationSyntax:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeDouble, fieldDeclarationSyntax.Declaration.Type.GetLocation()));
				break;
		}
	}

	private static bool CanBeDouble(SyntaxNode node, SyntaxNodeAnalysisContext context) {
		string? type;
		switch (node) {
			case LocalDeclarationStatementSyntax localDeclaration:
				type = localDeclaration.Declaration.Type.GetText().ToString().Trim();
				if (type == "var") {
					var symbolInfo = context.SemanticModel.GetSymbolInfo(localDeclaration.Declaration.Type);
					type = symbolInfo.Symbol?.ToString(); // the type symbol for the variable..
				}

				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				type = propertyDeclaration.Type.GetText().ToString().Trim();
				break;
			case FieldDeclarationSyntax fieldDeclaration:
				type = fieldDeclaration.Declaration.Type.GetText().ToString().Trim();
				break;
			default:
				return false;
		}

		if (type!.Equals("double")) {
			return false;
		}

		return type is "float" or "decimal";
	}
}