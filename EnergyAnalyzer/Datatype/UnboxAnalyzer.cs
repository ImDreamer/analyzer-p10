using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Datatype;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class UnboxAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "Unbox";

	private static readonly DiagnosticDescriptor UnBoxRule = new(DiagnosticId,
		"Unbox",
		"Using unboxed types is usually more efficient than boxed primitives.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(UnBoxRule);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.LocalDeclarationStatement);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.FieldDeclaration);
	}

	private static void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (!CanBeUnboxed(context.Node)) {
			return;
		}

		switch (context.Node) {
			case LocalDeclarationStatementSyntax localDeclarationStatementSyntax:
				context.ReportDiagnostic(
					Diagnostic.Create(UnBoxRule, localDeclarationStatementSyntax.Declaration.Type.GetLocation()));
				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				context.ReportDiagnostic(
					Diagnostic.Create(UnBoxRule, propertyDeclaration.Type.GetLocation()));
				break;
			case FieldDeclarationSyntax fieldDeclarationSyntax:
				context.ReportDiagnostic(
					Diagnostic.Create(UnBoxRule, fieldDeclarationSyntax.Declaration.Type.GetLocation()));
				break;
		}
	}

	internal static bool CanBeUnboxed(SyntaxNode node) {
		string type;
		switch (node) {
			case LocalDeclarationStatementSyntax localDeclaration:
				type = localDeclaration.Declaration.Type.GetText().ToString().Trim();
				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				type = propertyDeclaration.Type.GetText().ToString().Trim();
				break;
			case FieldDeclarationSyntax fieldDeclaration:
				type = fieldDeclaration.Declaration.Type.GetText().ToString().Trim();
				break;
			default:
				return false;
		}

		if (type.Equals(type.ToLower())) {
			return false;
		}

		return type.Replace("System.", "") is nameof(Boolean) or nameof(Byte) or nameof(SByte) or nameof(Char)
			or nameof(Decimal)
			or nameof(Double) or nameof(Int16) or nameof(UInt16) or nameof(Int32) or nameof(UInt32) or nameof(Int64)
			or nameof(UInt64) or nameof(Single) or nameof(String) or nameof(System.Object);
	}
}