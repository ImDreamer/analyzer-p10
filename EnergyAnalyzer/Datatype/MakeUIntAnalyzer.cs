using System.Collections.Concurrent;
using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Datatype;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class MakeUIntAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "Makeuint";
	private static ConcurrentDictionary<BlockSyntax, List<SyntaxToken>> VariablesToDiagnose = new();

	private static readonly DiagnosticDescriptor MakeUIntRule = new(DiagnosticId,
		"Use UInt",
		"Using uint is usually more efficient than other datatypes.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics => ImmutableArray.Create(MakeUIntRule);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeBlock, SyntaxKind.Block);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.LocalDeclarationStatement);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.FieldDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeExpression, SyntaxKind.ExpressionStatement);
		context.RegisterCodeBlockAction(SendDiagnostics);
	}

	private static void AnalyzeBlock(SyntaxNodeAnalysisContext context) {
		if (context.Node is not BlockSyntax block) {
			return;
		}

		if (!VariablesToDiagnose.ContainsKey(block)) {
			VariablesToDiagnose.TryAdd(block, new());
		}
	}

	private static void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (!CanBeMadeUInt(context.Node, context.SemanticModel)) {
			return;
		}

		switch (context.Node) {
			case LocalDeclarationStatementSyntax localDeclarationStatementSyntax:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeUIntRule,
						localDeclarationStatementSyntax.Declaration.Type.GetLocation()));
				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeUIntRule, propertyDeclaration.Type.GetLocation()));
				break;
			case FieldDeclarationSyntax fieldDeclarationSyntax:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeUIntRule, fieldDeclarationSyntax.Declaration.Type.GetLocation()));
				break;
		}
	}

	private static void AnalyzeExpression(SyntaxNodeAnalysisContext context) {
		if (((ExpressionStatementSyntax)context.Node).Expression is not AssignmentExpressionSyntax assignment) {
			return;
		}

		if (assignment.Left is not IdentifierNameSyntax l) {
			return;
		}

		if (assignment is not { Right: PrefixUnaryExpressionSyntax }) {
			return;
		}

		ISymbol? temp = context.SemanticModel.LookupSymbols(l.GetLocation().SourceSpan.Start)
			.FirstOrDefault(x => x.Name == l.Identifier.Text);
		Location? loc = temp?.Locations[0];
		BlockSyntax? block = assignment.AncestorsAndSelf().OfType<BlockSyntax>().FirstOrDefault();

		if (block == null) {
			return;
		}

		if (VariablesToDiagnose.ContainsKey(block)) {
			VariablesToDiagnose[block].Remove(
				VariablesToDiagnose[block].FirstOrDefault(token => token.GetLocation() == loc));
		}
	}

	private static void SendDiagnostics(CodeBlockAnalysisContext context) {
		if (context.CodeBlock is not MethodDeclarationSyntax methodDeclaration) {
			return;
		}

		if (methodDeclaration.Modifiers.Any(x => x.IsKind(SyntaxKind.AbstractKeyword))) {
			return;
		}

		//The body is null if it is an abstract method or a interface method definition
		if (methodDeclaration.Body == null) {
			return;
		}

		List<SyntaxToken> toBeRemoved = new();

		foreach (SyntaxToken syntaxToken in VariablesToDiagnose[methodDeclaration.Body]) {
			if (toBeRemoved.Contains(syntaxToken)) {
				continue;
			}

			try {
				context.ReportDiagnostic(Diagnostic.Create(MakeUIntRule,
					syntaxToken.Parent!.Parent!.Parent!.GetLocation()));
				toBeRemoved.Add(syntaxToken);
			}
			catch (System.Exception) {
				// ignored
			}
		}

		foreach (SyntaxToken syntaxToken in toBeRemoved) {
			if (!VariablesToDiagnose.ContainsKey((context.CodeBlock as MethodDeclarationSyntax)!.Body!)) {
				continue;
			}

			VariablesToDiagnose[(context.CodeBlock as MethodDeclarationSyntax)!.Body!].Remove(syntaxToken);
		}
	}

	internal static bool CanBeMadeUInt(SyntaxNode node, SemanticModel semanticModel) {
		string? type;
		List<EqualsValueClauseSyntax?> initializers;

		switch (node) {
			case LocalDeclarationStatementSyntax localDeclaration:
				type = localDeclaration.Declaration.Type.GetText().ToString().Trim();
				if (type == "var") {
					SymbolInfo symbolInfo = semanticModel.GetSymbolInfo(localDeclaration.Declaration.Type);
					type = symbolInfo.Symbol?.ToString(); // the type symbol for the variable..
				}

				if (type is not ("int" or "long" or "ulong" or "byte" or "sbyte" or "short" or "ushort" or "nint"
					or "nuint")) {
					return false;
				}

				initializers = localDeclaration.Declaration.Variables.Select(
					variable => variable.Initializer).ToList();

				// If the variable is used outside of the declaration, we wait with the diagnostic reporting
				if (localDeclaration.Declaration.Variables.Any(
					variable => IsWrittenOutside(variable, semanticModel))) {
					return false;
				}

				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				type = propertyDeclaration.Type.GetText().ToString().Trim();
				initializers = new List<EqualsValueClauseSyntax?>() {
					propertyDeclaration.Initializer
				};

				if (type is not ("int" or "long" or "ulong" or "byte" or "sbyte" or "short" or "ushort" or "nint"
					or "nuint")) {
					return false;
				}

				if (propertyDeclaration.Modifiers.Any(x => x.IsKind(SyntaxKind.OverrideKeyword))) {
					return false;
				}


				//TODO: Not sure if this is supported with field and prop
				//if (IsWrittenOutside(propertyDeclaration.Initializer, semanticModel)) {
				//return false;
				//}


				break;
			case FieldDeclarationSyntax fieldDeclaration:
				type = fieldDeclaration.Declaration.Type.GetText().ToString().Trim();

				if (type is not ("int" or "long" or "ulong" or "byte" or "sbyte" or "short" or "ushort" or "nint"
					or "nuint")) {
					return false;
				}

				initializers = fieldDeclaration.Declaration.Variables.Select(
					variable => variable.Initializer).ToList();

				// If the variable is used outside of the declaration, we wait with the diagnostic reporting
				//TODO: Not sure if this is supported with field and prop
				//if (fieldDeclaration.Declaration.Variables.Any(variable => IsWrittenOutside(variable, semanticModel))) {
				//	return false;
				//}

				break;
			default:
				return false;
		}

		if (initializers.Any(
			initializer => initializer != null && initializer is not { Value: LiteralExpressionSyntax })) {
			return false;
		}

		if (initializers.Any(IsNegative)) {
			return false;
		}

		if (initializers.Any(IsGreaterThanUInt)) {
			return false;
		}

		return true;
	}


	public static bool IsGreaterThanUInt(EqualsValueClauseSyntax? initializer) {
		if (initializer is not { Value: LiteralExpressionSyntax exp }) {
			return false;
		}

		return exp.Token.Value switch {
			long longValue => longValue > uint.MaxValue,
#pragma warning disable CS0652
			nint nativeValue => nativeValue > uint.MaxValue,
#pragma warning restore CS0652
			nuint uNativeValue => uNativeValue > uint.MaxValue,
			_ => false
		};
	}

	private static bool IsNegative(EqualsValueClauseSyntax? initializer) {
		return initializer is { Value: PrefixUnaryExpressionSyntax };
	}


	public static bool IsWrittenOutside(VariableDeclaratorSyntax declaratorSyntax, SemanticModel semanticModel) {
		// Perform data flow analysis on the local declaration found.
		DataFlowAnalysis dataFlowAnalysis = semanticModel.AnalyzeDataFlow(declaratorSyntax.Parent!.Parent!);

		BlockSyntax? block = declaratorSyntax.AncestorsAndSelf().OfType<BlockSyntax>().FirstOrDefault();

		if (block is null) {
			return false;
		}

		if (semanticModel.GetDeclaredSymbol(declaratorSyntax) is null) {
			return false;
		}

		ILocalSymbol? symbol = semanticModel.GetDeclaredSymbol(declaratorSyntax) as ILocalSymbol;
		if (symbol is null) {
			return false;
		}

		string? type = symbol.Type.ToString();
		if (type == "var" && declaratorSyntax.Parent.Parent is LocalDeclarationStatementSyntax l) {
			SymbolInfo symbolInfo = semanticModel.GetSymbolInfo(l.Declaration.Type);
			type = symbolInfo.Symbol?.ToString(); // the type symbol for the variable..
		}

		if (type is not ("int" or "long" or "ulong" or "byte" or "sbyte" or "short" or "ushort" or "nint"
			or "nuint")) {
			return false;
		}

		// See if the variables declared within the lambda expression contains the variable we are looking for
		// In which case everything is fine
		if (dataFlowAnalysis.WrittenOutside.Contains(semanticModel.GetDeclaredSymbol(declaratorSyntax)!)) {
			if (!VariablesToDiagnose.ContainsKey(block)) {
				VariablesToDiagnose.TryAdd(block, new List<SyntaxToken>());
			}

			VariablesToDiagnose[block].Add(declaratorSyntax.Identifier);
			return true;
		}

		return false;
	}
}