﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Datatype;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class MakeIntLongNintAnalyzer : DiagnosticAnalyzer {
	private const string DiagnosticId = "MakeIntLongNint";

	private static readonly DiagnosticDescriptor MakeIntLongNintRule = new(DiagnosticId,
		"Use int, long, or nint",
		"Using int, long, or nint is usually more efficient than other signed datatypes.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeIntLongNintRule);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.LocalDeclarationStatement);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.PropertyDeclaration);
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.FieldDeclaration);
	}

	internal static bool CheckDependencies(SyntaxNode node, SemanticModel semanticModel) {
		// Check if ulong (or anything that takes priority over it) should be used instead - In which case we do not give suggestion from here
		return MakeULongAnalyzer.CheckDependencies(node, semanticModel);
	}

	private void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (CheckDependencies(context.Node, context.SemanticModel)) {
			return;
		}

		if (!CanBeIntLongNint(context.Node, context)) {
			return;
		}

		switch (context.Node) {
			case LocalDeclarationStatementSyntax localDeclarationStatementSyntax:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeIntLongNintRule, localDeclarationStatementSyntax.Declaration.Type.GetLocation()));
				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeIntLongNintRule, propertyDeclaration.Type.GetLocation()));
				break;
			case FieldDeclarationSyntax fieldDeclarationSyntax:
				context.ReportDiagnostic(
					Diagnostic.Create(MakeIntLongNintRule, fieldDeclarationSyntax.Declaration.Type.GetLocation()));
				break;
		}
	}

	private static bool CanBeIntLongNint(SyntaxNode node, SyntaxNodeAnalysisContext context) {
		string? type;
		switch (node) {
			case LocalDeclarationStatementSyntax localDeclaration:
				type = localDeclaration.Declaration.Type.GetText().ToString().Trim();
				if (type == "var") {
					var symbolInfo = context.SemanticModel.GetSymbolInfo(localDeclaration.Declaration.Type);
					type = symbolInfo.Symbol?.ToString(); // the type symbol for the variable..
				}

				break;
			case PropertyDeclarationSyntax propertyDeclaration:
				type = propertyDeclaration.Type.GetText().ToString().Trim();
				break;
			case FieldDeclarationSyntax fieldDeclaration:
				type = fieldDeclaration.Declaration.Type.GetText().ToString().Trim();
				break;
			default:
				return false;
		}

		if (type is "int" or "long" or "nint" or "uint" or "ulong" or "nuint") {
			return false;
		}

		return type is "byte" or "sbyte" or "short" or "ushort";
	}
}