﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Datatype;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class StringBuilderAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "MakeStringBuilder";

	private static readonly DiagnosticDescriptor MakeStringBuilder = new(DiagnosticId,
		"MakeStringBuilder",
		"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeStringBuilder);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeBlock, SyntaxKind.Block);
	}

	private void AnalyzeBlock(SyntaxNodeAnalysisContext context) {
		Dictionary<string, int> count = new();
		// Go through all of the statements in the block
		foreach (StatementSyntax statement in ((BlockSyntax)context.Node).Statements) {
			// Count up for the actual declaration in case there is concatenation there and then again later
			if (statement is LocalDeclarationStatementSyntax l) {
				string type = l.Declaration.Type.GetText().ToString().Trim();
				if (type is not "string" and not "String") {
					continue;
				}

				foreach (VariableDeclaratorSyntax variableDeclaratorSyntax in l.Declaration.Variables) {
					count.Add(variableDeclaratorSyntax.Identifier.Text, 0);

					if (variableDeclaratorSyntax.Initializer != null) {
						count[variableDeclaratorSyntax.Identifier.Text] +=
							CountRight(variableDeclaratorSyntax.Initializer.Value);
					}
					else {
						// We set the start value to 0, as the default value of a string is null
						// This ensures that if a string is uninitialized, we won't be the ones making an error
						count[variableDeclaratorSyntax.Identifier.Text] = 0;
					}
				}
			}

			// If the statement is an expression, the expression is an assignment expression syntax and the left
			// side of the assignment is an identifier, we count the identifier up so we can figure out if there
			// are more than 2 and we want to say it should be a StringBuilder
			if (statement is ExpressionStatementSyntax e && e.Expression is AssignmentExpressionSyntax a &&
			    a.Left is IdentifierNameSyntax i) {
				ILocalSymbol? symbol = context.SemanticModel
					.LookupSymbols(i.Parent!.GetLocation().SourceSpan.Start).FirstOrDefault(a =>
						a.Name == i.Identifier.Text) as ILocalSymbol;
				if (symbol is null || symbol!.Type.ToString() != "string" && symbol.Type.ToString() != "String") {
					return;
				}
				if (count.ContainsKey(i.Identifier.Text)) {
					count[i.Identifier.Text]++;
				}
				else {
					// We start at 2 because there are always at least 2 strings being concatenated together if we are here
					// i.e. if we get here it is because a += has occurred which means there is an assignment previously
					// and at least two elements are therefore concatenated together
					count.Add(i.Identifier.Text, 2);
				}

				// If we haven't counted to 3 yet and we assign in a way that is not +=, then we know it's a new
				// assignment and we need to count from 0 again.
				// If it has += and it is equal to one, we count add one because we know a previous
				// string has been created that we are concatenating to, so we have at least 2
				if (count[i.Identifier.Text] < 3 && !a.ToString().Contains("+=")) {
					count[i.Identifier.Text] = 0;
				}
				else if (count[i.Identifier.Text] == 1 && a.ToString().Contains("+=")) {
					count[i.Identifier.Text]++;
				}

				count[i.Identifier.Text] += CountRight(a.Right);
			}
		}

		// Go through the statements in the block so we can find the variable declarations
		foreach (StatementSyntax statementSyntax in ((BlockSyntax)context.Node).Statements) {
			// If it is a variable declaration
			if (statementSyntax is LocalDeclarationStatementSyntax l) {
				// Go through all the variables in the declaration
				foreach (VariableDeclaratorSyntax variableDeclaratorSyntax in l.Declaration.Variables) {
					// If the identifiers name is in the dictionary, and there are more than 2 concatenation
					// we want it to be a StringBuilder, because otherwise another analyzer will want to make
					// it into string interpolation
					if (count.ContainsKey(variableDeclaratorSyntax.Identifier.Text) &&
					    count[variableDeclaratorSyntax.Identifier.Text] > 2) {
						context.ReportDiagnostic(Diagnostic.Create(MakeStringBuilder, l.GetLocation()));
					}
				}
			}
		}
	}

	private int CountRight(ExpressionSyntax e) {
		switch (e) {
			// Check which type of expression e is.
			// If it is a binary expression we use the CountRecursive method to check all parts of it
			// If it is an interpolated string, we count the number of things in it
			// If it is an invocation, we count the arguments in the invocation
			case BinaryExpressionSyntax b:
				return CountRecursive(b);
			case InterpolatedStringExpressionSyntax i:
				return i.Contents.Count;
			case InvocationExpressionSyntax ie:
				if (ie.ToString().Contains("string.Format")) {
					return ie.ArgumentList.Arguments.Count - 1;
				}
				else if (ie.ToString().Contains("string.Join") ||
				         ie.ToString().Contains("string.Concat")) {
					return ie.ArgumentList.Arguments.Count;
				}
				else {
					return 0;
				}
			default:
				return 0;
		}
	}

	private int CountRecursive(BinaryExpressionSyntax b) {
		var count = 1;

		switch (b.Left) {
			case InterpolatedStringExpressionSyntax i:
				count += i.Contents.Count;
				break;
			case InvocationExpressionSyntax ie:
				if (ie.ToString().Contains("string.Format")) {
					count += ie.ArgumentList.Arguments.Count - 1;
				}
				else if (ie.ToString().Contains("string.Join") ||
				         ie.ToString().Contains("string.Concat")) {
					count += ie.ArgumentList.Arguments.Count;
				}

				break;
			case BinaryExpressionSyntax bl:
				count += CountRecursive(bl);
				break;
			default:
				count++;
				break;
		}

		if (b.Right is BinaryExpressionSyntax br) {
			return count + CountRecursive(br);
		}

		switch (b.Right) {
			case InterpolatedStringExpressionSyntax i:
				return count + i.Contents.Count;
			case InvocationExpressionSyntax ie:
				if (ie.ToString().Contains("string.Format")) {
					count += ie.ArgumentList.Arguments.Count - 1;
				}
				else if (ie.ToString().Contains("string.Join") ||
				         ie.ToString().Contains("string.Concat")) {
					count += ie.ArgumentList.Arguments.Count;
				}

				return count;
			default:
				return count;
		}
	}
}