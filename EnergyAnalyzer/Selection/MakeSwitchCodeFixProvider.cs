﻿using System.Collections.Immutable;
using System.Composition;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace EnergyAnalyzer.Selection; 

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(MakeSwitchCodeFixProvider)), Shared]
public sealed class MakeSwitchCodeFixProvider : CodeFixProvider {
	public override ImmutableArray<string> FixableDiagnosticIds =>
		ImmutableArray.Create(MakeSwitchAnalyser.DiagnosticId);
	
	public override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;
	
	public override async Task RegisterCodeFixesAsync(CodeFixContext context) {
		SyntaxNode root = (await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false))!;

		Diagnostic diagnostic = context.Diagnostics.First();
		Microsoft.CodeAnalysis.Text.TextSpan diagnosticSpan = diagnostic.Location.SourceSpan;

		// Find the if statement identified by the diagnostic.
		IfStatementSyntax? ifStatement = root.FindToken(diagnosticSpan.Start).Parent!.AncestorsAndSelf()
			.OfType<IfStatementSyntax>().FirstOrDefault();

		// Register a code action that will invoke the fix.
		if (ifStatement != null) {
			CodeAction action = CodeAction.Create(
				"Make switch",
				_ => MakeSwitch(root, context.Document, ifStatement, diagnostic),
				nameof(MakeSwitchCodeFixProvider));
			context.RegisterCodeFix(action, diagnostic);
		}
		
	}

	private static Task<Document> MakeSwitch(SyntaxNode root, Document document,
		IfStatementSyntax ifStatement,Diagnostic diagnostic) {
		 
		SyntaxList<SwitchSectionSyntax> switchSectionsList = new SyntaxList<SwitchSectionSyntax>();
		BinaryExpressionSyntax expression = (ifStatement.Condition as BinaryExpressionSyntax)!;

		
		// Get the identifier name token and what we compare to
		IdentifierNameSyntax? identifierName = null;

		// Finds the left side of the expression and saves it to a variable
		switch (expression.Left) {
			case IdentifierNameSyntax i:
				ILocalSymbol? symbol = 
					document.GetSemanticModelAsync().Result!.LookupSymbols(diagnostic.Location.SourceSpan.Start).
						First(a => a.Name == i.ToString()) as ILocalSymbol;
				if (symbol is null) {
					break;
				}
				if (symbol.IsConst) { }
				else {
					identifierName = i;
				}
				break;
		}

		// Finds the right side of the expression and saves it to a variable
		switch (expression.Right) {
			case IdentifierNameSyntax i:
				ILocalSymbol? symbol = 
					document.GetSemanticModelAsync().Result!.LookupSymbols(diagnostic.Location.SourceSpan.Start).
						First(a => a.Name == i.ToString()) as ILocalSymbol;
				if (symbol is null) {
					break;
				}
				if (symbol.IsConst) { }
				else {
					identifierName = i;
				}
				break;
		}

		// Produce the rest of the section and add them to the list with sections

		switchSectionsList = GetSwitchSections(ifStatement, switchSectionsList, document, diagnostic);
		
		
		//Produce the equivalent switch statement
		SwitchStatementSyntax newSwitch = 
			SyntaxFactory.SwitchStatement(SyntaxFactory.ParseExpression(identifierName!.ToString()), switchSectionsList);

		// return document with the transformed tree
		return Task.FromResult(document.WithSyntaxRoot(root.ReplaceNode(ifStatement, newSwitch).
			NormalizeWhitespace("	", "\n")));
	}

	/// <summary>
	/// Takes all if statements and converts them to switch sections
	/// </summary>
	/// <param name="ifStatement">The if statements which if statements and else state is converted to switch sections</param>
	/// <param name="switchSectionsList">The list with the switch sections</param>
	/// <param name="document">The document that contains the code</param>
	/// <param name="diagnostic">The area which the diagnostic occurs</param>
	/// <returns></returns>
	private static SyntaxList<SwitchSectionSyntax> GetSwitchSections(IfStatementSyntax ifStatement,
		SyntaxList<SwitchSectionSyntax> switchSectionsList, Document document, Diagnostic diagnostic) {

		BinaryExpressionSyntax expression = (ifStatement.Condition as BinaryExpressionSyntax)!;
		IdentifierNameSyntax? constIdentifier = null;
		LiteralExpressionSyntax? literalExpression = null;
		SyntaxToken operatorToken = expression.OperatorToken;
		
		SyntaxList<SwitchLabelSyntax> switchLabels = new SyntaxList<SwitchLabelSyntax>();
		SyntaxList<StatementSyntax> switchStatements = new SyntaxList<StatementSyntax>();
		
		// Finds the left side of the expression and saves it to a variable
		switch (expression.Left) {
			case IdentifierNameSyntax i:
				ILocalSymbol? symbol = 
					document.GetSemanticModelAsync().Result!.LookupSymbols(diagnostic.Location.SourceSpan.Start).
						First(a => a.Name == i.ToString()) as ILocalSymbol;
				if (symbol is null) {
					break;
				}
				if (symbol.IsConst) {
					constIdentifier = i;
				}
				break;
			case LiteralExpressionSyntax l:
				literalExpression = l;
				break;
		}

		// Finds the right side of the expression and saves it to a variable
		switch (expression.Right) {
			case IdentifierNameSyntax i:
				ILocalSymbol? symbol = 
					document.GetSemanticModelAsync().Result!.LookupSymbols(diagnostic.Location.SourceSpan.Start).
						First(a => a.Name == i.ToString()) as ILocalSymbol;
				if (symbol is null) {
					break;
				}
				if (symbol.IsConst) {
					constIdentifier = i;
				}
				break;
			case LiteralExpressionSyntax l:
				literalExpression = l;
				break;
		}

		// Creates the label which is needed to create the section
		SwitchLabelSyntax? switchLabel = null;
		CasePatternSwitchLabelSyntax? relationalSwitchLabel = null;
		if (operatorToken.ToString().Equals("==")) {
			if (literalExpression is not null) {
				switchLabel = SyntaxFactory.CaseSwitchLabel(SyntaxFactory.ParseExpression($" {literalExpression}"));
			}
			else {
				switchLabel = SyntaxFactory.CaseSwitchLabel(SyntaxFactory.ParseExpression($" {constIdentifier}"));
			}
		}
		else {
			if (literalExpression is not null) {
				relationalSwitchLabel = 
					SyntaxFactory.CasePatternSwitchLabel(SyntaxFactory.RelationalPattern(operatorToken, SyntaxFactory.
						ParseExpression($" {literalExpression}")), SyntaxFactory.ParseToken(":"));
			}
			else {
				relationalSwitchLabel = 
					SyntaxFactory.CasePatternSwitchLabel(SyntaxFactory.RelationalPattern(operatorToken, SyntaxFactory.
						ParseExpression($" {constIdentifier}")), SyntaxFactory.ParseToken(":"));
			}
		}

		// Creates the statements which is needed to create the section
		if (ifStatement.Statement.ChildNodes().Count() > 1 || ifStatement.Statement.ToString().Contains("{")) {
			foreach (var statement in ifStatement.Statement.ChildNodes()) {
				switchStatements = switchStatements.Add((StatementSyntax)statement);
			}
		}
		else {
			switchStatements = switchStatements.Add(ifStatement.Statement);
		}


		if (switchLabel is not null) {
			switchLabels = switchLabels.Add(switchLabel);
		}
		else if (relationalSwitchLabel is not null) {
			switchLabels = switchLabels.Add(relationalSwitchLabel);
		}
		
		switchStatements = switchStatements.Add(SyntaxFactory.BreakStatement());

		SwitchSectionSyntax sectionSyntax = SyntaxFactory.SwitchSection(switchLabels, switchStatements);
		
		switchSectionsList = switchSectionsList.Add(sectionSyntax);
		
		if (ifStatement.Else?.ChildNodes().First() is IfStatementSyntax) {
			switchSectionsList = GetSwitchSections((IfStatementSyntax)ifStatement.Else.ChildNodes().
				First(), switchSectionsList, document, diagnostic);
		}
		
		// Produce the default section
		if (ifStatement.Else?.ChildNodes().First() is StatementSyntax && ifStatement.Else?.ChildNodes().
				First() is not IfStatementSyntax) {
			DefaultSwitchLabelSyntax defaultSwitchLabel = SyntaxFactory.DefaultSwitchLabel();
			SyntaxList<SwitchLabelSyntax> defaultSwitchLabels =
				new SyntaxList<SwitchLabelSyntax>().Add(defaultSwitchLabel);
			SyntaxList<StatementSyntax> defaultSwitchStatements =
				new SyntaxList<StatementSyntax>();

			if (ifStatement.Else!.ChildNodes().First().ChildNodes().Count() > 1 || ifStatement.Else!.ToString().Contains("{")) {
				foreach (var statement in ifStatement.Else!.ChildNodes().First().ChildNodes()) {
					defaultSwitchStatements = defaultSwitchStatements.Add((StatementSyntax)statement);
				}
			}
			else {
				defaultSwitchStatements = defaultSwitchStatements.Add((StatementSyntax)ifStatement.Else!.ChildNodes().First());
			}
			

			defaultSwitchStatements = defaultSwitchStatements.Add(SyntaxFactory.BreakStatement());
			
			SwitchSectionSyntax defaultSection = SyntaxFactory.SwitchSection(defaultSwitchLabels, defaultSwitchStatements);
			switchSectionsList = switchSectionsList.Add(defaultSection);
		}
	
		return switchSectionsList;
	}
}