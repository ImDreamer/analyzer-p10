﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Selection; 

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class MakeSwitchAnalyser : DiagnosticAnalyzer {
	public const string DiagnosticId = "MakeSwitch";
	List<string> constIdentifierList = new List<string>();
	
	public static readonly DiagnosticDescriptor MakeSwitchRule =
		new (DiagnosticId,
			"Use switch",
			"Using switch is usually more efficient when there is more than 3 statements.",
			"Usage",
			DiagnosticSeverity.Warning,
			true);
	
	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(MakeSwitchRule);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.RegisterSyntaxNodeAction(AnalyzeBlock, SyntaxKind.Block);
		context.EnableConcurrentExecution();
	}

	private void AnalyzeBlock(SyntaxNodeAnalysisContext context) {
		List<StatementSyntax> statementsWithConstOrNumeric = new List<StatementSyntax>();
		
		
		
		// Goes through all the statements in the block to find declarations which declares a const
		foreach (var statement in ((BlockSyntax)context.Node).Statements) {
			if (statement is LocalDeclarationStatementSyntax) {
				
				// If the declaration is a const add the identifier name to a list
				if (((LocalDeclarationStatementSyntax)statement).Modifiers.Any(SyntaxKind.ConstKeyword)) {
					constIdentifierList.Add(((LocalDeclarationStatementSyntax)statement).
						Declaration.Variables.First().Identifier.ToString().Trim());
				}
			}
		}
		
		// goes through all the statements in the block to find if statements, and if the if statement does not 
		// contain a const in its condition create a diagnostic
		foreach (StatementSyntax statement in ((BlockSyntax)context.Node).Statements) {
			if (statement is IfStatementSyntax ifStatement) {
				int elseIfStatements = CountElseIfStatements(ifStatement, 0);

				// Go through each of the childNodes for the condition to see if one of them is const or a NumericLiteralExpression
				foreach (var childNode in ifStatement.Condition.ChildNodes()) {
					// If the condition contains a const or NumericLiteralExpression, add the statement to a list
					if ((constIdentifierList.Contains(childNode.ToString().Trim()) 
					     || childNode.Kind() is SyntaxKind.NumericLiteralExpression) 
					    && !statementsWithConstOrNumeric.Contains(ifStatement)) {
							statementsWithConstOrNumeric.Add(ifStatement);
					}
				}
				
				// If the if statement condition contain a const or numeric,
				// and there is 2 or more else if statements create a diagnostic
				if (statementsWithConstOrNumeric.Contains(ifStatement) && elseIfStatements >= 2 && !MoreThanOneVariable(ifStatement)) {
					context.ReportDiagnostic(Diagnostic.Create(MakeSwitchRule, ifStatement.IfKeyword.GetLocation()));
				}
			}
		}
	}
	
	/// <summary>
	/// Counts the amount of else if statements Subsequently in an if statement
	/// </summary>
	/// <param name="statement">The if statement which if else statements are counted</param>
	/// <param name="elseIfStatements">The amount of else if statements before this</param>
	/// <returns>The amount of else if statements counted </returns>
	private int CountElseIfStatements(IfStatementSyntax statement, int elseIfStatements) {
		
		if (statement.Else?.ChildNodes().First() is IfStatementSyntax) {
			elseIfStatements++;
			elseIfStatements = CountElseIfStatements((IfStatementSyntax)statement.Else?.ChildNodes().First()!, elseIfStatements);
		}

		return elseIfStatements;
	}

	/// <summary>
	/// Goes through an if statement and checks if it has more than one variable
	/// </summary>
	/// <param name="ifStatement">The if statement to check</param>
	/// <returns>False if there is less than 2 variables, else returns true</returns>
	private bool MoreThanOneVariable(IfStatementSyntax ifStatement) {
		List<string> allVariablesInCondition = new();

		List<string> variablesInConditions = GetAllVariablesInConditions(ifStatement, allVariablesInCondition).Distinct().ToList();
		variablesInConditions = variablesInConditions.Except(constIdentifierList).ToList();
		
		if (variablesInConditions.Count < 2) {
			return false;
		}
		
		return true;
	}
	
	/// <summary>
	/// Get variable names in a condition of an if statement
	/// </summary>
	/// <param name="statement">The if statement which condition is being looked through</param>
	/// <returns>A list of strings with the name the variables in the condition</returns>
	private List<string> GetVariablesInCondition(IfStatementSyntax statement) {
		List<string> variablesInCondition = new();

		foreach (var childNode in statement.Condition.ChildNodes()) {
			if (childNode is IdentifierNameSyntax) {
				variablesInCondition.Add(childNode.ToString().Trim());
			}
		}

		return variablesInCondition;
	}

	/// <summary>
	/// Gets all variables from the conditions in an if else if chain
	/// </summary>
	/// <param name="ifStatement">The if statement to get the subsequent variables in the conditions from</param>
	/// <param name="allVariablesInCondition">The list which the variable names is being saved in</param>
	/// <returns>The list of string with all the variable names, contains duplicates</returns>
	private List<string> GetAllVariablesInConditions(IfStatementSyntax ifStatement, List<string> allVariablesInCondition) {

		allVariablesInCondition.AddRange(GetVariablesInCondition(ifStatement));
		
		if (ifStatement.Else?.ChildNodes().First() is IfStatementSyntax) {
			allVariablesInCondition = GetAllVariablesInConditions((IfStatementSyntax)ifStatement.Else?.ChildNodes().First()!, allVariablesInCondition);
		}

		return allVariablesInCondition;
	}

}