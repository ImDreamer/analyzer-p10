﻿using System.Collections.Immutable;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace EnergyAnalyzer.Object;

[DiagnosticAnalyzer(LanguageNames.CSharp)]
public class ObjectAnalyzer : DiagnosticAnalyzer {
	public const string DiagnosticId = "ReplaceClass";

	private static readonly DiagnosticDescriptor ReplaceClass = new(DiagnosticId,
		"Replace class",
		"Using struct is more efficient than classes when the main purpose is not field access.",
		"Usage",
		DiagnosticSeverity.Warning,
		true);

	public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics =>
		ImmutableArray.Create(ReplaceClass);

	public override void Initialize(AnalysisContext context) {
		context.ConfigureGeneratedCodeAnalysis(GeneratedCodeAnalysisFlags.None);
		context.EnableConcurrentExecution();
		context.RegisterSyntaxNodeAction(AnalyzeNode, SyntaxKind.ClassDeclaration);
	}

	private void AnalyzeNode(SyntaxNodeAnalysisContext context) {
		if (ShouldBeStruct((ClassDeclarationSyntax)context.Node, context.SemanticModel)) {
			context.ReportDiagnostic(Diagnostic.Create(ReplaceClass,
				((ClassDeclarationSyntax)context.Node).Keyword.GetLocation()));
		}
	}

	private static bool ShouldBeStruct(ClassDeclarationSyntax classDeclaration, SemanticModel semanticModel) {
		if (classDeclaration.Modifiers.Any(SyntaxKind.AbstractKeyword) ||
		    classDeclaration.Modifiers.Any(SyntaxKind.StaticKeyword)) {
			return false;
		}

		if (classDeclaration.BaseList != null) {
			foreach (BaseTypeSyntax baseTypeSyntax in classDeclaration.BaseList.Types) {
				try {
					ISymbol? temp = semanticModel.LookupSymbols(0).FirstOrDefault(symbol =>
						symbol.Name == ((IdentifierNameSyntax)baseTypeSyntax.Type).Identifier.Text);
					if (temp is null || temp is ITypeSymbol { TypeKind: TypeKind.Class }) {
						return false;
					}
				}
				catch (InvalidCastException) {
					// Probably a base type that shouldn't be considered
					return false;
				}
			}
		}


		if (!classDeclaration.Members.Any(SyntaxKind.FieldDeclaration)) {
			return true;
		}

		return false;
	}
}