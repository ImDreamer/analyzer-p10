﻿using System.Collections.Immutable;
using System.Composition;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CodeActions;
using Microsoft.CodeAnalysis.CodeFixes;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace EnergyAnalyzer.Object;

[ExportCodeFixProvider(LanguageNames.CSharp, Name = nameof(ObjectCodeFix)), Shared]
public class ObjectCodeFix : CodeFixProvider {
	public override ImmutableArray<string> FixableDiagnosticIds =>
		ImmutableArray.Create(ObjectAnalyzer.DiagnosticId);

	public override FixAllProvider GetFixAllProvider() => WellKnownFixAllProviders.BatchFixer;

	public override async Task RegisterCodeFixesAsync(CodeFixContext context) {
		SyntaxNode? root = await context.Document.GetSyntaxRootAsync(context.CancellationToken).ConfigureAwait(false);

		Diagnostic diagnostic = context.Diagnostics.First();
		Microsoft.CodeAnalysis.Text.TextSpan diagnosticSpan = diagnostic.Location.SourceSpan;

		ClassDeclarationSyntax? classDeclarationSyntax = root!.FindToken(diagnosticSpan.Start).Parent!
			.AncestorsAndSelf()
			.OfType<ClassDeclarationSyntax>().FirstOrDefault();

		if (classDeclarationSyntax == null) {
			return;
		}

		CodeAction action = CodeAction.Create(
			"Make Struct",
			_ => MakeStructAsync(root, context.Document, classDeclarationSyntax),
			equivalenceKey: nameof(ObjectCodeFix));
		context.RegisterCodeFix(action, diagnostic);
	}

	private static Task<Document> MakeStructAsync(SyntaxNode root, Document document,
		ClassDeclarationSyntax classDeclarationSyntax) {
		StructDeclarationSyntax structDeclarationSyntax = SyntaxFactory.StructDeclaration(
				classDeclarationSyntax.AttributeLists, classDeclarationSyntax.Modifiers,
				SyntaxFactory.Token(SyntaxKind.StructKeyword), classDeclarationSyntax.Identifier,
				classDeclarationSyntax.TypeParameterList, classDeclarationSyntax.BaseList,
				classDeclarationSyntax.ConstraintClauses, classDeclarationSyntax.OpenBraceToken,
				classDeclarationSyntax.Members, classDeclarationSyntax.CloseBraceToken,
				classDeclarationSyntax.SemicolonToken).WithTriviaFrom(classDeclarationSyntax);

		return Task.FromResult(document.WithSyntaxRoot(root.ReplaceNode(classDeclarationSyntax, structDeclarationSyntax)));
	}
}