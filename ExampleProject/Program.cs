﻿using System.Reflection;

int x = 1;


//TODO: some of the suggestions are not working when using in top level programs
//TODO: Should we fix this?
//TODO: the class suggestion should be on class keyword and not on the entire class


public struct Test {
	void Hello() {
		Boolean b = true;
		List<int> list = new List<int>();

		list.Add(10);
		list.Add(10);
		list.Add(10);

		List<int> temp = list.Where(i => i == 0).ToList();

		string t = "hello" + "world" + 10 + 21;
	}

	private static readonly InvocationHelper InstanceObject = new();

	private static readonly MethodInfo MethodReflection = typeof(InvocationHelper).GetMethod("Test");

	void Method() {
		MethodReflection.Invoke(InstanceObject, new object[] { 10 });
	}

	public class InvocationHelper {
		public void Test(int a) { }
	}
}