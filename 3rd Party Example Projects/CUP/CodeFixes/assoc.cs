namespace CUP
{
	using System;
	
	/* Defines integers that represent the associativity of terminals
	* @version last updated: 7/3/96
	* @author  Frank Flannery
	*/
	
	public class assoc
	{
		
		/* various associativities, no_prec being the default value */
#pragma warning disable Makeuint
		public const int left = 0;
#pragma warning restore Makeuint
#pragma warning disable Makeuint
		public const int right = 1;
#pragma warning restore Makeuint
#pragma warning disable Makeuint
		public const int nonassoc = 2;
#pragma warning restore Makeuint
		public static int no_prec = - 1;
	}
}