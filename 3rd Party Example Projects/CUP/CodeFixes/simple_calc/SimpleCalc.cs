using CsharpRAPL.Benchmarking.Attributes;
using CUP.runtime;
using simple_calc;

namespace SimpleCalcFixes {
	public class SimpleCalc {
		public static ulong Iterations;
		public static ulong LoopIterations;

		private const string Expression = " 5 * 2 / 0.5 + 17 - 31 + 714 * 17";

		static double Calculate(string input, bool debug) {
			string strText = input + ";";
			double result = 0;
			System.Text.Encoding encoding = System.Text.Encoding.Default;
			byte[] bytes = encoding.GetBytes(strText);
			MemoryStream stream = new MemoryStream(bytes);
			parser parser_obj = new parser(new scanner(stream));

			/* open input files, etc. here */
			Symbol parse_tree = null;

			if (debug) {
				try {
					parse_tree = parser_obj.debug_parse();
				}
				catch (Exception ex) {
					Console.WriteLine("Exception=" + ex);
					Console.WriteLine(ex.StackTrace);
					parse_tree = null;
				}
			}
			else {
				try {
					parse_tree = parser_obj.parse();
				}
				catch (Exception ex) {
					Console.WriteLine("Exception=" + ex);
					Console.WriteLine(ex.StackTrace);
					parse_tree = null;
				}
			}

			try {
				if (parse_tree != null) {
					result = decimal.ToDouble((decimal)parse_tree.Value);
				}
			}
			catch (Exception ex) {
				Console.WriteLine(ex.StackTrace);
			}

			return result;
		}

		[Benchmark("CUP", "CUP with code fixes", plotOrder: 1)]
		public double CodeFixes() {
			double result = 0;
			for (ulong i = 0; i < LoopIterations; i++) {
				result += Calculate(Expression, false);
			}

			return result;
		}
	}
}