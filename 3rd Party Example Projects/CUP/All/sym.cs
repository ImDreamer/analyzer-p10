//----------------------------------------------------
// The following code was generated by CUPFixes v0.10k
// Sun Jul 25 13:35:26 EDT 1999
//----------------------------------------------------
namespace CUP
{
	using System;
	
	/// <summary>CUPFixes generated class containing symbol constants. 
	/// </summary>
	public class sym
	{
		/* terminals */
		public const uint NON = 8;
		public const uint NONTERMINAL = 27;
		public const uint STAR = 15;
		public const uint SEMI = 13;
		public const uint CODE = 4;
#pragma warning disable Makeuint
		public const int EOF = 0;
#pragma warning restore Makeuint
		public const uint NONASSOC = 23;
		public const uint LEFT = 21;
		public const uint PACKAGE = 2;
#pragma warning disable Makeuint
		public const int COLON = 17;
#pragma warning restore Makeuint
		public const uint WITH = 11;
		public const uint IMPORT = 3;
		public const uint error = 1;
#pragma warning disable Makeuint
		public const int COLON_COLON_EQUALS = 18;
#pragma warning restore Makeuint
		public const uint COMMA = 14;
		public const uint DOT = 16;
		public const uint SCAN = 10;
#pragma warning disable Makeuint
		public const int ID = 28;
#pragma warning restore Makeuint
		public const uint INIT = 9;
		public const uint PARSER = 6;
		public const uint TERMINAL = 7;
		public const uint PRECEDENCE = 20;
		public const uint LBRACK = 25;
		public const uint RBRACK = 26;
#pragma warning disable Makeuint
		public const int PERCENT_PREC = 24;
#pragma warning restore Makeuint
		public const uint START = 12;
		public const uint RIGHT = 22;
		public const uint BAR = 19;
		public const uint ACTION = 5;
#pragma warning disable Makeuint
		public const int CODE_STRING = 29;
#pragma warning restore Makeuint
	}
}