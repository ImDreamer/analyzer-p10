﻿namespace jsonparserAll.JsonModel
{
    public class JNull : JSon
    {
        public override bool IsNull => true;
    }
}