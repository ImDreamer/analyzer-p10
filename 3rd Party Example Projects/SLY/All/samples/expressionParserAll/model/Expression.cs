﻿namespace expressionParserAll.model
{
    public interface Expression
    {
        int? Evaluate(ExpressionContext context);
    }
}