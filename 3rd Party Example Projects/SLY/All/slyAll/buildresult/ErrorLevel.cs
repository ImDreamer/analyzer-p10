﻿namespace slyAll.buildresult
{
    public enum ErrorLevel
    {
        FATAL,
        ERROR,
        WARN
    }
}