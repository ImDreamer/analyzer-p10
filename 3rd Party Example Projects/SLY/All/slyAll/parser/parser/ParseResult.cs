﻿using System.Collections.Generic;
using slyAll.parser.syntax.tree;

namespace slyAll.parser.parser
{
#pragma warning disable ReplaceClass
	public class ParseResult<IN, OUT> where IN : struct
    {
        public OUT Result { get; set; }
        
        public ISyntaxNode<IN> SyntaxTree { get; set; }

        public bool IsError { get; set; }

        public bool IsOk => !IsError;

        public List<ParseError> Errors { get; set; }
    }
#pragma warning restore ReplaceClass
}