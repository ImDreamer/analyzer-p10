﻿namespace slyAll.parser.parser
{

    public enum ErrorType
    {
        UnexpectedEOS,
        UnexpectedToken,
        UnexpectedChar,
        IndentationError
    }
    
#pragma warning disable ReplaceClass
    public class ParseError
    {
        public virtual ErrorType ErrorType { get; protected set; }
#pragma warning disable Makeuint
        public virtual int Column { get; protected set; }
#pragma warning restore Makeuint
        public virtual string ErrorMessage { get; protected set; }
#pragma warning disable Makeuint
        public virtual int Line { get; protected set; }
#pragma warning restore Makeuint


        //public ParseError(int line, int column)
        //{
        //    Column = column;
        //    Line = line;
        //}

        public int CompareTo(object obj)
        {
#pragma warning disable Makeuint
	        int comparison = 0;
#pragma warning restore Makeuint
	        if (obj is ParseError unexpectedError)
            {
                uint lineComparison = (uint)Line.CompareTo(unexpectedError != null ? unexpectedError.Line : 0);
                uint columnComparison = (uint)Column.CompareTo(unexpectedError != null ? unexpectedError.Column : 0);

                if (lineComparison > 0) comparison = 1;
                if (lineComparison == 0) comparison = (int)columnComparison;
                if (lineComparison < 0) comparison = -1;
            }

            return comparison;
        }
    }
#pragma warning restore ReplaceClass
}