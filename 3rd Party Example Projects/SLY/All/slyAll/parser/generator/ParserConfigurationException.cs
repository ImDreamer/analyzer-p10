﻿using System;

namespace slyAll.parser.generator
{
    public class ParserConfigurationException : Exception
    {
        public ParserConfigurationException(string message) : base(message)
        {
        }
    }
}