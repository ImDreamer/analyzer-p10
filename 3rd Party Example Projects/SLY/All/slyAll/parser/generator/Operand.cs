﻿using System;

namespace slyAll.parser.generator
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class OperandAttribute : Attribute
    {
    }
}