﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace slyAll.lexer.fsm.transitioncheck {
	public class TransitionAnyExcept : AbstractTransitionCheck {
		private readonly List<char> TokenExceptions;

		public TransitionAnyExcept(params char[] tokens) {
			TokenExceptions = new List<char>();
			TokenExceptions.AddRange(tokens);
		}

		public TransitionAnyExcept(TransitionPrecondition precondition, params char[] tokens) {
			TokenExceptions = new List<char>();
			TokenExceptions.AddRange(tokens);
			Precondition = precondition;
		}

		[ExcludeFromCodeCoverage]
		public override string ToGraphViz() {
			StringBuilder label = new StringBuilder("");
			if (Precondition != null) label = new StringBuilder("[|] ");
			label.Append("^(");
			List<string> tokens = new List<string>();
			foreach (char tokenException in TokenExceptions) {
				tokens.Add(tokenException.ToEscaped());
			}
			label.Append(string.Join(", ", tokens));
			label.Append(")");
			return new StringBuilder(@"[ label=""").Append(label).Append(@""" ]").ToString();
		}

		public override bool Match(char input) {
			return !TokenExceptions.Contains(input);
		}
	}
}