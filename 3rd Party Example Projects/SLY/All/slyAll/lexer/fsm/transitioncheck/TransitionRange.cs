﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace slyAll.lexer.fsm.transitioncheck
{
    public class TransitionRange : AbstractTransitionCheck
    {
        private readonly char RangeEnd;
        private readonly char RangeStart;

        public TransitionRange(char start, char end)
        {
            RangeStart = start;
            RangeEnd = end;
        }


        public TransitionRange(char start, char end, TransitionPrecondition precondition)
        {
            RangeStart = start;
            RangeEnd = end;
            Precondition = precondition;
        }

        [ExcludeFromCodeCoverage]
        public override string ToGraphViz()
        {
            StringBuilder t = new StringBuilder("");
            if (Precondition != null) t = new StringBuilder("[|] ");
            t.Append("[");
            t.Append(RangeStart.ToEscaped());
            t.Append("-");
            t.Append(RangeEnd.ToEscaped());
            t.Append("]");
            return new StringBuilder(@"[ label=""").Append(t).Append(@""" ]").ToString();
        }


        public override bool Match(char input)
        {
            return input.CompareTo(RangeStart) >= 0 && input.CompareTo(RangeEnd) <= 0;
        }
    }
}