﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace slyAll.lexer.fsm.transitioncheck
{
    public class TransitionMany : AbstractTransitionCheck
    {
        private readonly char[] TransitionToken;

        public TransitionMany(char[] token)
        {
            TransitionToken = token;
        }


        public TransitionMany(char[] token, TransitionPrecondition precondition)
        {
            TransitionToken = token;
            Precondition = precondition;
        }

        [ExcludeFromCodeCoverage]
        public override string ToGraphViz()
        {
            StringBuilder t = new StringBuilder("");
            if (Precondition != null) t = new StringBuilder("[|] ");
            t.Append("[");
            List<string> tokens = new List<string>();
            foreach (char transitionToken in TransitionToken) {
	            tokens.Add(transitionToken.ToEscaped());
            }
            t.Append(string.Join(", ", tokens));
            t.Append("]");
            return new StringBuilder(@"[ label=""").Append(t).Append(@""" ]").ToString();
        }

        public override bool Match(char input)
        {
            return TransitionToken.Contains<char>(input);
        }
    }
}