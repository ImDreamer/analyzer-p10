﻿namespace slyAll.lexer.fsm
{
#pragma warning disable ReplaceClass
	public class FSMNode<N>
    {
        internal FSMNode(N value)
        {
            Value = value;
        }

        internal N Value { get; set; }

#pragma warning disable Makeuint
        internal int Id { get; set; } = 0;
#pragma warning restore Makeuint

        internal bool IsEnd { get; set; } = false;

        internal bool IsStart { get; set; } = false;
        public string Mark { get; internal set; }
        public bool IsLineEnding { get; set; }
    }
#pragma warning restore ReplaceClass
}