﻿using System;
using System.Collections.Generic;
using System.Text;
using slyAll.lexer.fsm.transitioncheck;

namespace slyAll.lexer.fsm
{
    public class FSMTransition
    {
#pragma warning disable Makeuint
	    public int FromNode;
#pragma warning restore Makeuint

#pragma warning disable Makeuint
	    public int ToNode;
#pragma warning restore Makeuint

	    internal FSMTransition(AbstractTransitionCheck check, int from, int to)
        {
            Check = check;
            FromNode = from;
            ToNode = to;
        }

        public AbstractTransitionCheck Check { get; set; }


        public string ToGraphViz<N>(Dictionary<int, FSMNode<N>> nodes)
        {
	        StringBuilder f = new StringBuilder("\"").Append(nodes[FromNode].Mark ?? "").Append(" #").Append(FromNode).Append("\"");
	        StringBuilder t = new StringBuilder("\"").Append(nodes[ToNode].Mark ?? "").Append(" #").Append(ToNode).Append("\"");
	        return new StringBuilder().Append(f).Append(" -> ").Append(t).Append(" ").Append(Check.ToGraphViz()).ToString();
        }


        internal bool Match(char token, ReadOnlyMemory<char> value)
        {
            return Check.Check(token, value);
        }

        internal bool Match(char token)
        {
            return Check.Match(token);
        }
    }
}