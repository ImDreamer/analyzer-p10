namespace slyAll.lexer.attributes
{
    public class AlphaNumIdAttribute : LexemeAttribute
    {
        public AlphaNumIdAttribute() : base(GenericToken.Identifier,IdentifierType.AlphaNumeric)
        {
            
        } 
    }
}