namespace slyAll.lexer.attributes
{
    public class IntAttribute : LexemeAttribute
    {
        public IntAttribute() : base(GenericToken.Int)
        {
            
        }
    }
}