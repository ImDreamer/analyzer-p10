namespace slyAll.lexer.attributes
{
    public class DoubleAttribute : LexemeAttribute
    {
        public DoubleAttribute() : base(GenericToken.Double)
        {
            
        }
    }
}