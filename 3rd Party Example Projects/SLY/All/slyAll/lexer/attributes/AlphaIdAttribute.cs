namespace slyAll.lexer.attributes
{
    public class AlphaIdAttribute : LexemeAttribute
    {
        public AlphaIdAttribute() : base(GenericToken.Identifier,IdentifierType.Alpha)
        {
            
        } 
    }
}