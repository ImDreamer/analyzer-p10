namespace slyAll.lexer.attributes
{
    public class KeywordAttribute : LexemeAttribute
    {
        public KeywordAttribute(string keyword) : base(GenericToken.KeyWord, keyword)
        {
            
        }
    }
}