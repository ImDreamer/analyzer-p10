namespace slyAll.lexer.attributes
{
    public class ExtensionAttribute : LexemeAttribute
    {
        public ExtensionAttribute() : base(GenericToken.Extension)
        {
            
        } 
    }
}