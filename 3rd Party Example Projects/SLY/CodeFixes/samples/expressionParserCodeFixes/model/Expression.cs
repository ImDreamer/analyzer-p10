﻿namespace expressionParserCodeFixes.model
{
    public interface Expression
    {
        int? Evaluate(ExpressionContext context);
    }
}