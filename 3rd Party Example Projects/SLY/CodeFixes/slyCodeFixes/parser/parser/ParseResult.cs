﻿using System.Collections.Generic;
using slyCodeFixes.parser.syntax.tree;

namespace slyCodeFixes.parser
{
#pragma warning disable ReplaceClass
	public class ParseResult<IN, OUT> where IN : struct
    {
        public OUT Result { get; set; }
        
        public ISyntaxNode<IN> SyntaxTree { get; set; }

        public bool IsError { get; set; }

        public bool IsOk => !IsError;

        public List<ParseError> Errors { get; set; }
    }
#pragma warning restore ReplaceClass
}