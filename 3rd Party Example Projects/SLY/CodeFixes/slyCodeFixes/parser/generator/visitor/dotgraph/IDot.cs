namespace slyCodeFixes.parser.generator.visitor.dotgraph
{
    public interface IDot
    {
        string ToGraph();
    }
}