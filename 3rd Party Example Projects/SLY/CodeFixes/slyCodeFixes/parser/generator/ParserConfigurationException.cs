﻿using System;

namespace slyCodeFixes.parser.generator
{
    public class ParserConfigurationException : Exception
    {
        public ParserConfigurationException(string message) : base(message)
        {
        }
    }
}