﻿using System;

namespace slyCodeFixes.parser.generator
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class OperandAttribute : Attribute
    {
    }
}