using System;

namespace slyCodeFixes.lexer
{
    public class TokenCallbackAttribute : Attribute
    {
#pragma warning disable Makeuint
	    public int EnumValue { get; set; }
#pragma warning restore Makeuint

	    public TokenCallbackAttribute(int enumValue)
        {
            EnumValue = enumValue;
        }
    }
}