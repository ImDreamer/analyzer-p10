using System.Collections.Generic;
namespace slyCodeFixes.lexer
{
    public struct LexerResult<IN> where IN : struct
    {
        public bool IsError { get; set; }

        public bool IsOk => !IsError;
        
        public LexicalError Error { get; }
        
        public List<Token<IN>> Tokens { get; set; }
        
        public LexerResult(List<Token<IN>> tokens)
        {
            IsError = false;
            Tokens = tokens;
            Error = default;
        }

        public LexerResult(LexicalError error)
        {
            IsError = true;
            Error = error;
            Tokens = default;
        }
        
    }
}