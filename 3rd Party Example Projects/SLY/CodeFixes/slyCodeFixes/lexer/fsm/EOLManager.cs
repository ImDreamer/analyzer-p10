using System;
using System.Collections.Generic;

namespace slyCodeFixes.lexer.fsm
{
    public static class EOLManager
    {
        public static ReadOnlyMemory<char> GetToEndOfLine(ReadOnlyMemory<char> value, int position)
        {
            uint CurrentPosition = (uint)position;
            var spanValue = value.Span;
            var current = spanValue[(int)CurrentPosition];
            var end = IsEndOfLine(value, (int)CurrentPosition);
            while (CurrentPosition < value.Length && end == EOLType.No)
            {
                CurrentPosition++;
                end = IsEndOfLine(value, (int)CurrentPosition);
            }

            return value.Slice(position, (int)(CurrentPosition - position + (end == EOLType.Windows ? 2 : 1)));
        }

        public static EOLType IsEndOfLine(ReadOnlyMemory<char> value, int position)
        {
            var end = EOLType.No;
            var n = value.At<char>(position);
            switch (n)
            {
                case '\n':
                    end = EOLType.Nix;
                    break;
                case '\r' when value.At<char>(position + 1) == '\n':
                    end = EOLType.Windows;
                    break;
                case '\r':
                    end = EOLType.Mac;
                    break;
            }

            return end;
        }

        public static List<int> GetLinesLength(ReadOnlyMemory<char> value)
        {
            var lineLengths = new List<int>();
            var lines = new List<string>();
            uint previousStart = 0;
            uint i = 0;
            while (i < value.Length)
            {
                var end = IsEndOfLine(value, (int)i);
                if (end != EOLType.No)
                {
                    if (end == EOLType.Windows) i ++;
                    var line = value.Slice((int)previousStart, (int)(i - previousStart));
                    lineLengths.Add(line.Length);
                    lines.Add(line.ToString());
                    previousStart = i + 1;
                }

                i++;
            }

            lineLengths.Add(value.Slice((int)previousStart, (int)(i - previousStart)).Length);
            return lineLengths;
        }
    }
}