﻿using System;
using System.Collections.Generic;
using slyCodeFixes.lexer.fsm;

namespace slyCodeFixes.lexer
{
    /// <summary>
    ///     T is the token type
    /// </summary>
    /// <typeparam name="T">T is the enum Token type</typeparam>
    public class Lexer<T> : ILexer<T> where T : struct
    {
        public string I18n { get; set; }
        public LexerPostProcess<T> LexerPostProcess { get; set; }

        private readonly IList<TokenDefinition<T>> tokenDefinitions = new List<TokenDefinition<T>>();

        public void AddDefinition(TokenDefinition<T> tokenDefinition)
        {
            tokenDefinitions.Add(tokenDefinition);
        }


        public LexerResult<T> Tokenize(string source)
        {
            List<Token<T>> tokens = new List<Token<T>>();
            
#pragma warning disable Makeuint
            var currentIndex = 0;
#pragma warning restore Makeuint
            //List<Token<T>> tokens = new List<Token<T>>();
#pragma warning disable Makeuint
            var currentLine = 1;
#pragma warning restore Makeuint
#pragma warning disable Makeuint
            var currentColumn = 0;
#pragma warning restore Makeuint
#pragma warning disable Makeuint
	        var currentLineStartIndex = 0;
#pragma warning restore Makeuint
	        Token<T> previousToken = null;

            while (currentIndex < source.Length)
            {
                currentColumn = currentIndex - currentLineStartIndex + 1;
                TokenDefinition<T> matchedDefinition = null;
                var matchLength = 0;

                foreach (var rule in tokenDefinitions)
                {
                    var match = rule.Regex.Match(source.Substring(currentIndex));

                    if (match.Success && match.Index == 0)
                    {
                        matchedDefinition = rule;
                        matchLength = match.Length;
                        break;
                    }
                }

                if (matchedDefinition == null)
                {
                    return new LexerResult<T>(new LexicalError(currentLine, currentColumn, source[currentIndex],I18n));
                }

                var value = source.Substring(currentIndex, matchLength);

                if (matchedDefinition.IsEndOfLine)
                {
                    currentLineStartIndex = currentIndex + matchLength;
                    currentLine++;
                }

                if (!matchedDefinition.IsIgnored)
                {
                    previousToken = new Token<T>(matchedDefinition.TokenID, value,
                        new LexerPosition(currentIndex, currentLine, currentColumn));
                    tokens.Add(previousToken);
                }

                currentIndex += matchLength;
            }

            var eos = new Token<T>();
            if (previousToken != null)
            {
                eos.Position = new LexerPosition(previousToken.Position.Index + 1, previousToken.Position.Line,
                    previousToken.Position.Column + previousToken.Value.Length);
            }
            else
            {
                eos.Position = new LexerPosition(0,0,0);
            }


            tokens.Add(eos);
            return new LexerResult<T>(tokens);
        }

        public LexerResult<T> Tokenize(ReadOnlyMemory<char> source)
        {
            return new LexerResult<T>(new LexicalError(0, 0, '.',I18n));
        }
    }
}