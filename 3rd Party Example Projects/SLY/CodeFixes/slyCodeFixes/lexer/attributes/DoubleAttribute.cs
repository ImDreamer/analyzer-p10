namespace slyCodeFixes.lexer
{
    public class DoubleAttribute : LexemeAttribute
    {
        public DoubleAttribute() : base(GenericToken.Double)
        {
            
        }
    }
}