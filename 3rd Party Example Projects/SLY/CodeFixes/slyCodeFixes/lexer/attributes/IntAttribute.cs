namespace slyCodeFixes.lexer
{
    public class IntAttribute : LexemeAttribute
    {
        public IntAttribute() : base(GenericToken.Int)
        {
            
        }
    }
}