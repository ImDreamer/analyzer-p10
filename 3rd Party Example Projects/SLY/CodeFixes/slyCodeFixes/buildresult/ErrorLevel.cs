﻿namespace slyCodeFixes.buildresult
{
    public enum ErrorLevel
    {
        FATAL,
        ERROR,
        WARN
    }
}