﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Selection; 

using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Selection.MakeSwitchAnalyser>;

public class SwitchAnalyzerTests {
	[Test]
	public async Task SwitchTest01_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										
										switch (a) {
											case 1: 
												a = 2; 
												break;
											case 2:
												a = 3;
												break;
											case 3:
												a = 1;
												break;
										}
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}
	
	[Test]
	public async Task SwitchTest02_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										
										if (a == 1) {
											a = 2; 
										} 
										else if (a == 2) {
											a = 3;
										} 
										else {
											a = 1;								
										}
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task SwitchTest03_Diagnostic() {
		const string testCode = @"class Test { 
    								void Method(){ 
   										const int a = 2; 
   										int b = 0;
    
   										if (a > 0) {b = 0;}
   										else if (a > 0) {b = 1;}
   										else if (a == 0) {b = 2;} 
   										else {b = 3;}
   									}
   								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeSwitch").WithSpan(6, 14, 6, 16)
				.WithMessage("Using switch is usually more efficient when there is more than 3 statements.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task SwitchTest04_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										int b = 0;

										if (a > 0) {
											if (b == 0) {b = 1;}
											else if (b == 1){b = 2;}
											else {b = 0;}
										}
										else if (a == 0) {b = 2;} 
										else {b = 3;}
										}
   								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}
    
	[Test]
	public async Task SwitchTest05_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 

										if (a > 0) {a = 0;}
										else {a = 3;}
									}
								}";

		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}
	
	[Test]
	public async Task SwitchTest06_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 

										if (a > 0) {a = 0;}
										else if (a > 0) {a = 1;}
										else if (a == 0) {a = 2;} 
										else {a = 3;}
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeSwitch").WithSpan(5, 11, 5, 13)
				.WithMessage("Using switch is usually more efficient when there is more than 3 statements.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task SwitchTest07_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 

										if (a > 0) {a = 0;}
										else if (a > 0) {a = 1;}
										else if (a == 0) {a = 2;} 
										else if (a < 0) {a = 3;}
										else {a = 4;}
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeSwitch").WithSpan(5, 11, 5, 13)
				.WithMessage("Using switch is usually more efficient when there is more than 3 statements.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task SwitchTest08_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										int b = 0;

										if (a == 0) {
											if (b == 0) {b = 1;}
											else if (b == 1){b = 2;}
											else if (b == 2){b = 3;}
											else {b = 0;}
										}
										else if (a == 1) {b = 2;} 
										else if (a == 2) {b = 3;}
										else {b = 4;}
										}
   								}";

		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeSwitch").WithSpan(6, 11, 6, 13)
				.WithMessage("Using switch is usually more efficient when there is more than 3 statements.");
		DiagnosticResult expected2 =
			DiagnosticResult.CompilerWarning("MakeSwitch").WithSpan(7, 12, 7, 14)
				.WithMessage("Using switch is usually more efficient when there is more than 3 statements.");

		await Verify.VerifyAnalyzerAsync(testCode, expected , expected2);
	}
	
	[Test]
	public async Task SwitchTest09_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										int b = 0;

										if (a > b) {
											a = 7;
											b = 7;
										}
										else if (a == b) {
											b = 14;
										} 
										else {
											b = 2;
										}
									}
   								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task SwitchTest10_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										int b = 0;

										if (a > 0) {
											a = 7;
											b = 7;
										}
										else if (b == 2) {
											b = 14;
										} 
										else {
											b = 2;
										}
									}
   								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task SwitchTest11_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										int b = 0;

										if (a > b) {
											a = 7;
											b = 7;
										}
										else if (a == 2) {
											b = 14;
										} 
										else {
											b = 2;
										}
									}
   								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task SwitchTest12_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										int b = 0;

										if (a > b) {
											a = 7;
											b = 7;
										}
										else if (a == 2) {
											b = 14;
										} else if (a == 17) {
											a = 4;
											b = 4;
										} else if (b == 13) {
											a = 3;
											b = 7;
										} else {
											b = 2;
										}
									}
   								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task SwitchTest13_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										int b = 0;

										if (a > 0) {
											a = 7;
											b = 7;
										}
										else if (a == 2) {
											b = 14;
										} else if (a == 17) {
											a = 4;
											b = 4;
										} else if (a == 13) {
											a = 3;
											b = 7;
										} else {
											b = 2;
										}
									}
   								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeSwitch").WithSpan(6, 11, 6, 13)
				.WithMessage("Using switch is usually more efficient when there is more than 3 statements.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task SwitchTest14_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										int b = 0;

										if (a > b) {
											a = 7;
											b = 7;
										}
										else if (a == 2) {
											b = 14;
										} else if (a == 17) {
											a = 4;
											b = 4;
										} else if (b == a) {
											a = 3;
											b = 7;
										} else {
											b = 2;
										}
									}
   								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task SwitchTest15_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										int b = 0;

										if (a > 0) {
											a = 7;
											b = 7;
										}
										else if (a == 2) {
											b = 14;
										} else if (a == 17) {
											a = 4;
											b = 4;
										} else if (b == 13) {
											a = 3;
											b = 7;
										} else {
											b = 2;
										}
									}
   								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task SwitchTest16_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 2; 
										const int b = 0;

										if (a > 0) {
											a = 7;
										}
										else if (a == 2) {
											a = 14;
										} else if (a == b) {
											a = 4;
										} else if (a > 13) {
											a = 3;
										} else {
											a = 2;
										}
									}
   								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeSwitch").WithSpan(6, 11, 6, 13)
				.WithMessage("Using switch is usually more efficient when there is more than 3 statements.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}