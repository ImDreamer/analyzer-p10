﻿using System.Threading.Tasks;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Selection; 

using VerifyCS =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Selection.MakeSwitchAnalyser, EnergyAnalyzer.Selection.MakeSwitchCodeFixProvider>;


public class SwitchCodeFixTests {
	
	[Test]
	public async Task SwitchTest01_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
	void method(){
		int a = 2; 
   		int b = 0;
    
   		[|if|] (a > 1) {b = 0;}
   		else if (a > 0) {b = 1;}
   		else if (a == 0) {b = 2;} 
   		else {b = 3;}
	}
}
", @"using System;

class Program
{
	void method()
	{
		int a = 2;
		int b = 0;
		switch (a)
		{
			case> 1:
				b = 0;
				break;
			case> 0:
				b = 1;
				break;
			case 0:
				b = 2;
				break;
			default:
				b = 3;
				break;
		}
	}
}");
	}
	
	[Test]
	public async Task SwitchTest02_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
	void method(){
		int a = 2; 
		const int b = 0;

		[|if|] (a > 13) {
			a = 7;
		} else if (a == 2) {
			a = 14;
		} else if (a == b) {
			a = 4;
		} else if (a > 0) {
			a = 3;
		} else {
			a = 2;
		}
	}
}
", @"using System;

class Program
{
	void method()
	{
		int a = 2;
		const int b = 0;
		switch (a)
		{
			case> 13:
				a = 7;
				break;
			case 2:
				a = 14;
				break;
			case b:
				a = 4;
				break;
			case> 0:
				a = 3;
				break;
			default:
				a = 2;
				break;
		}
	}
}");
	}
	
	[Test]
	public async Task SwitchTest03_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
	void method(){
		int a = 2; 
		const int b = 0;

		[|if|] (a > 13) {
			a = 7;
		} else if (a == 2) {
			a = 14;
		} else if (b == a) {
			a = 4;
		} else if (a > 0) {
			a = 3;
		} else {
			a = 2;
		}
	}
}
", @"using System;

class Program
{
	void method()
	{
		int a = 2;
		const int b = 0;
		switch (a)
		{
			case> 13:
				a = 7;
				break;
			case 2:
				a = 14;
				break;
			case b:
				a = 4;
				break;
			case> 0:
				a = 3;
				break;
			default:
				a = 2;
				break;
		}
	}
}");
	}
	
	[Test]
	public async Task SwitchTest04_NoFix() {
		const string testCode = @"using System;

class Program
{
	void method()
	{
		int a = 2;
		const int b = 0;
		switch (a)
		{
			case> 13:
				a = 7;
				break;
			case 2:
				a = 14;
				break;
			case b:
				a = 4;
				break;
			case> 0:
				a = 3;
				break;
			default:
				a = 2;
				break;
		}
	}
}";
		await VerifyCS.VerifyCodeFixAsync(testCode, testCode);
	}
	
	[Test]
	public async Task SwitchTest05_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
	void method(){
		long a = 2; 
		const long b = 0;

		[|if|] (a > 13) {
			a = 7;
		} else if (a == 2) {
			a = 14;
		} else if (b == a) {
			a = 4;
		} else if (a > 0) {
			a = 3;
		} else {
			a = 2;
		}
	}
}
", @"using System;

class Program
{
	void method()
	{
		long a = 2;
		const long b = 0;
		switch (a)
		{
			case> 13:
				a = 7;
				break;
			case 2:
				a = 14;
				break;
			case b:
				a = 4;
				break;
			case> 0:
				a = 3;
				break;
			default:
				a = 2;
				break;
		}
	}
}");
	}
	
	[Test]
	public async Task SwitchTest06_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
	void method(){
		long a = 2; 
		const long b = 3;

		[|if|] (a > b) {
			a = 7;
		} else if (a == 2) {
			a = 14;
		} else if (b == a) {
			a = 4;
		} else if (a > 0) {
			a = 3;
		} else {
			a = 2;
		}
	}
}
", @"using System;

class Program
{
	void method()
	{
		long a = 2;
		const long b = 3;
		switch (a)
		{
			case> b:
				a = 7;
				break;
			case 2:
				a = 14;
				break;
			case b:
				a = 4;
				break;
			case> 0:
				a = 3;
				break;
			default:
				a = 2;
				break;
		}
	}
}");
	}
	
	[Test]
	public async Task SwitchTest07_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
	void method(){
		long a = 2; 
		const long b = 3;

		[|if|] (a > b) {
			a = 7;
			a++;
			a *= 5;
		} else if (a == 2) {
			a = 14;
			Console.WriteLine(""Hi!"");
		} else if (b == a) {
			a = 4;
			a *= 2;
		} else if (a > 0) {
			a = 3;
			++a;
		} else {
			a = 2;
			a--;
		}
	}
}
", @"using System;

class Program
{
	void method()
	{
		long a = 2;
		const long b = 3;
		switch (a)
		{
			case> b:
				a = 7;
				a++;
				a *= 5;
				break;
			case 2:
				a = 14;
				Console.WriteLine(""Hi!"");
				break;
			case b:
				a = 4;
				a *= 2;
				break;
			case> 0:
				a = 3;
				++a;
				break;
			default:
				a = 2;
				a--;
				break;
		}
	}
}");
	}
	
	[Test]
	public async Task SwitchTest08_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
	void method(){
		long sec = 2; 
		string pad = """";

		[|if|] (sec < 10)
			pad = ""   "";
		else if (sec < 100)
			pad = ""  "";
		else if (sec < 1000)
			pad = "" "";
		else
			pad = """";
	}
}
", @"using System;

class Program
{
	void method()
	{
		long sec = 2;
		string pad = """";
		switch (sec)
		{
			case < 10:
				pad = ""   "";
				break;
			case < 100:
				pad = ""  "";
				break;
			case < 1000:
				pad = "" "";
				break;
			default:
				pad = """";
				break;
		}
	}
}");
	}
	
	[Test]
	public async Task SwitchTest09_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
	void method(){
		long sec = 2; 
		string pad = """";

		[|if|] (sec < 10) {
			pad = ""   "";
		} else if (sec < 100) {
			pad = ""  "";
		} else if (sec < 1000) {
			pad = "" "";
		} else {
			pad = """";
		}
	}
}
", @"using System;

class Program
{
	void method()
	{
		long sec = 2;
		string pad = """";
		switch (sec)
		{
			case < 10:
				pad = ""   "";
				break;
			case < 100:
				pad = ""  "";
				break;
			case < 1000:
				pad = "" "";
				break;
			default:
				pad = """";
				break;
		}
	}
}");
	}
}