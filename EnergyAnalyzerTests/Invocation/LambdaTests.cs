﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Invocation; 

using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Invocation.LambdaAnalyzerV2>;

public class LambdaTests {
	[Test]
	public async Task LambdaTest01_Diagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method(){ 
										uint a = 5;
										Func<uint> test = () => a + 5; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("RemoveClosure").WithSpan(6, 29, 6, 40)
				.WithMessage("Using parameters in lambda expressions is usually more efficient than accessing outside variables directly.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task LambdaTest02_NoDiagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method(){ 
										uint a = 5;
										Func<uint, uint> test = value => value + 5; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task LambdaTest03_NoDiagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method(){ 
										uint a = 5;
										Func<uint> test = () => {
											uint value = 2;
											return value + 5; 
										};
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task LambdaTest04_Diagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									delegate uint Lambda();
									void Method(){ 
										uint a = 5;
										Lambda test = () => {
											uint value = a;
											return value + 5; 
										};
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("RemoveClosure").WithSpan(7, 25, 10, 12)
				.WithMessage("Using parameters in lambda expressions is usually more efficient than accessing outside variables directly.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task LambdaTest05_NoDiagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									delegate uint Lambda();
									void Method(){ 
										uint a = 5;
										Lambda test = () => {
											uint value = 2;
											return value + 5; 
										};
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task LambdaTest06_NoDiagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method(){ 
										uint a = 5;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task LambdaTest07_NoDiagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									delegate uint Lambda(uint other);
									void Method(){ 
										uint a = 5;
										Lambda test = other => {
											uint value = other;
											return value + 5; 
										};
										test(a);
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task LambdaTest08_NoDiagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method(){
										Func<uint> test = () => 5; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task LambdaTest09_Diagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method(){ 
										uint a = 5;
										Func<uint> test = () => a + 5; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("RemoveClosure").WithSpan(6, 29, 6, 40)
				.WithMessage("Using parameters in lambda expressions is usually more efficient than accessing outside variables directly.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}