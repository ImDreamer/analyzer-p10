﻿using System.Threading.Tasks;
using NUnit.Framework;
using VerifyCS =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Invocation.ReflectionAnalyzer,
		EnergyAnalyzer.Invocation.ReflectionCodeFixProvider>;

namespace EnergyAnalyzerTests.Invocation;

public class ReflectionCodeFixTests {
	[Test]
	public async Task ReflectionTest01_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Test() { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										[|MethodReflection.Invoke|](InstanceObject, Array.Empty<object>());
									}
								}
								";
		const string expected = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Test() { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										MethodReflectionDelegate(InstanceObject);
									}

									private static readonly Action<InvocationHelper> MethodReflectionDelegate = (Action<InvocationHelper>)Delegate.CreateDelegate(typeof(Action<InvocationHelper>), MethodReflection);
								}
								";
		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task ReflectionTest02_NoDiagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Invoke() { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Invoke));

									private static readonly Action<InvocationHelper> ReflectionDelegate = (Action<InvocationHelper>)Delegate.CreateDelegate(typeof(Action<InvocationHelper>), MethodReflection);

									void Method() {
										ReflectionDelegate(InstanceObject);
									}
								}
								";
		await VerifyCS.VerifyCodeFixAsync(testCode, testCode);
	}

	[Test]
	public async Task ReflectionTest03_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Test() { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										" + "typeof(InvocationHelper).GetMethod(\"Test\");" + @"

									void Method() {
										[|MethodReflection.Invoke|](InstanceObject, Array.Empty<object>());
									}
								}
								";
		const string expected = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Test() { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										" + "typeof(InvocationHelper).GetMethod(\"Test\");" + @"

									void Method() {
										MethodReflectionDelegate(InstanceObject);
									}

									private static readonly Action<InvocationHelper> MethodReflectionDelegate = (Action<InvocationHelper>)Delegate.CreateDelegate(typeof(Action<InvocationHelper>), MethodReflection);
								}
								";
		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task ReflectionTest04_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public int Test() { return 0; }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										[|MethodReflection.Invoke|](InstanceObject, Array.Empty<object>());
									}
								}
								";
		const string expected = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public int Test() { return 0; }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										MethodReflectionDelegate(InstanceObject);
									}

									private static readonly Func<InvocationHelper, int> MethodReflectionDelegate = (Func<InvocationHelper, int>)Delegate.CreateDelegate(typeof(Func<InvocationHelper, int>), MethodReflection);
								}
								";
		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task ReflectionTest05_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Test(int a) { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										" + "typeof(InvocationHelper).GetMethod(\"Test\");" + @"

									void Method() {
										[|MethodReflection.Invoke|](InstanceObject, new object[] { 10 });
									}
								}
								";
		const string expected = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Test(int a) { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										" + "typeof(InvocationHelper).GetMethod(\"Test\");" + @"

									void Method() {
										MethodReflectionDelegate(InstanceObject, 10);
									}

									private static readonly Action<InvocationHelper, int> MethodReflectionDelegate = (Action<InvocationHelper, int>)Delegate.CreateDelegate(typeof(Action<InvocationHelper, int>), MethodReflection);
								}
								";
		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	//TODO: This test is not working.
	[Test, Ignore("This has a different structure using tuple expression instead of cast expression as one example")]
	public async Task ReflectionTest06_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public int Test(int a) { return a; }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										[|MethodReflection.Invoke|](InstanceObject, new object[] { 10 });
									}
								}
								";
		const string expected = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public int Test(int a) { return a; }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										MethodReflectionDelegate(InstanceObject, 10);
									}

									private static readonly Func<InvocationHelper, int, int> MethodReflectionDelegate = (Func<InvocationHelper, int, int>)Delegate.CreateDelegate(typeof(Func<InvocationHelper, int, int>), MethodReflection);
								}
								";
		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task ReflectionTest07_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Test(int a, float b) { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										" + "typeof(InvocationHelper).GetMethod(\"Test\");" + @"

									void Method() {
										[|MethodReflection.Invoke|](InstanceObject, new object[] { 10, 10.0f });
									}
								}
								";
		const string expected = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Test(int a, float b) { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										" + "typeof(InvocationHelper).GetMethod(\"Test\");" + @"

									void Method() {
										MethodReflectionDelegate(InstanceObject, 10, 10.0f);
									}

									private static readonly Action<InvocationHelper, int, float> MethodReflectionDelegate = (Action<InvocationHelper, int, float>)Delegate.CreateDelegate(typeof(Action<InvocationHelper, int, float>), MethodReflection);
								}
								";
		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	//TODO: This test is not working.
	[Test, Ignore("This has a different structure using tuple expression instead of cast expression as one example")]
	public async Task ReflectionTest08_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public int Test(int a, float b) { return a; }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										[|MethodReflection.Invoke|](InstanceObject, new object[] { 10, 10.0f });
									}
								}
								";
		const string expected = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public int Test(int a, float b) { return a; }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										MethodReflectionDelegate(InstanceObject, 10, 10.0f);
									}

									private static readonly Func<InvocationHelper, int, float, int> MethodReflectionDelegate = (Func<InvocationHelper, int, float, int>)Delegate.CreateDelegate(typeof(Func<InvocationHelper, int, float, int>), MethodReflection);
								}
								";
		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}
	
	[Test]
	public async Task ReflectionTest09_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public struct InvocationHelper {
									public void Test() { }
								}

								struct Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										[|MethodReflection.Invoke|](InstanceObject, Array.Empty<object>());
									}
								}
								";
		const string expected = @"
								using System;
								using System.Reflection;

								public struct InvocationHelper {
									public void Test() { }
								}

								struct Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										MethodReflectionDelegate(InstanceObject);
									}

									private static readonly Action<InvocationHelper> MethodReflectionDelegate = (Action<InvocationHelper>)Delegate.CreateDelegate(typeof(Action<InvocationHelper>), MethodReflection);
								}
								";
		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}
}