﻿using System.Threading.Tasks;
using NUnit.Framework;
using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Invocation.LambdaAnalyzerV2,
		EnergyAnalyzer.Invocation.LambdaCodeFixProvider>;

namespace EnergyAnalyzerTests.Invocation;

public class LambdaCodeFixTests {
	[Test]
	public async Task LambdaCodeFix_Diagnostic01() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		Func<uint> test = [|() => a + 5|];
	}
}", @"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		Func<uint, uint> test = (a) => a + 5;
	}
}");
	}
	
	[Test]
	public async Task LambdaCodeFix_Diagnostic02() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		uint b = 7;
		Func<uint> test = [|() => a + b + 5|];
	}
}", @"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		uint b = 7;
		Func<uint, uint, uint> test = (a, b) => a + b + 5;
	}
}");
	}
	
	[Test]
	public async Task LambdaCodeFix_Diagnostic03() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		uint baa = 7;
		Func<uint> test = [|() => a + baa + 5|];
	}
}", @"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		uint baa = 7;
		Func<uint, uint, uint> test = (a, baa) => a + baa + 5;
	}
}");
	}
	
	[Test]
	public async Task LambdaCodeFix_NoDiagnostic04() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		uint baa = 7;
		Func<uint, uint, uint> test = (a, baa) => a + baa + 5;
	}
}", @"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		uint baa = 7;
		Func<uint, uint, uint> test = (a, baa) => a + baa + 5;
	}
}");
	}
	
	[Test]
	public async Task LambdaCodeFix_Diagnostic05() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		uint baa = 7;
		Func<uint, uint> test = [|(a) => a + baa + 5|];
	}
}", @"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		uint baa = 7;
		Func<uint, uint, uint> test = (a, baa) => a + baa + 5;
	}
}");
	}
	
	[Test]
	public async Task LambdaCodeFix_Diagnostic06() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		uint baa = 7;
		uint cascsa = 10;
		Func<uint> test = [|() => a + baa + cascsa + 5|];
	}
}", @"
using System;
class Test { 
	void Method(){ 
		uint a = 5;
		uint baa = 7;
		uint cascsa = 10;
		Func<uint, uint, uint, uint> test = (a, baa, cascsa) => a + baa + cascsa + 5;
	}
}");
	}
	
	[Test]
	public async Task LambdaCodeFix_Diagnostic07() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method(){ 
		int a = 2;
		uint baa = 7;
		uint cascsa = 10;
		Func<uint> test = [|() => (uint)a + baa + cascsa + 5|];
	}
}", @"
using System;
class Test { 
	void Method(){ 
		int a = 2;
		uint baa = 7;
		uint cascsa = 10;
		Func<int, uint, uint, uint> test = (a, baa, cascsa) => (uint)a + baa + cascsa + 5;
	}
}");
	}
}