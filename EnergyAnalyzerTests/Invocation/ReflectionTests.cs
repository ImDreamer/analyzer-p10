﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Invocation;

using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Invocation.ReflectionAnalyzer>;

public class ReflectionTests {
	[Test]
	public async Task ReflectionTest01_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Test() { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Test));

									void Method() {
										MethodReflection.Invoke(InstanceObject, Array.Empty<object>());
									}
								}
								";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("ReplaceOrWrapReflection").WithSpan(16, 11, 16, 34)
				.WithMessage(
					"Wrapping reflection invocation in a delegate is more efficient than just using reflection. " +
					"If invoking a method directly is possible instead, that is the most efficient.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ReflectionTest02_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Invoke() { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Invoke));

									void Method() {
										MethodReflection.Invoke(InstanceObject, Array.Empty<object>());
									}
								}
								";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("ReplaceOrWrapReflection").WithSpan(16, 11, 16, 34)
				.WithMessage(
					"Wrapping reflection invocation in a delegate is more efficient than just using reflection. " +
					"If invoking a method directly is possible instead, that is the most efficient.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ReflectionTest03_NoDiagnostic() {
		const string testCode = @"
								using System;
								using System.Reflection;

								public class InvocationHelper {
									public void Invoke() { }
								}

								class Test { 
									private static readonly InvocationHelper InstanceObject = new();

									private static readonly MethodInfo MethodReflection =
										typeof(InvocationHelper).GetMethod(nameof(InvocationHelper.Invoke));

									private static readonly Action<InvocationHelper> ReflectionDelegate =
										(Action<InvocationHelper>)Delegate.CreateDelegate(
										typeof(Action<InvocationHelper>), MethodReflection);

									void Method() {
										ReflectionDelegate(InstanceObject);
									}
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task ReflectionTest04_NoDiagnostic() {
		const string testCode = @"
								class Test { 
									void Method() {
										uint a = 5;
									}
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
}