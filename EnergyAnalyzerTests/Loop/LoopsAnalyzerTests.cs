﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;
using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Loops.LoopsAnalyzer>;

namespace EnergyAnalyzerTests.Loop;

public class LoopsAnalyzerTests {
	[Test]
	public async Task LoopTest01_NoDiagnostic() {
		const string testCode = @"
	                            using System.Collections.Generic;
								class Test { 
									void Method(){ 
										uint result = 0;
										List<uint> list = new List<uint>();
										foreach(uint i in list){ 
											result += i;
										} 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task LoopTest02_NoDiagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
									    int t = 0;
										for(int i = 0; t < 10; i++){ 
											t = i; 
										} 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task LoopTest03_NoDiagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										for(int i = 0; i < 10; i++){ 
											int j = i; 
										} 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);

		int i;
		int y;
		for (i = 10, y = 0; y < i; i++, y += 2) { }
	}

	[Test]
	public async Task LoopTest04_NoDiagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										for(int i = 0; i < 10; i++){ 
										} 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task LoopTest05_NoDiagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										int[] array = new int[10];
										for(int i = 0; i < 10; i++){ 
											array[i] = i;
										} 
									}
								}";


		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task LoopTest06_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> list = new List<int>();
										for(int i = 0; i < 10; i++){ 
											list[i] = i;
										} 
									}
								}";

		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task LoopTest07_NoDiagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										int[] array = new int[10];
										for(int i = 0; i < 10; i++){ 
											int a = array[i];
										} 
									}
								}";

		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task LoopTest08_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> list = new List<int>();
										for(int i = 0; i < 10; i++){ 
											int a = list[i];
										} 
									}
								}";

		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task LoopTest09_NoDiagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										int sum = 0;
										int[] array = new int[10];
										for(int i = 0; i < 10; i++){ 
											sum += array[i];
										} 
									}
								}";

		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task LoopTest10_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> list = new List<int>();
										int sum = 0;
										for(int i = 0; i < 10; i++){ 
											sum += list[i];
										} 
									}
								}";

		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task LoopTest11_Diagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										int[] array = new int[10];
										for(int i = 0; i < array.Length; i++){ 
											int a = array[i];
										} 
									}
								}";

		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("PreferForEachOverForLoop").WithSpan(5, 11, 5, 14)
				.WithMessage(
					"When iterating through all element sequentially it has shown to be more efficient to use foreach rather than using for loops.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest12_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> list = new List<int>();
										for(int i = 0; i < list.Count; i++){ 
											int a = list[i];
										} 
									}
								}";

		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("PreferForEachOverForLoop").WithSpan(6, 11, 6, 14)
				.WithMessage(
					"When iterating through all element sequentially it has shown to be more efficient to use foreach rather than using for loops.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest13_Diagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										int sum = 0;
										int[] array = new int[10];
										for(int i = 0; i < array.Length; i++){ 
											sum += array[i];
										} 
									}
								}";

		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("PreferForEachOverForLoop").WithSpan(6, 11, 6, 14)
				.WithMessage(
					"When iterating through all element sequentially it has shown to be more efficient to use foreach rather than using for loops.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest14_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> list = new List<int>();
										int sum = 0;
										for(int i = 0; i < list.Count; i++){ 
											sum += list[i];
										} 
									}
								}";

		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("PreferForEachOverForLoop").WithSpan(7, 11, 7, 14)
				.WithMessage(
					"When iterating through all element sequentially it has shown to be more efficient to use foreach rather than using for loops.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest15_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for(int i = 0; i < list.Count; i++){
			for(int j = 0; j < list.Count; j++){
				sum += list[i] + list[j];
			}
		}
	}
}";

		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("PreferForEachOverForLoop").WithSpan(8, 3, 8, 6)
				.WithMessage(
					"When iterating through all element sequentially it has shown to be more efficient to use foreach rather than using for loops.");
		DiagnosticResult expected2 =
			DiagnosticResult.CompilerWarning("PreferForEachOverForLoop").WithSpan(9, 4, 9, 7)
				.WithMessage(
					"When iterating through all element sequentially it has shown to be more efficient to use foreach rather than using for loops.");
		await Verify.VerifyAnalyzerAsync(testCode, expected, expected2);
	}

	[Test]
	public async Task LoopTest16NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for(int i = 0; i < list.Count; i++){
			if(i < 0){}
			sum += list[i];
		}
	}
}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest17NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for(int i = 0; i < list.Count; i++){
			if(i == 0){}
			sum += list[i];
		}
	}
}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest18NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for(int i = 0; i < list.Count; i++){
			if(i > 0){}
			sum += list[i];
		}
	}
}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest19NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for(int i = 0; i < list.Count; i++){
			if(i >= 0){}
			sum += list[i];
		}
	}
}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest20NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for(int i = 0; i < list.Count; i++){
			if(i <= 0){}
			sum += list[i];
		}
	}
}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest21NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for(int i = 0; i < list.Count; i++){
			if(i != 0){}
			sum += list[i];
		}
	}
}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest22NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for(int i = 0; i < list.Count; i++){
			if(i != 0 && i > 1){}
			sum += list[i];
		}
	}
}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest23NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test {
		void Method() {
			int sym = 3;
			short tag;
			short[] row = { 1, 2, 3, 4, 5, 6, 7 };
			for (int probe = 0; probe < row.Length; probe++) {
				/* is this entry labeled with our Symbol or the default? */
				tag = row[probe++];
				if (tag == sym || tag == -1) {
					/* return the next entry */
					var t = row[probe];
				}
			}
		}
	}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest24NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test {
		void Method() {
			int sym = 3;
			short tag;
			short[] row = { 1, 2, 3, 4, 5, 6, 7 };
			for (int probe = 0; probe < row.Length; probe++) {
				/* is this entry labeled with our Symbol or the default? */
				tag = row[probe--];
				if (tag == sym || tag == -1) {
					/* return the next entry */
					var t = row[probe];
				}
			}
		}
	}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest25NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test {
		void Method() {
			int sym = 3;
			short tag;
			short[] row = { 1, 2, 3, 4, 5, 6, 7 };
			for (int probe = 0; probe < row.Length; probe++) {
				/* is this entry labeled with our Symbol or the default? */
				tag = row[--probe];
				if (tag == sym || tag == -1) {
					/* return the next entry */
					var t = row[probe];
				}
			}
		}
	}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest26NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test {
		void Method() {
			int sym = 3;
			short tag;
			short[] row = { 1, 2, 3, 4, 5, 6, 7 };
			for (int probe = 0; probe < row.Length; probe++) {
				/* is this entry labeled with our Symbol or the default? */
				tag = row[++probe];
				if (tag == sym || tag == -1) {
					/* return the next entry */
					var t = row[probe];
				}
			}
		}
	}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest27NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test {
		void Method() {
			int sym = 3;
			short tag;
			short[] row = { 1, 2, 3, 4, 5, 6, 7 };
			for (int probe = 0; probe < row.Length; probe++) {
				/* is this entry labeled with our Symbol or the default? */
				probe++;
				tag = row[probe];
				if (tag == sym || tag == -1) {
					/* return the next entry */
					var t = row[probe];
				}
			}
		}
	}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest28NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test {
		void Method() {
			int sym = 3;
			short tag;
			short[] row = { 1, 2, 3, 4, 5, 6, 7 };
			for (int probe = 0; probe < row.Length; probe++) {
				/* is this entry labeled with our Symbol or the default? */
				probe = probe + 1;
				tag = row[probe];
				if (tag == sym || tag == -1) {
					/* return the next entry */
					var t = row[probe];
				}
			}
		}
	}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest29NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test {
		void Method() {
			int sym = 3;
			short tag;
			short[] row = { 1, 2, 3, 4, 5, 6, 7 };
			for (int probe = 0; probe < row.Length; probe++) {
				/* is this entry labeled with our Symbol or the default? */
				probe = 1;
				tag = row[probe];
				if (tag == sym || tag == -1) {
					/* return the next entry */
					var t = row[probe];
				}
			}
		}
	}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest30NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test {
		void Method() {
			int sym = 3;
			short tag;
			short[] row = { 1, 2, 3, 4, 5, 6, 7 };
			for (int probe = 0; probe < row.Length; probe++) {
				/* is this entry labeled with our Symbol or the default? */
				probe -= 1;
				tag = row[probe];
				if (tag == sym || tag == -1) {
					/* return the next entry */
					var t = row[probe];
				}
			}
		}
	}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest31NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
class Test {
		void Method() {
			int sym = 3;
			short tag;
			short[] row = { 1, 2, 3, 4, 5, 6, 7 };
			for (int probe = 0; probe < row.Length; probe++) {
				/* is this entry labeled with our Symbol or the default? */
				probe += 1;
				tag = row[probe];
				if (tag == sym || tag == -1) {
					/* return the next entry */
					var t = row[probe];
				}
			}
		}
	}";

		await Verify.VerifyAnalyzerAsync(testCode);
	}


	[Test]
	public async Task LoopTest32NoDiagnostic() {
		const string testCode = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for (int i = 0; i < list.Count; i++) {
			var next = list[i] < list.Count - 1 ? list[i + 1] : -1;
			sum += list[i] + list.Count;
		}
	}
}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest33NoDiagnostic() {
		const string testCode = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for (int i = 0; i < list.Count; i++) {
			sum += list[i+1] + list.Count;
		}
	}
}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest34NoDiagnostic() {
		const string testCode = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for (int i = 0; i < list.Count; i++) {
			sum += list[i+1] + list.Count;
		}
	}
}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest35NoDiagnostic() {
		const string testCode = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		for (int i = 0; i < 10; i++) {
			sum += list[i+1] + list.Count;
		}
	}
}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task LoopTest36_Diagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										int[] array = new int[10];
										for(int i = 0; i < array.Length; i++){ 
											int a = array[i];
											int t = i + 1;
										} 
									}
								}";

		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("PreferForEachOverForLoop").WithSpan(5, 11, 5, 14)
				.WithMessage(
					"When iterating through all element sequentially it has shown to be more efficient to use foreach rather than using for loops.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest37_Diagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										int[] array = new int[10];
										for(int i = 0; i < array.Length; i++){ 
											int a = array[i];
											int t = 1 + i;
										} 
									}
								}";

		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("PreferForEachOverForLoop").WithSpan(5, 11, 5, 14)
				.WithMessage(
					"When iterating through all element sequentially it has shown to be more efficient to use foreach rather than using for loops.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}