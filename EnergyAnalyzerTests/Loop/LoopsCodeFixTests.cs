﻿using System.Collections.Generic;
using System.Threading.Tasks;
using NUnit.Framework;
using VerifyCS =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Loops.LoopsAnalyzer,
		EnergyAnalyzer.Loops.LoopsCodeFixProvider>;


namespace EnergyAnalyzerTests.Loop;

public class LoopsCodeFixTests {
	[Test]
	public async Task LoopTest01_Diagnostic() {
		const string testCode = @"
class Test
{
	void Method()
	{
		int[] array = new int[10];
		[|for|](int i = 0; i < array.Length; i++) {
			int a = array[i];
		}
	}
}";

		const string expected = @"
class Test
{
	void Method()
	{
		int[] array = new int[10];
		foreach(var item in array) {
			int a = item;
		}
	}
}";

		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest02_Diagnostic() {
		const string testCode = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		[|for|](int i = 0; i < list.Count; i++){
			int a = list[i];
		}
	}
}";

		const string expected = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		foreach(var item in list){
			int a = item;
		}
	}
}";

		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest03_Diagnostic() {
		const string testCode = @"
class Test
{
	void Method(){
		int sum = 0;
		int[] array = new int[10];
		[|for|](int i = 0; i < array.Length; i++){
			sum += array[i];
		}
	}
}";

		const string expected = @"
class Test
{
	void Method(){
		int sum = 0;
		int[] array = new int[10];
		foreach(var item in array){
			sum += item;
		}
	}
}";

		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest4_Diagnostic() {
		const string testCode = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		[|for|](int i = 0; i < list.Count; i++){
			sum += list[i];
		}
	}
}";
		const string expected = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		foreach(var item in list){
			sum += item;
		}
	}
}";

		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest5_Diagnostic() {
		const string testCode = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		foreach(var item in list){
			[|for|](int j = 0; j < list.Count; j++){
				sum += item + list[j];
			}
		}
	}
}";
		const string expected = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		foreach(var item in list){
			foreach(var item2 in list){
				sum += item + item2;
			}
		}
	}
}";

		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest6_Diagnostic() {
		const string testCode = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		foreach(var item in list){
			[|for|](int j = 0; j < list.Count; j++){
				sum += item + list[j];
			}
		}
		int item2 = 0;
	}
}";
		const string expected = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		foreach(var item in list){
			foreach(var item3 in list){
				sum += item + item3;
			}
		}
		int item2 = 0;
	}
}";

		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest7_Diagnostic() {
		const string testCode = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		foreach(var item3 in list){
			[|for|](int j = 0; j < list.Count; j++){
				sum += item3 + list[j];
			}
		}
		int item = 0;
	}
}";
		const string expected = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<int> list = new List<int>();
		int sum = 0;
		foreach(var item3 in list){
			foreach(var item4 in list){
				sum += item3 + item4;
			}
		}
		int item = 0;
	}
}";

		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}

	[Test]
	public async Task LoopTest8_Diagnostic() {
		const string testCode = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<List<int>> list = new List<List<int>>();
		int sum = 0;
		foreach (var item in list) {
			[|for|] (int j = 0; j < item.Count; j++) {
				sum += item[j] + item.Count;
			}
		}
	}
}";
		const string expected = @"
using System.Collections.Generic;
class Test
{
	void Method(){
		List<List<int>> list = new List<List<int>>();
		int sum = 0;
		foreach (var item in list) {
			foreach(var item2 in item) {
				sum += item2 + item.Count;
			}
		}
	}
}";

		await VerifyCS.VerifyCodeFixAsync(testCode, expected);
	}
}