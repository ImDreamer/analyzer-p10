﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Datatype;

using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Datatype.MakeIntLongNintAnalyzer>;

public class IntAnalyserTests {
	[Test]
	public async Task SignedTest01_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = -20; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest02_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										short a = -20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeIntLongNint").WithSpan(3, 11, 3, 16)
				.WithMessage("Using int, long, or nint is usually more efficient than other signed datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task SignedTest03_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										sbyte a = -20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeIntLongNint").WithSpan(3, 11, 3, 16)
				.WithMessage("Using int, long, or nint is usually more efficient than other signed datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task SignedTest04_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										long a = -20; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest05_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										nint a = -20; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest06_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 20; 

										a = -20;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest07_Diagnostic() {
		const string testCode = @"class Test { 
									sbyte a = -20; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeIntLongNint").WithSpan(2, 10, 2, 15)
				.WithMessage("Using int, long, or nint is usually more efficient than other signed datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task SignedTest08_Diagnostic() {
		const string testCode = @"class Test { 
									sbyte a {get; set;} = -20; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeIntLongNint").WithSpan(2, 10, 2, 15)
				.WithMessage("Using int, long, or nint is usually more efficient than other signed datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task SignedTest09_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = -20; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest10_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = -20L; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task InterfaceTest11_NoDiagnostic() {
		const string testCode = @"interface ITest { 
									public void Method();
								}
								class Test2 : ITest {
									public void Method(){ 
										var a = -20L; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task AbstractTest12_NoDiagnostic() {
		const string testCode = @"abstract class Test { 
									protected abstract void Method();
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}
}