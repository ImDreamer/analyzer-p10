﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Datatype;

using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Datatype.MakeULongAnalyzer>;

public class ULongAnalyserTests {
	[Test]
	public async Task ULongTest01_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										ulong a = 4294967296; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ULongTest02_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										long a = 4294967296; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeUlong").WithSpan(3, 11, 3, 15)
				.WithMessage("Using ulong is usually more efficient than other datatypes when above the 32-bit limit.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ULongTest03_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										long a = 4294967296; 
										a = -1;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ULongTest04_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										long a = -1;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ULongTest05_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										ulong a = 4294967296; 
										a = 2;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ULongTest06_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										long a = 4294967296; 
										a = 2;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeUlong").WithSpan(3, 11, 3, 31)
				.WithMessage("Using ulong is usually more efficient than other datatypes when above the 32-bit limit.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ULongTest07_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										uint a = 5; 
										a = 2;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ULongTest08_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 5; 
										a = 2;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ULongTest09_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 5; 
										a = -2;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ULongTest10_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										long a = 4294967296; 
										a = -2;
										a = 3;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ULongTest11_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										long a = 4294967296; 
										a = 2;
										a = 3;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeUlong").WithSpan(3, 11, 3, 31)
				.WithMessage("Using ulong is usually more efficient than other datatypes when above the 32-bit limit.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ULongTest12_NoDiagnostic() {
		const string testCode = @"";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test, Order(-1)]
	public async Task ULongTest13_Diagnostic() {
		const string testCode = @"class Test { 
									long a = 4294967296; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeUlong").WithSpan(2, 10, 2, 14)
				.WithMessage("Using ulong is usually more efficient than other datatypes when above the 32-bit limit.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test, Order(-1)]
	public async Task ULongTest14_Diagnostic() {
		const string testCode = @"class Test { 
									long a {get; set;} = 4294967296;									
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeUlong").WithSpan(2, 10, 2, 14)
				.WithMessage("Using ulong is usually more efficient than other datatypes when above the 32-bit limit.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}