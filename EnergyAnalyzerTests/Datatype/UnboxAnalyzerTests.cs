﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Datatype;

using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Datatype.UnboxAnalyzer>;

public class UnboxAnalyzerTests {
	[Test]
	public async Task UnBoxingTest01_NoDiagnostic() {
		const string testCode = @"using System;
								class Test { 
									void Method(){ 
										uint a = 20; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task UnBoxingTest02_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									void Method(){ 
										UInt32 a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(4, 11, 4, 17)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest03_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									void Method(){ 
										Decimal a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(4, 11, 4, 18)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest04_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									void Method(){ 
										Single a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(4, 11, 4, 17)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest05_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									void Method(){ 
										Boolean a = false; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(4, 11, 4, 18)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest06_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									Boolean a = false; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(3, 10, 3, 17)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest07_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									Boolean a {get; set;} = false; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(3, 10, 3, 17)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest08_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									Boolean a; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(3, 10, 3, 17)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest09_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									Boolean a {get; set;} 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(3, 10, 3, 17)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest10_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									System.Boolean a {get; set;} 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(3, 10, 3, 24)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest11_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									void Method(){ 
										String a = """"; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(4, 11, 4, 17)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest12_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									String a = """"; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(3, 10, 3, 16)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest13_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									String a {get; set;} = """"; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(3, 10, 3, 16)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest14_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									void Method(){ 
										Object a = """"; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(4, 11, 4, 17)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest15_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									Object a = """"; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(3, 10, 3, 16)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task UnBoxingTest16_Diagnostic() {
		const string testCode = @"using System;
								class Test { 
									Object a {get; set;} = """"; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Unbox").WithSpan(3, 10, 3, 16)
				.WithMessage("Using unboxed types is usually more efficient than boxed primitives.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}