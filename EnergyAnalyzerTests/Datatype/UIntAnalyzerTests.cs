﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Datatype;

using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Datatype.MakeUIntAnalyzer>;

public class UIntAnalyzerTests {
	[Test]
	public async Task UnsignedTest01_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										uint a = 20; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task UnsignedTest02_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 14)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest03_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										long a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 15)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest04_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										nint a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 15)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest05_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										short a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 16)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest06_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										byte a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 15)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest07_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										sbyte a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 16)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest08_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										ulong a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 16)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest09_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										nuint a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 16)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest10_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										ushort a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 17)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest11_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 20u; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task UnsignedTest12_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 20; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 14)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest13_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 20l; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 14)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest14_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 20ul; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 14)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest15_Diagnostic() {
		const string testCode = @"class Test { 
									int a = 20; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(2, 10, 2, 13)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest16_Diagnostic() {
		const string testCode = @"class Test { 
									int a {get; set;} = 20; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(2, 10, 2, 13)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest17_Diagnostic() {
		const string testCode = @"class Test { 
									int a; 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(2, 10, 2, 13)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest18_Diagnostic() {
		const string testCode = @"class Test { 
									int a {get; set;}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(2, 10, 2, 13)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}


	[Test]
	public async Task SignedTest01_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = -20; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest02_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										long a = -20; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest03_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										nint a = -20; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest04_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 20; 

										a = -20;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest05_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 20; 

										a = 25;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 22)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task SignedTest06_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 20; 

										a = 25;

										a = 45;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 22)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task SignedTest07_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 20; 

										a = -25;

										a = 45;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest08_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 20; 

										a = 25;

										a = -45;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest09_NoDiagnostic() {
		const string testCode = @"class Test { 
									int a = -20; 
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest10_NoDiagnostic() {
		const string testCode = @"class Test { 
									int a {get; set;} = -20; 
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest11_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = -20; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest12_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = -20L; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest13_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 20; 

										a = -20;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest14_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 20; 

										a = 25;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 22)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task SignedTest15_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 20; 

										a = 25;

										a = 45;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(3, 11, 3, 22)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task SignedTest16_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 20; 

										a = -25;

										a = 45;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest17_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 20; 

										a = 25;

										a = -45;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest18_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 20; 

										a = 25;

										a = -45;
									}

									void Method2(){ 
										int a = 20; 

										a = 25;

										a = -45;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest19_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method2(){ 
										int a = -1;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task SignedTest20_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method2(){ 
										var a = -1;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}


	[Test]
	public async Task SignedTest21_OverrideDiagnostic() {
		const string testCode = @"class Test {
		public virtual int Line {
			get { return 10; }
		}
	}


	class Test2 : Test {
		public override int Line {
			get { return 11; }
		}
	}";

		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(2, 18, 2, 21)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test(Description = "Ensures that the analyzer doesn't suggest changes when the value is not a literal")]
	public async Task UnsignedTest22_NoDiagnostic() {
		const string testCode = @"
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main()
    {
		string ruleString = ""asd:fer"";
		int i = ruleString.IndexOf("":"");
    }
}
";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task UnsignedTest23_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method2(){ 
										string a;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task UnsignedTest24_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method2(){ 
										string a = null;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task UnsignedTest25_NoDiagnostic() {
		const string testCode = @"class Test { 
									char b;
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task UnsignedTest26_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method2(){ 
										char b;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task UnsignedTest27_NoDiagnostic() {
		const string testCode = @"class Test { 
									bool need_prop;
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task UnsignedTest28_Diagnostic() {
		const string testCode = @"class Test { 
									public virtual void compute_closure() { 
										Test2 need_prop = null;
									}
								}

								public class Test2 {
									public int Hello;
									public double Fellow;
									public float Test;
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("Makeuint").WithSpan(8, 17, 8, 20)
				.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task UnsignedTest29_NoDiagnostic() {
		const string testCode = @"class Test { 
									public virtual void compute_closure() { 
										bool need_prop;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task UnsignedTest30_Diagnostic() {
		const string testCode = @"class Test { 
									Symbol lhs_sym = null;
								}

								public class Symbol	{
									public Symbol(int id, int l, int r, object o) : this(id) {
										left = l;
										right = r;
										Value = o;
									}
									
									public Symbol(int id, object o):this(id, - 1, - 1, o){
									}
									
									
									public Symbol(int id, int l, int r):this(id, l, r, null){
									}
									
									public Symbol(int sym_num):this(sym_num, - 1){
										left = - 1;
										right = - 1;
										Value = null;
									}
									
									internal Symbol(int sym_num, int state){
										sym = sym_num;
										parse_state = state;
									}
									public int sym;
									public int parse_state;
									internal bool used_by_parser = false;
									public int left, right;
									public object Value;
									
									public override string ToString()
									{
										return ""#"" + sym;
									}
								}
";

		DiagnosticResult ex = DiagnosticResult.CompilerWarning("Makeuint").WithSpan(29, 17, 29, 20)
			.WithMessage("Using uint is usually more efficient than other datatypes.");
		DiagnosticResult ex1 = DiagnosticResult.CompilerWarning("Makeuint").WithSpan(30, 17, 30, 20)
			.WithMessage("Using uint is usually more efficient than other datatypes.");
		DiagnosticResult ex2 = DiagnosticResult.CompilerWarning("Makeuint").WithSpan(32, 17, 32, 20)
			.WithMessage("Using uint is usually more efficient than other datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, ex, ex1, ex2);
	}
	
	[Test]
	public async Task UnsignedTest31_Diagnostic() {
		const string testCode = @"class Test { 
									public void test() { 
										var a = test2();
									}

									public int test2() {
										return 2;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task UnsignedTest32_Diagnostic() {
		const string testCode = @"class Test { 
									public void test() { 
										int koEndingPosition = -1;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task UnsignedTest33_Diagnostic() {
		const string testCode = @"class Test { 
									int koEndingPosition = -1;
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
}