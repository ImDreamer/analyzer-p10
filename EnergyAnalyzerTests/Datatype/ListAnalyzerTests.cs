﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Datatype;

using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Datatype.MakeIntListAnalyzer>;

public class ListAnalyzerTests {
	[Test]
	public async Task ListTest01_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> list = new List<int>(); 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ListTest02_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<uint> list = new List<uint>(); 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeIntList").WithSpan(5, 11, 5, 46)
				.WithMessage("Using int in a list is usually more efficient than uint.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ListTest03_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>(); 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeIntList").WithSpan(6, 11, 6, 50)
				.WithMessage("Using int in a list is usually more efficient than uint.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ListTest04_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Collections.Generic;
								class Test { 
									List<UInt32> list = new List<UInt32>(); 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeIntList").WithSpan(5, 10, 5, 49)
				.WithMessage("Using int in a list is usually more efficient than uint.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ListTest05_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Collections.Generic;
								class Test { 
									List<UInt32> list {get; set;} = new List<UInt32>(); 
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeIntList").WithSpan(5, 10, 5, 61)
				.WithMessage("Using int in a list is usually more efficient than uint.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ListTest06_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Collections.Generic;
								class Test { 
									List<UInt32> list {get; set;}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeIntList").WithSpan(5, 10, 5, 39)
				.WithMessage("Using int in a list is usually more efficient than uint.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ListTest07_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Collections.Generic;
								class Test { 
									List<UInt32> list;
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeIntList").WithSpan(5, 10, 5, 28)
				.WithMessage("Using int in a list is usually more efficient than uint.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}