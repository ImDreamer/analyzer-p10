﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Datatype;

using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Datatype.MakeDoubleAnalyzer>;

public class DoubleAnalyzerTests {
	[Test]
	public async Task DecimalTest01_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										double a = 1.1; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task DecimalTest02_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										float a = 1.1f; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDouble").WithSpan(3, 11, 3, 16)
				.WithMessage("Using double is usually more efficient than other decimal datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DecimalTest03_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										decimal a = 1.1m; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDouble").WithSpan(3, 11, 3, 18)
				.WithMessage("Using double is usually more efficient than other decimal datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task DecimalTest04_Diagnostic() {
		const string testCode = @"class Test { 
									decimal a = 1.1m;
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDouble").WithSpan(2, 10, 2, 17)
				.WithMessage("Using double is usually more efficient than other decimal datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task DecimalTest05_Diagnostic() {
		const string testCode = @"class Test { 
									decimal a {get; set;} = 1.1m;
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDouble").WithSpan(2, 10, 2, 17)
				.WithMessage("Using double is usually more efficient than other decimal datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task DecimalTest06_Diagnostic() {
		const string testCode = @"class Test { 
									decimal a {get; set;}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDouble").WithSpan(2, 10, 2, 17)
				.WithMessage("Using double is usually more efficient than other decimal datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task DecimalTest07_Diagnostic() {
		const string testCode = @"class Test { 
									decimal a;
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDouble").WithSpan(2, 10, 2, 17)
				.WithMessage("Using double is usually more efficient than other decimal datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task DecimalTest08_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 1.1; 
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task DecimalTest09_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 1.1f; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDouble").WithSpan(3, 11, 3, 14)
				.WithMessage("Using double is usually more efficient than other decimal datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DecimalTest10_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										var a = 1.1m; 
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDouble").WithSpan(3, 11, 3, 14)
				.WithMessage("Using double is usually more efficient than other decimal datatypes.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}