﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Datatype;

using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Datatype.StringBuilderAnalyzer>;

public class StringBuilderAnalyzerTests {
	[Test]
	public async Task StringBuilderTest01_NoDiagnostic() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder a = new StringBuilder(""hi"");
										a.Append(""hello"");
										a.Append(""heya"");
										string b = a.ToString();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task StringBuilderTest02_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										uint num = 5;
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										string a = $""{num}{secondaryString}{thirdString}"";
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(6, 11, 6, 61)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringBuilderTest03_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										string a = firstString + secondaryString + thirdString;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(6, 11, 6, 66)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringBuilderTest04_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string a = $""{firstString}{secondaryString}"";
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringBuilderTest05_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 5;
										int b = 7;
										int c = 9;
										int d = a + b + c;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringBuilderTest06_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string a = firstString + secondaryString;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringBuilderTest07_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										string a = firstString;
										a += secondaryString;
										a += thirdString;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(6, 11, 6, 34)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringBuilderTest08_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string a = firstString;
										a += secondaryString;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringBuilderTest09_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										string a = firstString;
										a += secondaryString + thirdString;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(6, 11, 6, 34)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringBuilderTest10_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										string a = string.Format(""{0}{1}{2}"", firstString, secondaryString, thirdString);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(6, 11, 6, 92)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest11_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string a = string.Format(""{0}{1}"", firstString, secondaryString);
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest12_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										string a = string.Concat(firstString, secondaryString, thirdString);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(6, 11, 6, 79)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest13_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string a = string.Concat(firstString, secondaryString);
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest14_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										string a = string.Join(firstString, secondaryString, thirdString);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(6, 11, 6, 77)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest15_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string a = string.Join(firstString, secondaryString);
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest16_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""three"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"" + string.Format(""{0}{1}"", firstString, secondaryString);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(5, 11, 5, 96)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest17_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = $""{firstString} {secondaryString}"";
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(5, 11, 5, 67)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest18_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = $""{firstString} "";
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest19_NoDiagnostic() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder firstString = new StringBuilder(""hi"").Append(""with"");
										string secondaryString = firstString.ToString();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest20_NoDiagnostic() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder firstString = new StringBuilder().Append(""hi"").Append(""with"");
										string secondaryString = firstString.ToString();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringBuilderTest01_NoDiagnosticNoDeclaration() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder a = new StringBuilder(""hi"");
										a.Append(""hello"");
										a.Append(""heya"");
										string b = a.ToString();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task StringBuilderTest02_DiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										uint num = 5;
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										secondaryString = $""{num}{secondaryString}{thirdString}"";
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(4, 11, 4, 43)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringBuilderTest03_DiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										secondaryString = firstString + secondaryString + thirdString;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(4, 11, 4, 43)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringBuilderTest04_NoDiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										firstString = $""{firstString}{secondaryString}"";
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringBuilderTest05_NoDiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										int a = 5;
										int b = 7;
										int c = 9;
										int d = a + b + c;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringBuilderTest06_NoDiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										firstString = firstString + secondaryString;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringBuilderTest07_DiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										firstString = firstString;
										firstString += secondaryString;
										firstString += thirdString;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(3, 11, 3, 38)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringBuilderTest08_NoDiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										firstString = firstString;
										firstString += secondaryString;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringBuilderTest09_DiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										firstString = firstString;
										firstString += secondaryString + thirdString;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(3, 11, 3, 38)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringBuilderTest10_DiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										firstString = string.Format(""{0}{1}{2}"", firstString, secondaryString, thirdString);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(3, 11, 3, 38)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest11_NoDiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										firstString = string.Format(""{0}{1}"", firstString, secondaryString);
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest12_DiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										firstString = string.Concat(firstString, secondaryString, thirdString);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(3, 11, 3, 38)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest13_NoDiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										firstString = string.Concat(firstString, secondaryString);
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest14_DiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										firstString = string.Join(firstString, secondaryString, thirdString);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(3, 11, 3, 38)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest15_NoDiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										firstString = string.Join(firstString, secondaryString);
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringBuilderTest16_DiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"" + ""hi"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										firstString += ""three"";
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(3, 11, 3, 45)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringBuilderTest17_DiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string thirdString = ""Bonus"";
										firstString = ""three"" + string.Format(""{0}{1}"", firstString, secondaryString);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(3, 11, 3, 38)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest18_DiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										firstString = $""{firstString} {secondaryString}"";
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(3, 11, 3, 38)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest19_NoDiagnosticNoDeclaration() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										firstString = $""{firstString} "";
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest20_NoDiagnosticNoDeclaration() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder firstString = new StringBuilder();
										firstString.Append(""hi"").Append(""With"");
										string secondaryString = firstString.ToString();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest21_NoDiagnosticNoDeclaration() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder firstString = new StringBuilder();
										firstString.Append(""hi"").Append(""With"").Append(""hi2"");
										string secondaryString = firstString.ToString();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest22_NoDiagnosticNoDeclaration() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder firstString = new StringBuilder().Append(""hi"");
										firstString.Append(""hi"");
										string secondaryString = firstString.ToString();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringBuilderTest23_DiagnosticNoDeclaration() {
		const string testCode = @"
    							using System.Text;
    							class Test { 
    								void Method(){ 
    									string b;
										b = """";
										b += ""a"";
										b += ""aa"";
    									string a = b;
    								}
    							}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(5, 14, 5, 23)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringBuilderTest24_DiagnosticNoDeclaration() {
		const string testCode = @"
    							using System.Text;
    							class Test { 
    								void Method(){ 
    									string b;
										b = string.Format(""{0}"", ""a"");
										b += string.Format(""{0}{1}"", ""b"", ""c"");
    									string a = b;
    								}
    							}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringBuilder").WithSpan(5, 14, 5, 23)
				.WithMessage(
					"Using StringBuilder is usually more efficient than other string concatenation methods when concatenating 3 or more strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}