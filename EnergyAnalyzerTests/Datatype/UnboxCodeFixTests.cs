﻿using System.Threading.Tasks;
using NUnit.Framework;
using VerifyCS =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Datatype.UnboxAnalyzer,
		EnergyAnalyzer.Datatype.UnboxCodeFixProvider>;

namespace EnergyAnalyzerTests.Datatype;

public class UnboxCodeFixTests {
	[Test]
	public async Task UnboxTest01_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|Single|] i = 1.1f;
}
", @"
using System;

class Program
{
    float i = 1.1f;
}
");
	}

	[Test]
	public async Task UnboxTest02_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|Single|] i { get; set; } = 1.1f;
}
", @"
using System;

class Program
{
    float i { get; set; } = 1.1f;
}
");
	}

	[Test]
	public async Task UnboxTest03_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|Single|] i = 1.1f;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        float i = 1.1f;
    }
}
");
	}

	[Test]
	public async Task UnboxTest04_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|Decimal|] i = 1.1m;
}
", @"
using System;

class Program
{
    decimal i = 1.1m;
}
");
	}

	[Test]
	public async Task UnboxTest05_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|Decimal|] i { get; set; } = 1.1m;
}
", @"
using System;

class Program
{
    decimal i { get; set; } = 1.1m;
}
");
	}

	[Test]
	public async Task UnboxTest06_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|Decimal|] i = 1.1m;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        decimal i = 1.1m;
    }
}
");
	}

	[Test]
	public async Task UnboxTest07_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|Single|] i;
}
", @"
using System;

class Program
{
    float i;
}
");
	}

	[Test]
	public async Task UnboxTest08_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|Single|] i { get; set; }
}
", @"
using System;

class Program
{
    float i { get; set; }
}
");
	}

	[Test]
	public async Task UnboxTest09_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|Single|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        float i;
    }
}
");
	}

	[Test]
	public async Task UnboxTest10_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|Decimal|] i;
}
", @"
using System;

class Program
{
    decimal i;
}
");
	}

	[Test]
	public async Task UnboxTest11_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|Decimal|] i { get; set; }
}
", @"
using System;

class Program
{
    decimal i { get; set; }
}
");
	}

	[Test]
	public async Task UnboxTest12_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|Decimal|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        decimal i;
    }
}
");
	}
	
	[Test]
	public async Task UnboxTest13_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|System.Decimal|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        decimal i;
    }
}
");
	}
	
	[Test]
	public async Task UnboxTest14_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|String|] i;
}
", @"
using System;

class Program
{
    string i;
}
");
	}

	[Test]
	public async Task UnboxTest15_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|String|] i { get; set; }
}
", @"
using System;

class Program
{
    string i { get; set; }
}
");
	}

	[Test]
	public async Task UnboxTest16_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|String|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        string i;
    }
}
");
	}
	
	[Test]
	public async Task UnboxTest17_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|System.String|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        string i;
    }
}
");
	}
	
	[Test]
	public async Task UnboxTest18_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|Object|] i;
}
", @"
using System;

class Program
{
    object i;
}
");
	}

	[Test]
	public async Task UnboxTest19_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|Object|] i { get; set; }
}
", @"
using System;

class Program
{
    object i { get; set; }
}
");
	}

	[Test]
	public async Task UnboxTest20_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|Object|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        object i;
    }
}
");
	}
	
	[Test]
	public async Task UnboxTest21_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|System.Object|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        object i;
    }
}
");
	}
}