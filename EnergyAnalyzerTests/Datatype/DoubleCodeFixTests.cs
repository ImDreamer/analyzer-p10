﻿using System.Threading.Tasks;
using EnergyAnalyzer.Datatype;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Datatype;

using VerifyCS =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<MakeDoubleAnalyzer, MakeDoubleCodeFixProvider>;

public class DoubleCodeFixTests {
	[Test]
	public async Task DecimalTest01_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|float|] i = 1.1f;
}
", @"
using System;

class Program
{
    double i = 1.1;
}
");
	}

	[Test]
	public async Task DecimalTest02_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|float|] i { get; set; } = 1.1f;
}
", @"
using System;

class Program
{
    double i { get; set; } = 1.1;
}
");
	}

	[Test]
	public async Task DecimalTest03_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|float|] i = 1.1f;
        Console.WriteLine(i);
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        double i = 1.1;
        Console.WriteLine(i);
    }
}
");
	}

	[Test]
	public async Task DecimalTest04_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|decimal|] i = 1.1m;
}
", @"
using System;

class Program
{
    double i = 1.1;
}
");
	}

	[Test]
	public async Task DecimalTest05_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|decimal|] i { get; set; } = 1.1m;
}
", @"
using System;

class Program
{
    double i { get; set; } = 1.1;
}
");
	}

	[Test]
	public async Task DecimalTest06_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|decimal|] i = 1.1m;
        Console.WriteLine(i);
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        double i = 1.1;
        Console.WriteLine(i);
    }
}
");
	}

	[Test]
	public async Task DecimalTest07_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|float|] i;
}
", @"
using System;

class Program
{
    double i;
}
");
	}

	[Test]
	public async Task DecimalTest08_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|float|] i { get; set; }
}
", @"
using System;

class Program
{
    double i { get; set; }
}
");
	}

	[Test]
	public async Task DecimalTest09_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|float|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        double i;
    }
}
");
	}

	[Test]
	public async Task DecimalTest10_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|decimal|] i;
}
", @"
using System;

class Program
{
    double i;
}
");
	}

	[Test]
	public async Task DecimalTest11_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|decimal|] i { get; set; }
}
", @"
using System;

class Program
{
    double i { get; set; }
}
");
	}

	[Test]
	public async Task DecimalTest12_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|decimal|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        double i;
    }
}
");
	}

	[Test]
	public async Task DecimalTest13_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|var|] i = 1.1f;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        double i = 1.1;
    }
}
");
	}

	[Test]
	public async Task DecimalTest14_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|var|] i = 1.1m;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        double i = 1.1;
    }
}
");
	}
}