﻿using System.Threading.Tasks;
using EnergyAnalyzer.Datatype;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Datatype;

using VerifyCS =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<MakeUIntAnalyzer, MakeUIntCodeFixProvider>;

public class UIntCodeFixTests {
	[Test]
	public async Task UnsignedTest01_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|int|] i = 1;
}
", @"
using System;

class Program
{
    uint i = 1;
}
");
	}

	[Test]
	public async Task UnsignedTest02_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|int|] i { get; set; } = 1;
}
", @"
using System;

class Program
{
    uint i { get; set; } = 1;
}
");
	}

	[Test]
	public async Task UnsignedTest03_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|int|] i = 1;
        Console.WriteLine(i);
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        uint i = 1;
        Console.WriteLine(i);
    }
}
");
	}

	[Test]
	public async Task UnsignedTest04_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|int|] i;
}
", @"
using System;

class Program
{
    uint i;
}
");
	}

	[Test]
	public async Task UnsignedTest05_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|int|] i { get; set; }
}
", @"
using System;

class Program
{
    uint i { get; set; }
}
");
	}

	[Test]
	public async Task UnsignedTest06_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|int|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        uint i;
    }
}
");
	}

	[Test]
	public async Task UnsignedTest07_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|var|] i = 1;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        uint i = 1;
    }
}
");
	}

	[Test]
	public async Task UnsignedTest08_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|long|] i = 1;
}
", @"
using System;

class Program
{
    uint i = 1;
}
");
	}

	[Test]
	public async Task UnsignedTest09_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|long|] i { get; set; } = 1;
}
", @"
using System;

class Program
{
    uint i { get; set; } = 1;
}
");
	}

	[Test]
	public async Task UnsignedTest10_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|long|] i = 1;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        uint i = 1;
    }
}
");
	}

	[Test]
	public async Task UnsignedTest11_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|long|] i;
}
", @"
using System;

class Program
{
    uint i;
}
");
	}

	[Test]
	public async Task UnsignedTest12_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|long|] i { get; set; }
}
", @"
using System;

class Program
{
    uint i { get; set; }
}
");
	}

	[Test]
	public async Task UnsignedTest13_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|long|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        uint i;
    }
}
");
	}

	[Test]
	public async Task UnsignedTest14_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|var|] i = 1L;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        uint i = 1;
    }
}
");
	}
	
	[Test]
	public async Task UnsignedTest15_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|ulong|] i = 1;
}
", @"
using System;

class Program
{
    uint i = 1;
}
");
	}

	[Test]
	public async Task UnsignedTest16_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|ulong|] i { get; set; } = 1;
}
", @"
using System;

class Program
{
    uint i { get; set; } = 1;
}
");
	}

	[Test]
	public async Task UnsignedTest17_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|ulong|] i = 1;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        uint i = 1;
    }
}
");
	}

	[Test]
	public async Task UnsignedTest18_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|ulong|] i;
}
", @"
using System;

class Program
{
    uint i;
}
");
	}

	[Test]
	public async Task UnsignedTest19_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    [|ulong|] i { get; set; }
}
", @"
using System;

class Program
{
    uint i { get; set; }
}
");
	}

	[Test]
	public async Task UnsignedTest20_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|ulong|] i;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        uint i;
    }
}
");
	}

	[Test]
	public async Task UnsignedTest21_Fix() {
		await VerifyCS.VerifyCodeFixAsync(@"
using System;

class Program
{
    static void Main()
    {
        [|var|] i = 1UL;
    }
}
", @"
using System;

class Program
{
    static void Main()
    {
        uint i = 1;
    }
}
");
	}
}