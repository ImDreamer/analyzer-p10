﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;
using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Datatype.StringInterpolationAnalyzer>;

namespace EnergyAnalyzerTests.Datatype;

public class StringInterpolationAnalyzerTests {
	[Test]
	public async Task StringInterpolationTest01_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										uint num = 5;
										string secondaryString = ""Five"";
										string a = $""{num}{secondaryString}"";
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task StringInterpolationTest02_Diagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										string firstString = ""one"";
										string secondaryString = ""Five"";
										string a = firstString + secondaryString;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringInterpolation").WithSpan(5, 11, 5, 52)
				.WithMessage(
					"Using String Interpolation is usually more efficient than other string concatenation methods when concatenating 2 strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringInterpolationTest03_Diagnostic() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder firstString = new StringBuilder(""One"");
										firstString.Append(""Five"");
										string a = firstString.ToString();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringInterpolation").WithSpan(5, 11, 5, 64)
				.WithMessage(
					"Using String Interpolation is usually more efficient than other string concatenation methods when concatenating 2 strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringInterpolationTest04_Diagnostic() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder firstString = new StringBuilder();
										firstString.Append(""Five"");
										firstString.Append(""one"");
										string a = firstString.ToString();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringInterpolation").WithSpan(5, 11, 5, 59)
				.WithMessage(
					"Using String Interpolation is usually more efficient than other string concatenation methods when concatenating 2 strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringInterpolationTest05_NoDiagnostic() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder firstString = new StringBuilder();
										firstString.Append(""Five"");
										firstString.Append(""one"");
										firstString.Append(""woo"");
										string a = firstString.ToString();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringInterpolationTest06_NoDiagnostic() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder firstString = new StringBuilder(""woo"");
										firstString.Append(""Five"");
										firstString.Append(""one"");
										string a = firstString.ToString();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringInterpolationTest07_NoDiagnostic() {
		const string testCode = @"
								using System.Text;
								class Test { 
									void Method(){ 
										StringBuilder firstString = new StringBuilder(""woo"").Append(""Five"");
										firstString.Append(""one"");
										string a = firstString.ToString();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringInterpolationTest08_Diagnostic() {
		const string testCode = @"
    							using System.Text;
    							class Test { 
    								void Method(){ 
    									StringBuilder firstString = new StringBuilder(""woo"").Append(""Five"");
    									string a = firstString.ToString();
    								}
    							}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringInterpolation").WithSpan(5, 14, 5, 82)
				.WithMessage(
					"Using String Interpolation is usually more efficient than other string concatenation methods when concatenating 2 strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringInterpolationTest09_Diagnostic() {
		const string testCode = @"
        						using System.Text;
        						class Test { 
        							void Method(){ 
        								StringBuilder firstString = new StringBuilder();
										firstString.Append(""woo"").Append(""Five"");
        								string a = firstString.ToString();
        							}
        						}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringInterpolation").WithSpan(5, 17, 5, 65)
				.WithMessage(
					"Using String Interpolation is usually more efficient than other string concatenation methods when concatenating 2 strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task StringInterpolationTest10_NoDiagnostic() {
		const string testCode = @"
        						using System.Text;
        						class Test { 
        							void Method(){ 
        								StringBuilder firstString = new StringBuilder();
										firstString.Append(""woo"").Append(""Five"").Append(""three"");
        								string a = firstString.ToString();
        							}
        						}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task StringInterpolationTest11_NoDiagnostic() {
		const string testCode = @"
    							using System.Text;
    							class Test { 
    								void Method(){ 
    									StringBuilder firstString = new StringBuilder(""woo"").Append(""Five"").Append(""three"");
    									string a = firstString.ToString();
    								}
    							}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringInterpolationTest12_Diagnostic() {
		const string testCode = @"
    							using System.Text;
    							class Test { 
    								void Method(){ 
    									string b;
										b = """";
										b += ""a"";
    									string a = b;
    								}
    							}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeStringInterpolation").WithSpan(5, 14, 5, 23)
				.WithMessage(
					"Using String Interpolation is usually more efficient than other string concatenation methods when concatenating 2 strings.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task StringInterpolationTest13_NoDiagnostic() {
		const string testCode = @"
    							using System.Text;
    							class Test { 
    								void Method(){ 
    									string b;
										b = """";
										b += ""c"";
										b += ""a"";
    									string a = b;
    								}
    							}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task StringInterpolationTest14_NoDiagnostic() {
		const string testCode = @"
    							using System.Text;
    							class Test { 
    								void Method(){ 
    									string b;
										b = string.Format(""{0}"", ""a"");
										b += string.Format(""{0}{1}"", ""b"", ""c"");
    									string a = b;
    								}
    							}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
}