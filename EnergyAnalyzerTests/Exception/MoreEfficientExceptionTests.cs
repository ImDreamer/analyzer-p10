﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Exception; 

using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Exception.MoreEfficientExceptionAnalyzer>;

public class MoreEfficientExceptionTests {
	[Test]
	public async Task MoreEfficientExceptionTest01_Diagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method() {
										uint b = 0;
										try {
											uint a = 5 / b;
											b = 0;
										} catch (DivideByZeroException e) { 
											b = 2;
										}
									}
								}
								";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("ReplaceInefficientException").WithSpan(9, 19, 9, 44)
				.WithMessage("Using the base Exception class is more efficient than DivideByZeroException or ArgumentException.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task MoreEfficientExceptionTest02_Diagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method() {
										uint b = 0;
										try {
											uint a = 5 / b;
											b = 0;
										} catch (ArgumentException e) { 
											b = 2;
										}
									}
								}
								";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("ReplaceInefficientException").WithSpan(9, 19, 9, 40)
				.WithMessage("Using the base Exception class is more efficient than DivideByZeroException or ArgumentException.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task MoreEfficientExceptionTest03_NoDiagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method() {
										uint b = 0;
										try {
											uint a = 5 / b;
											b = 0;
										} catch (Exception e) { 
											b = 2;
										}
									}
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task MoreEfficientExceptionTest04_NoDiagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method() {
										uint a = 5;
									}
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
}