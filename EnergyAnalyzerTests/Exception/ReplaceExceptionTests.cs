﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Exception; 

using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Exception.ReplaceExceptionAnalyzer>;

public class ReplaceExceptionTests {
	[Test]
	public async Task ReplaceExceptionTest01_Diagnostic() {
		const string testCode = @"
								using System;
								class Test { 
									void Method() {
										try {
											uint a = 5;
										} catch(Exception e) { }
									}
								}
								";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("ReplaceException").WithSpan(5, 11, 5, 14)
				.WithMessage("Removing try-catch blocks or using if statements to check for exceptions is more efficient if possible.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ReplaceExceptionTest02_NoDiagnostic() {
		const string testCode = @"
								class Test { 
									void Method() {
										uint a = 5;
									}
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
}