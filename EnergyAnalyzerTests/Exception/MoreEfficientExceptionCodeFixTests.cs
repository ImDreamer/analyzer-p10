﻿using System.Threading.Tasks;
using Microsoft;
using NUnit.Framework;
using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Exception.MoreEfficientExceptionAnalyzer,
		EnergyAnalyzer.Exception.MoreEfficientExceptionCodeFix>;

namespace EnergyAnalyzerTests.Exception; 

public class MoreEfficientExceptionCodeFixTests {
	[Test]
	public async Task MoreEfficientExceptionCodeFix_Diagnostic01() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method() {
		uint b = 0;
		try {
			uint a = 5 / b;
			b = 0;
		} catch [|(DivideByZeroException e)|] { 
			b = 2;
		}
	}
}
", @"
using System;
class Test { 
	void Method() {
		uint b = 0;
		try {
			uint a = 5 / b;
			b = 0;
		} catch (Exception e) { 
			b = 2;
		}
	}
}
");
	}
	
	[Test]
	public async Task MoreEfficientExceptionCodeFix_Diagnostic02() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method() {
		uint b = 0;
		try {
			uint a = 5 / b;
			b = 0;
		} catch [|(ArgumentException abc)|] { 
			b = 2;
		}
	}
}
", @"
using System;
class Test { 
	void Method() {
		uint b = 0;
		try {
			uint a = 5 / b;
			b = 0;
		} catch (Exception abc) { 
			b = 2;
		}
	}
}
");
	}
	
	[Test]
	public async Task MoreEfficientExceptionCodeFix_NoDiagnostic03() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method() {
		uint b = 0;
		try {
			uint a = 5 / b;
			b = 0;
		} catch (Exception abc) { 
			b = 2;
		}
	}
}
", @"
using System;
class Test { 
	void Method() {
		uint b = 0;
		try {
			uint a = 5 / b;
			b = 0;
		} catch (Exception abc) { 
			b = 2;
		}
	}
}
");
	}
	
	[Test]
	public async Task MoreEfficientExceptionCodeFix_NoDiagnostic04() {
		await Verify.VerifyCodeFixAsync(@"
using System;
class Test { 
	void Method() {
		int a = 5;
	}
}
", @"
using System;
class Test { 
	void Method() {
		int a = 5;
	}
}
");
	}
}