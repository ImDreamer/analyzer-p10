﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Object; 

using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Object.ObjectAnalyzer>;

public class ObjectTests {
	[Test]
	public async Task ObjectTest01_Diagnostic() {
		const string testCode = @"
								class Test { 
									void Method() {
										uint a = 5;
									}
								}
								";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("ReplaceClass").WithSpan(2, 9, 2, 14)
				.WithMessage("Using struct is more efficient than classes when the main purpose is not field access.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ObjectTest02_NoDiagnostic() {
		const string testCode = @"
								struct Test { 
									void Method() {
										uint a = 5;
									}
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task ObjectTest03_NoDiagnostic() {
		const string testCode = @"
								class Test {
									private uint b = 3;
									void Method() {
										uint a = b;
									}
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task ObjectTest04_NoDiagnostic() {
		const string testCode = @"
								class Test2 {
									protected uint b = 4;
								}
								class Test : Test2 { 
									void Method() {
										uint a = 5;
									}
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task ObjectTest05_NoDiagnostic() {
		const string testCode = @"
								interface Test2 {
									public void Method();
								}
								struct Test : Test2 { 
									public void Method() {
										uint a = 5;
									}
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test]
	public async Task ObjectTest06_Diagnostic() {
		const string testCode = @"
								interface Test2 {
									public void Method();
								}
								class Test : Test2 { 
									public void Method() {
										uint a = 5;
									}
								}
								";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("ReplaceClass").WithSpan(5, 9, 5, 14)
				.WithMessage("Using struct is more efficient than classes when the main purpose is not field access.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ObjectTest07_NoDiagnostic() {
		const string testCode = @"
								interface Test2 {
									public void Method();
								}
								class Test3 {
									protected int b = 4;
								}
								class Test : Test3, Test2 { 
									public void Method() {
										uint a = 5;
									}
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test, Description("Used to check that the analyzer doesn't suggest struct if the inherited class can't be found.")]
	public async Task ObjectTest08_NoDiagnostic() {
		const string testCode = @"
								public class a:b { 
								}
								";
		DiagnosticResult expected =
			DiagnosticResult.CompilerError("CS0246").WithSpan(2, 24, 2, 25)
				.WithMessage("The type or namespace name 'b' could not be found (are you missing a using directive or an assembly reference?)");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test, Description("Used to check that the analyzer doesn't suggest struct if it's an abstract class.")]
	public async Task ObjectTest09_NoDiagnostic() {
		const string testCode = @"
								public abstract class a { 
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
	
	[Test, Description("Used to check that the analyzer doesn't suggest struct if it's a static class.")]
	public async Task ObjectTest10_NoDiagnostic() {
		const string testCode = @"
								public static class a { 
								}
								";
		await Verify.VerifyAnalyzerAsync(testCode);
	}
}