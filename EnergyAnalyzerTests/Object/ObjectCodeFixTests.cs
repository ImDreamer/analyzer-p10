﻿using System.Threading.Tasks;
using NUnit.Framework;
using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Object.ObjectAnalyzer,
		EnergyAnalyzer.Object.ObjectCodeFix>;

namespace EnergyAnalyzerTests.Object; 

public class ObjectCodeFixTests {
	[Test]
	public async Task ObjectCodeFix_Diagnostic01() {
		await Verify.VerifyCodeFixAsync(@"
[|class|] Test {
	void Method(){
		int a = 5;
	}
}", @"
struct Test {
	void Method(){
		int a = 5;
	}
}");
	}
	
	[Test]
	public async Task ObjectCodeFix_NoDiagnostic02() {
		await Verify.VerifyCodeFixAsync(@"
struct Test {
	void Method(){
		int a = 5;
	}
}", @"
struct Test {
	void Method(){
		int a = 5;
	}
}");
	}
	
	[Test]
	public async Task ObjectCodeFix_NoDiagnostic03() {
		await Verify.VerifyCodeFixAsync(@"
class Test {
	int b = 5;
	void Method(){
		int a = 5;
	}
}", @"
class Test {
	int b = 5;
	void Method(){
		int a = 5;
	}
}");
	}
	
	[Test]
	public async Task ObjectCodeFix_Diagnostic04() {
		await Verify.VerifyCodeFixAsync(@"
[|class|] Test {
	void Method(){
		int a = 5;
	}

	int A(){
		int c = 5;
		return c;
	}
}", @"
struct Test {
	void Method(){
		int a = 5;
	}

	int A(){
		int c = 5;
		return c;
	}
}");
	}
}