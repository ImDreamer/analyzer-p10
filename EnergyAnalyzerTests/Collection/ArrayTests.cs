using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;
using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Collection.ArrayAnalyzer>;

namespace EnergyAnalyzerTests.Collection;

public class ArrayTests {
	[Test]
	public async Task ArrayTest01_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int[] a = new int[100];
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}
	
	[Test]
	public async Task ArrayTest02_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> a = new List<int>();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}
	
	[Test]
	public async Task ArrayTest03_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> a = new List<int>(100);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeArray").WithSpan(5, 11, 5, 44)
				.WithMessage("Using an array is usually more efficient than other types of lists.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ArrayTest04_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										int a = 5;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}
	
	[Test]
	public async Task ArrayTest05_Diagnostic() {
		const string testCode = @"
								using System.Collections;
								class Test { 
									void Method(){ 
										ArrayList a = new ArrayList(100);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeArray").WithSpan(5, 11, 5, 44)
				.WithMessage("Using an array is usually more efficient than other types of lists.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ArrayTest06_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										LinkedList<int> a = new LinkedList<int>();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task ArrayTest07_Diagnostic() {
		const string testCode = @"
								using System.Linq;
								using System.Collections.Immutable;
								class Test { 
									void Method(){ 
										ImmutableList<int> a = Enumerable.Range(0, 100).ToImmutableList();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeArray").WithSpan(6, 11, 6, 77)
				.WithMessage("Using an array is usually more efficient than other types of lists.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ArrayTest08_Diagnostic() {
		const string testCode = @"
								using System.Linq;
								using System.Collections.Immutable;
								class Test { 
									void Method(){ 
										ImmutableArray<int> a = Enumerable.Range(0, 100).ToImmutableArray();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeArray").WithSpan(6, 11, 6, 79)
				.WithMessage("Using an array is usually more efficient than other types of lists.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ArrayTest09_Diagnostic() {
		const string testCode = @"
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> a = Enumerable.Range(0, 100).ToList();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeArray").WithSpan(6, 11, 6, 59)
				.WithMessage("Using an array is usually more efficient than other types of lists.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ArrayTest10_Diagnostic() {
		const string testCode = @"
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										IEnumerable<int> a = Enumerable.Range(0, 100);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeArray").WithSpan(6, 11, 6, 57)
				.WithMessage("Using an array is usually more efficient than other types of lists.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ArrayTest11_Diagnostic() {
		const string testCode = @"
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									IEnumerable<int> a = Enumerable.Range(0, 100);
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeArray").WithSpan(5, 10, 5, 56)
				.WithMessage("Using an array is usually more efficient than other types of lists.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ArrayTest12_Diagnostic() {
		const string testCode = @"
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									IEnumerable<int> a {get; set;} = Enumerable.Range(0, 100);
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeArray").WithSpan(5, 10, 5, 68)
				.WithMessage("Using an array is usually more efficient than other types of lists.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}