﻿using System.Threading.Tasks;
using Microsoft;
using NUnit.Framework;
using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Collection.ArrayAnalyzer,
		EnergyAnalyzer.Collection.ArrayCodeFix>;

namespace EnergyAnalyzerTests.Collection; 

public class ArrayCodeFixTests {
	[Test]
	public async Task ArrayCodeFix_Diagnostic01() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|List<string> list = new List<string>(10);|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		string[] list = new string[10];
	}
}");
	}
	
	[Test]
	public async Task ArrayCodeFix_NoDiagnostic02() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		string[] list = new string[10];
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		string[] list = new string[10];
	}
}");
	}
	
	[Test]
	public async Task ArrayCodeFix_NoDiagnostic03() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		int a = 5;
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		int a = 5;
	}
}");
	}
	
	[Test]
	public async Task ArrayCodeFix_NoDiagnostic04() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		int[] list = new int[10];
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		int[] list = new int[10];
	}
}");
	}
	
	[Test]
	public async Task ArrayCodeFix_Diagnostic05() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|List<int> list = new List<int>(10);|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		int[] list = new int[10];
	}
}");
	}
	
	[Test]
	public async Task ArrayCodeFix_Diagnostic06() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|List<int> list = new List<int>(10);|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	int[] list = new int[10];
}");
	}
	
	[Test]
	public async Task ArrayCodeFix_Diagnostic07() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|List<int> list {get; set;} = new List<int>(10);|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	int[] list {get; set;} = new int[10];
}");
	}
	
	[Test]
	public async Task ArrayCodeFix_NoDiagnostic08() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	int[] list = new int[10];
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	int[] list = new int[10];
}");
	}
	
	[Test]
	public async Task ArrayCodeFix_NoDiagnostic09() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	int[] list {get; set;} = new int[10];
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	int[] list {get; set;} = new int[10];
}");
	}
	
	[Test]
	public async Task ArrayCodeFix_Diagnostic10() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|List<string> list {get; set;} = new List<string>(10);|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	string[] list {get; set;} = new string[10];
}");
	}
	
	[Test]
	public async Task ArrayCodeFix_Diagnostic11() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|List<string> list = new List<string>(10);|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	string[] list = new string[10];
}");
	}
}