﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;
using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Collection.SetAnalyzer>;

namespace EnergyAnalyzerTests.Collection;

public class SetTests {
	[Test]
	public async Task HashSetTest01_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										HashSet<int> set = new HashSet<int>();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task HashSetTest02_Diagnostic() {
		const string testCode = @"
								using System.Collections.Immutable;
								class Test { 
									void Method(){ 
										ImmutableHashSet<int> set;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeHashSet").WithSpan(5, 11, 5, 37)
				.WithMessage(
					"Using a HashSet is usually more efficient than other types of sets.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task HashSetTest03_Diagnostic() {
		const string testCode = @"
								using System.Collections.Immutable;
								class Test { 
									void Method(){ 
										ImmutableSortedSet<int> set;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeHashSet").WithSpan(5, 11, 5, 39)
				.WithMessage(
					"Using a HashSet is usually more efficient than other types of sets.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task HashSetTest04_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										SortedSet<int> set;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeHashSet").WithSpan(5, 11, 5, 30)
				.WithMessage(
					"Using a HashSet is usually more efficient than other types of sets.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task HashSetTest05_NoDiagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										uint a = 5;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task HashSetTest06_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									SortedSet<int> set;
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeHashSet").WithSpan(4, 10, 4, 29)
				.WithMessage(
					"Using a HashSet is usually more efficient than other types of sets.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task HashSetTest07_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									SortedSet<int> set {get; set;}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeHashSet").WithSpan(4, 10, 4, 40)
				.WithMessage(
					"Using a HashSet is usually more efficient than other types of sets.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task HashSetTest08_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									SortedSet<int> set = new SortedSet<int>();
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeHashSet").WithSpan(4, 10, 4, 52)
				.WithMessage(
					"Using a HashSet is usually more efficient than other types of sets.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task HashSetTest09_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									SortedSet<int> set {get; set;} = new SortedSet<int>();
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeHashSet").WithSpan(4, 10, 4, 64)
				.WithMessage(
					"Using a HashSet is usually more efficient than other types of sets.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}