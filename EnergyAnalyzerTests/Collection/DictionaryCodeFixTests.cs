﻿using System.Threading.Tasks;
using NUnit.Framework;
using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Collection.DictionaryAnalyzer,
		EnergyAnalyzer.Collection.DictionaryCodeFix>;

namespace EnergyAnalyzerTests.Collection;

public class DictionaryCodeFixTests {
	[Test]
	public async Task DictionaryCodeFix_Diagnostic01() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|ImmutableDictionary<string, string> table;|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<string, string> table;
	}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_Diagnostic02() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|ImmutableDictionary<string, string> table;|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	Dictionary<string, string> table;
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_Diagnostic03() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Generic;
class Test {
	[|SortedDictionary<string, string> table {get; set;}|]
}", @"
using System.Collections.Generic;
class Test {
	Dictionary<string, string> table {get; set;}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_Diagnostic04() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|ImmutableDictionary<string, int> table;|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<string, int> table;
	}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_NoDiagnostic05() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<string, int> table;
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<string, int> table;
	}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_Diagnostic06() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Generic;
class Test {
	[|SortedDictionary<string, string> table {get; set;} = new SortedDictionary<string, string>();|]
}", @"
using System.Collections.Generic;
class Test {
	Dictionary<string, string> table {get; set;} = new Dictionary<string, string>();
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_Diagnostic07() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Generic;
class Test {
	[|SortedDictionary<string, string> table = new SortedDictionary<string, string>();|]
}", @"
using System.Collections.Generic;
class Test {
	Dictionary<string, string> table = new Dictionary<string, string>();
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_Diagnostic08() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|SortedDictionary<string, string> table = new SortedDictionary<string, string>();|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<string, string> table = new Dictionary<string, string>();
	}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_NoDiagnostic09() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<string, int> table = new Dictionary<string, int>();
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<string, int> table = new Dictionary<string, int>();
	}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_NoDiagnostic10() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	Dictionary<string, int> table = new Dictionary<string, int>();
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	Dictionary<string, int> table = new Dictionary<string, int>();
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_NoDiagnostic11() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	Dictionary<string, int> table {get; set;} = new Dictionary<string, int>();
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	Dictionary<string, int> table {get; set;} = new Dictionary<string, int>();
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_Diagnostic12() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|SortedDictionary<string, string> table = new SortedDictionary<string, string>(), table2 = new SortedDictionary<string, string>();|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<string, string> table = new Dictionary<string, string>(), table2 = new Dictionary<string, string>();
	}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_NoDiagnostic13() {
		await Verify.VerifyCodeFixAsync(@"
class Test {
	void Method(){
		int a = 5;
	}
}", @"
class Test {
	void Method(){
		int a = 5;
	}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_NoDiagnostic14() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		var table = new Dictionary<string, int>();
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		var table = new Dictionary<string, int>();
	}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_Diagnostic15() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections;
using System.Collections.Generic;
class Test {
	void Method(){
		[|Hashtable table = new Hashtable();|]
	}
}", @"
using System.Collections;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<object, object> table = new Dictionary<object, object>();
	}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_Diagnostic16() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections;
using System.Collections.Generic;
class Test {
	void Method(){
		[|SortedList table = new SortedList();|]
	}
}", @"
using System.Collections;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<object, object> table = new Dictionary<object, object>();
	}
}");
	}
	
	[Test]
	public async Task DictionaryCodeFix_Diagnostic17() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections;
using System.Collections.Generic;
class Test {
	void Method(){
		[|SortedDictionary<string, string> table = new SortedDictionary<string, string>();|]
	}
}", @"
using System.Collections;
using System.Collections.Generic;
class Test {
	void Method(){
		Dictionary<string, string> table = new Dictionary<string, string>();
	}
}");
	}
}