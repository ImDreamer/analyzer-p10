﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;
using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Collection.DictionaryAnalyzer>;

namespace EnergyAnalyzerTests.Collection;

public class DictionaryTests {
	[Test]
	public async Task DictionaryTest01_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										Dictionary<string, string> table = new Dictionary<string, string>();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task DictionaryTest02_Diagnostic() {
		const string testCode = @"
								using System.Collections;
								class Test { 
									void Method(){ 
										Hashtable table = new Hashtable();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(5, 11, 5, 45)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DictionaryTest03_Diagnostic() {
		const string testCode = @"
								using System.Collections.Immutable;
								class Test { 
									void Method(){ 
										ImmutableDictionary<string, string> table;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(5, 11, 5, 53)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DictionaryTest04_Diagnostic() {
		const string testCode = @"
								using System.Collections.Immutable;
								class Test { 
									void Method(){ 
										ImmutableSortedDictionary<string, string> table;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(5, 11, 5, 59)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DictionaryTest05_Diagnostic() {
		const string testCode = @"
								using System.Collections.ObjectModel;
								class Test { 
									void Method(){ 
										ReadOnlyDictionary<string, string> table;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(5, 11, 5, 52)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DictionaryTest06_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										SortedDictionary<string, string> table = new SortedDictionary<string, string>();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(5, 11, 5, 91)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DictionaryTest07_Diagnostic() {
		const string testCode = @"
								using System.Collections;
								class Test { 
									void Method(){ 
										SortedList table = new SortedList();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(5, 11, 5, 47)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DictionaryTest08_NoDiagnostic() {
		const string testCode = @"
								class Test { 
									void Method(){ 
										uint a = 5;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task DictionaryTest09_Diagnostic() {
		const string testCode = @"
								using System.Collections;
								class Test { 
									private Hashtable table = new Hashtable();
									void Method(){ 
										table = new Hashtable();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(4, 10, 4, 52)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DictionaryTest10_Diagnostic() {
		const string testCode = @"
								using System.Collections;
								class Test { 
									Hashtable table = new Hashtable();
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(4, 10, 4, 44)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DictionaryTest11_Diagnostic() {
		const string testCode = @"
								using System.Collections;
								class Test { 
									Hashtable table {get; set;} = new Hashtable();
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(4, 10, 4, 56)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DictionaryTest12_Diagnostic() {
		const string testCode = @"
								using System.Collections;
								class Test { 
									Hashtable table;
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(4, 10, 4, 26)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task DictionaryTest13_Diagnostic() {
		const string testCode = @"
								using System.Collections;
								class Test { 
									Hashtable table {get; set;}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeDictionary").WithSpan(4, 10, 4, 37)
				.WithMessage(
					"Using a Dictionary is usually more efficient than other types of tables.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}