﻿using System.Threading.Tasks;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;
using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<EnergyAnalyzer.Collection.ListAnalyzer>;

namespace EnergyAnalyzerTests.Collection;

public class ListTests {
	[Test]
	public async Task ListTest01_NoDiagnostic() {
		const string testCode = @"class Test { 
									void Method(){ 
										int[] a = new int[100];
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ListTest02_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> a = new List<int>();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ListTest03_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> a = new List<int>(100);
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task ListTest04_NoDiagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										int a = 5;
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task ListTest05_NoDiagnostic() {
		const string testCode = @"
								using System.Collections;
								class Test { 
									void Method(){ 
										ArrayList a = new ArrayList(100);
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task ListTest06_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										LinkedList<int> a = new LinkedList<int>();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeList").WithSpan(5, 11, 5, 53)
				.WithMessage(
					"Using a List is usually more efficient than other types of lists, when array is not easily available.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ListTest07_Diagnostic() {
		const string testCode = @"
								using System.Collections.Immutable;
								class Test { 
									void Method(){ 
										ImmutableList<int> a;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeList").WithSpan(5, 11, 5, 32)
				.WithMessage(
					"Using a List is usually more efficient than other types of lists, when array is not easily available.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ListTest08_Diagnostic() {
		const string testCode = @"
								using System.Collections.Immutable;
								class Test { 
									void Method(){ 
										ImmutableArray<int> a;
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeList").WithSpan(5, 11, 5, 33)
				.WithMessage(
					"Using a List is usually more efficient than other types of lists, when array is not easily available.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task ListTest09_NoDiagnostic() {
		const string testCode = @"
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<int> a = Enumerable.Range(0, 100).ToList();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode);
	}

	[Test]
	public async Task ListTest10_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										LinkedList<int> a = new LinkedList<int>();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeList").WithSpan(5, 11, 5, 53)
				.WithMessage(
					"Using a List is usually more efficient than other types of lists, when array is not easily available.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ListTest11_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									LinkedList<int> a = new LinkedList<int>();
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeList").WithSpan(4, 10, 4, 52)
				.WithMessage(
					"Using a List is usually more efficient than other types of lists, when array is not easily available.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ListTest12_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									LinkedList<int> a {get; set;} = new LinkedList<int>();
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeList").WithSpan(4, 10, 4, 64)
				.WithMessage(
					"Using a List is usually more efficient than other types of lists, when array is not easily available.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ListTest13_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									LinkedList<int> a;
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeList").WithSpan(4, 10, 4, 28)
				.WithMessage(
					"Using a List is usually more efficient than other types of lists, when array is not easily available.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
	
	[Test]
	public async Task ListTest14_Diagnostic() {
		const string testCode = @"
								using System.Collections.Generic;
								class Test { 
									LinkedList<int> a {get; set;}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("MakeList").WithSpan(4, 10, 4, 39)
				.WithMessage(
					"Using a List is usually more efficient than other types of lists, when array is not easily available.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}
}