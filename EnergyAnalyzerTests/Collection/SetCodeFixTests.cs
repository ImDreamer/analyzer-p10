﻿using System.Threading.Tasks;
using NUnit.Framework;
using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Collection.SetAnalyzer,
		EnergyAnalyzer.Collection.SetCodeFix>;

namespace EnergyAnalyzerTests.Collection;

public class SetCodeFixTests {
	[Test]
	public async Task SetCodeFix_Diagnostic01() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|SortedSet<string> set = new SortedSet<string>();|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		HashSet<string> set = new HashSet<string>();
	}
}");
	}
	
	[Test]
	public async Task SetCodeFix_Diagnostic02() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|ImmutableHashSet<string> set;|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		HashSet<string> set;
	}
}");
	}
	
	[Test]
	public async Task SetCodeFix_Diagnostic03() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|ImmutableSortedSet<string> set;|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		HashSet<string> set;
	}
}");
	}
	
	[Test]
	public async Task SetCodeFix_Diagnostic04() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|SortedSet<string> set = new SortedSet<string>();|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	HashSet<string> set = new HashSet<string>();
}");
	}
	
	[Test]
	public async Task SetCodeFix_Diagnostic05() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|SortedSet<string> set {get; set;} = new SortedSet<string>();|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	HashSet<string> set {get; set;} = new HashSet<string>();
}");
	}
	
	[Test]
	public async Task SetCodeFix_NoDiagnostic06() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	HashSet<string> set {get; set;} = new HashSet<string>();
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	HashSet<string> set {get; set;} = new HashSet<string>();
}");
	}
	
	[Test]
	public async Task SetCodeFix_NoDiagnostic07() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	HashSet<string> set = new HashSet<string>();
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	HashSet<string> set = new HashSet<string>();
}");
	}
	
	[Test]
	public async Task SetCodeFix_NoDiagnostic08() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		HashSet<string> set = new HashSet<string>();
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		HashSet<string> set = new HashSet<string>();
	}
}");
	}
	
	[Test]
	public async Task SetCodeFix_Diagnostic09() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|ImmutableHashSet<string> set;|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		HashSet<string> set;
	}
}");
	}
	
	[Test]
	public async Task SetCodeFix_Diagnostic10() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|ImmutableHashSet<string> set;|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	HashSet<string> set;
}");
	}
	
	[Test]
	public async Task SetCodeFix_Diagnostic11() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|ImmutableHashSet<string> set {get; set;}|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	HashSet<string> set {get; set;}
}");
	}
}