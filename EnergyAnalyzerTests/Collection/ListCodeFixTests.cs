﻿using System.Threading.Tasks;
using Microsoft;
using NUnit.Framework;
using Verify =
	Microsoft.CodeAnalysis.CSharp.Testing.NUnit.CodeFixVerifier<EnergyAnalyzer.Collection.ListAnalyzer,
		EnergyAnalyzer.Collection.ListCodeFix>;

namespace EnergyAnalyzerTests.Collection; 

public class ListCodeFixTests {
	[Test]
	public async Task ListCodeFix_Diagnostic01() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|LinkedList<string> list = new LinkedList<string>();|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		List<string> list = new List<string>();
	}
}");
	}
	
	[Test]
	public async Task ListCodeFix_Diagnostic02() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|LinkedList<string> list = new LinkedList<string>();|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	List<string> list = new List<string>();
}");
	}
	
	[Test]
	public async Task ListCodeFix_Diagnostic03() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|LinkedList<string> list {get; set;} = new LinkedList<string>();|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	List<string> list {get; set;} = new List<string>();
}");
	}
	
	[Test]
	public async Task ListCodeFix_NoDiagnostic04() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		List<string> list = new List<string>();
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		List<string> list = new List<string>();
	}
}");
	}
	
	[Test]
	public async Task ListCodeFix_NoDiagnostic05() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		uint a = 5;
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		uint a = 5;
	}
}");
	}
	
	[Test]
	public async Task ListCodeFix_NoDiagnostic06() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	List<string> list = new List<string>();
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	List<string> list = new List<string>();
}");
	}
	
	[Test]
	public async Task ListCodeFix_NoDiagnostic07() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	List<string> list {get; set;} = new List<string>();
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	List<string> list {get; set;} = new List<string>();
}");
	}
	
	[Test]
	public async Task ListCodeFix_Diagnostic08() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|LinkedList<int> list {get; set;} = new LinkedList<int>();|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	List<int> list {get; set;} = new List<int>();
}");
	}
	
	[Test]
	public async Task ListCodeFix_Diagnostic09() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	[|LinkedList<object> list {get; set;} = new LinkedList<object>();|]
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	List<object> list {get; set;} = new List<object>();
}");
	}
	
	[Test]
	public async Task ListCodeFix_Diagnostic10() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections;
using System.Collections.Generic;
class Test {
	[|ArrayList list {get; set;} = new ArrayList();|]
}", @"
using System.Collections;
using System.Collections.Generic;
class Test {
	List<object> list {get; set;} = new List<object>();
}");
	}
	
	[Test]
	public async Task ListCodeFix_Diagnostic11() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections;
using System.Collections.Generic;
class Test {
	[|ArrayList list = new ArrayList();|]
}", @"
using System.Collections;
using System.Collections.Generic;
class Test {
	List<object> list = new List<object>();
}");
	}
	
	[Test]
	public async Task ListCodeFix_Diagnostic12() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections;
using System.Collections.Generic;
class Test {
	void Method(){
		[|ArrayList list = new ArrayList();|]
	}
}", @"
using System.Collections;
using System.Collections.Generic;
class Test {
	void Method(){
		List<object> list = new List<object>();
	}
}");
	}
	
	[Test]
	public async Task ListCodeFix_Diagnostic13() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|ImmutableList<string> list;|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		List<string> list;
	}
}");
	}
	
	[Test]
	public async Task ListCodeFix_Diagnostic14() {
		await Verify.VerifyCodeFixAsync(@"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		[|IEnumerable<string> list;|]
	}
}", @"
using System.Collections.Immutable;
using System.Collections.Generic;
class Test {
	void Method(){
		List<string> list;
	}
}");
	}
}