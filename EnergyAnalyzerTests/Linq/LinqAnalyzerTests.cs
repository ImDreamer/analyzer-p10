﻿using System.Threading.Tasks;
using EnergyAnalyzer.Linq;
using Microsoft.CodeAnalysis.Testing;
using NUnit.Framework;

namespace EnergyAnalyzerTests.Linq;

using Verify = Microsoft.CodeAnalysis.CSharp.Testing.NUnit.AnalyzerVerifier<LinqAnalyzer>;

public class LinqAnalyzerTests {
	[Test]
	public async Task LINQTest01_NoDiagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>();
									}
								}";
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}

	[Test]
	public async Task LINQTest02_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>();
										list.Where(x=> x == 0);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("LINQUsage").WithSpan(8, 11, 8, 33)
				.WithMessage("Replace LINQ as LINQ has shown to be significantly less efficient than any alternative.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LINQTest03_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>();
										var test = list.Where(x=> x == 0);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("LINQUsage").WithSpan(8, 22, 8, 44)
				.WithMessage("Replace LINQ as LINQ has shown to be significantly less efficient than any alternative.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LINQTest04_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>();
										list.Where(x=> x == 0).ToList();
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("LINQUsage").WithSpan(8, 11, 8, 42)
				.WithMessage("Replace LINQ as LINQ has shown to be significantly less efficient than any alternative.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LINQTest05_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>();
										list.Where(x => x == 0).Select(u => u + u).ToList();
									}
								}";

		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("LINQUsage").WithSpan(8, 11, 8, 62)
				.WithMessage("Replace LINQ as LINQ has shown to be significantly less efficient than any alternative.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LINQTest06_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>();
										list.Select(x=> x == 0);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("LINQUsage").WithSpan(8, 11, 8, 34)
				.WithMessage("Replace LINQ as LINQ has shown to be significantly less efficient than any alternative.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LINQTest07_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>();
										list.SelectMany(_ => list.Select(u => u + 1));
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("LINQUsage").WithSpan(8, 11, 8, 56)
				.WithMessage("Replace LINQ as LINQ has shown to be significantly less efficient than any alternative.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LINQTest08_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>();
										list.All(x => x == x);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("LINQUsage").WithSpan(8, 11, 8, 32)
				.WithMessage("Replace LINQ as LINQ has shown to be significantly less efficient than any alternative.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LINQTest09_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>();
										list.Any(x => x == x);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("LINQUsage").WithSpan(8, 11, 8, 32)
				.WithMessage("Replace LINQ as LINQ has shown to be significantly less efficient than any alternative.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LINQTest10_Diagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										List<UInt32> list = new List<UInt32>();
										list.Where(u => u == 10).Contains<uint>(10);
									}
								}";
		DiagnosticResult expected =
			DiagnosticResult.CompilerWarning("LINQUsage").WithSpan(8, 11, 8, 54)
				.WithMessage("Replace LINQ as LINQ has shown to be significantly less efficient than any alternative.");
		await Verify.VerifyAnalyzerAsync(testCode, expected);
	}

	[Test]
	public async Task LINQTest11_NoDiagnostic() {
		const string testCode = @"
								using System;
								using System.Linq;
								using System.Collections.Generic;
								class Test { 
									void Method(){ 
										var result = string.Join(""\n"", new List<string>());
									}
								}";
		
		await Verify.VerifyAnalyzerAsync(testCode, DiagnosticResult.EmptyDiagnosticResults);
	}
}
/*
List<UInt32> list = new List<UInt32>();
list.Concat(list);
list.Concat(list);
list.DefaultIfEmpty();
list.Distinct();
list.Except(list);
list.Intersect(list);
list.Union(list);
list.OrderBy(u => u);
list.OrderByDescending(u => u);
list.OrderBy(u => u).ThenBy(u => u);
list.OrderByDescending(u => u).ThenByDescending(u => u);
list.OrderBy(u => u).ThenByDescending(u => u);
list.OrderByDescending(u => u).ThenBy(u => u);
list.Reverse();
list.Aggregate(list, (longest, _) => longest, tt => tt);
list.Average(u => u);
list.Count(u => u == 10);
list.LongCount(u => u == 10);
list.Max(u => u == 10);
list.Min(u => u == 10);
list.MaxBy(u => u == 10);
list.MinBy(u => u == 10);
list.Sum(u => u);
list.Cast<int>();
list.OfType<uint>();
list.ElementAt(10);
list.ElementAtOrDefault(10);
list.First();
list.FirstOrDefault();
list.Last();
list.LastOrDefault();
list.Single();
list.SingleOrDefault();
list.Skip(10);
list.SkipWhile(u => u == 10);
list.Take(10);
list.TakeWhile(u => u == 10);
list.TakeLast(10);
list.ToList();
list.SequenceEqual(list);
list.ToArray();
list.ToDictionary(u => u, u => u + 10);
list.Chunk(10);
list.Prepend(10u);
list.Append(10u);
list.Zip(list);
list.GroupBy(u => u);
list.Join(list, u => u, u => u, (u1, u2) => u1 + u2);
list.GroupJoin(list,
	person => person,
	pet => pet,
	(person, petCollection) => person + petCollection.Count());
*/